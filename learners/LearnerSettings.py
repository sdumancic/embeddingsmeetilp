
class LearnerSettings:

    def __init__(self):
        self.params = {}
        self.defaults = {}
        self._add_defaults()

    def _add_defaults(self):
        self.defaults['class_evaluation'] = 'accuracy'
        self.defaults["criterion"] = ["gini", "entropy"]
        self.defaults["max_depth"] = [4, 8, 12, 16, 24, 48]
        self.defaults["min_samples_leaf"] = [2, 4, 6, 8]
        self.defaults["min_impurity_decrease"] = [0.0, 0.05, 0.1, 0.15]
        self.defaults["n_splits"] = 5
        self.defaults["n_neighbors"] = [3, 5, 7, 9, 11, 13, 15]
        self.defaults["weights"] = ["uniform", "distance"]
        self.defaults["kernel"] = ["rbf", "linear", "poly"]
        self.defaults["degree"] = [3, 5, 7, 9]
        self.defaults['gamma'] = [0.01, 0.1, 0.5, 0.05, 0.2, 0.3, 0.6, 0.7, 1]
        self.defaults["C"] = [0.1, 1.0, 10.0, 100.0]
        self.defaults['penalty'] = ['l1', 'l2']
        self.defaults['dual'] = [True, False]
        self.defaults['fit_intercept'] = [True, False]
        self.defaults['lr_solver'] = ['lbfgs', 'liblinear']
        self.defaults['lr_multiclass'] = ['ovr', 'multinomial']
        self.defaults["minimal_cases"] = [2, 4, 6, 8, 10, 12, 15]
        self.defaults["accuracy"] = [0.75, 0.8, 0.9, 1.0]
        self.defaults["heuristics"] = ["gain", "gainratio"]
        self.defaults["n_folds"] = 5
        self.defaults["threshold"] = 50
        self.defaults["hierarchical_distance"] = "euclidean"
        self.defaults['spectral_affinity'] = "rbf"
        self.defaults['n_bounds'] = 5
        self.defaults['subset'] = 0.1
        self.defaults['aleph_search'] = 'bf'
        self.defaults['aleph_clause_length'] = [3, 4, 6]
        self.defaults['aleph_min_acc'] = [0.0, 0.5, 0.7, 0.9, 1.0]
        self.defaults['aleph_min_pos'] = [2, 4, 6, 8, 10, 12]
        self.defaults['aleph_folds'] = 4
        self.defaults['aleph_verbosity'] = 0
        self.defaults['aleph_construct_bottom'] = 'reduction'
        self.defaults['aleph_minscore'] = 3
        self.defaults['aleph_max_features'] = 10000
        self.defaults['aleph_evalfn'] = 'posonly'
        self.defaults['timeout'] = 120
        self.defaults['max_negatives'] = 100
        self.defaults['kfoil_max_clauses'] = [1000]
        self.defaults['kfoil_max_literals'] = [3, 5, 7, 9]
        self.defaults['kfoil_beam_size'] = [1, 2, 3, 4, 5, 6, 7]
        self.defaults['kfoil_t'] = [0, 1, 2, 3]
        self.defaults['kfoil_d'] = self.defaults['degree']
        self.defaults['kfoil_g'] = [0.5, 2.0, 1.0, 0.1, 10.0, 0.01]
        self.defaults['kfoil_s'] = [0.01, 0.05, 0.1, 0.5, 1.0, 2.0, 10.0]
        self.defaults['kfoil_r'] = self.defaults['C']
        self.defaults['kfoil_b'] = [0, 1]
        self.defaults['kfoil_N'] = [True, False]
        self.defaults['kfoil_M'] = [True, False]
        self.defaults['kfoil_c'] = [0.1, 0.25, 1.0]
        self.defaults['amie_folds'] = 4
        self.defaults['amie_maxad'] = [3, 5, 7, 10]
        self.defaults['amie_minc'] = [0.1, 0.5, 0.7, 0.8, 0.9, 1.0]
        self.defaults['amie_minis'] = 2
        self.defaults['amie_mins'] = [2, 4, 6, 8, 10]
        self.defaults['amie_rl'] = [0]
        self.defaults['amie_minpca'] = [0.0, 0.01, 0.1, 0.2]
        self.defaults['amie_include_negatives'] = True
        self.defaults['recent_depth'] = [0, 1, 2]
        self.defaults['metagol_include_negatives'] = False
        self.defaults['metagol_max_clauses'] = [10, 20, 30, 50]
        self.defaults['metagol_folds'] = 4
        self.defaults['metagol_metarules'] = ['metarule([P,Q],([P,A,B]:-[[Q,A,B]])).',
                                              'metarule([P/2,Q/2,C/0],([P,A,C]:-[[Q,A,B]])).',
                                              'metarule([P/2,Q/2/C/0],([P,A]:-[[Q,A,C]])).',
                                              'metarule([P,Q],([P,A,B]:-[[Q,B,A]])).',
                                              'metarule([P,Q,R],([P,A,B]:-[[Q,A,C],[R,C,B]])).',
                                              'metarule([P,Q,R],([P,A,C]:-[[Q,A,C],[R,C]])).',
                                              'metarule([P,Q,R],([P,A,C]:-[[Q,A,C],[R,A]])).']
                                              #'metarule([P,Q],([P,A,B]:-[[Q,A,C],[P,C,B]])).']

    def add_parameter(self, name, value):
        self.params[name] = value

    def get_parameter(self, name):
        if name not in self.params:
            return self.defaults[name]
        else:
            return self.params[name]

    def get_parameter_names(self):
        return list(self.params.keys())

    def has_parameter(self, name):
        return name in self.params or name in self.defaults


constant_domains = ['word', 'propertylink', 'typee', 'typel', 'typec', 'typeia', 'typea', 'typlelog', 'typeio',
                    'featuredom', 'feature', 'dbiltype', 'chetype', 'tptype', 'gottype', 'gpttype', 'ztttype', 'durtype',
                    'ttttype', 'tbiltype', 'agetype', 'acttype', 'fibtype', 'albtype', 'sextype', 'tchotype',
                    'genderd', 'genderdom', 'district_name', 'region', 'inhabitants', 'mun1', 'mun2', 'mun3', 'mun4',
                    'cities', 'ratio', 'avgsal', 'unemploy95', 'unemploy96', 'enterpreneurs', 'crimes95', 'crimes96',
                    'complex', 'enzyme', 'location', 'intertype', 'phenotype', 'class', 'dir', 'level', 'phase', 'faculty',
                    'years', 'typeio', 'typlelog', 'typea', 'typee', 'typel', 'typec', 'typeia', 'featuredom',
                    'complex', 'enzyme', 'intertype', 'location', 'phenotype', 'class', 'azart', 'eatyp',
                    'a1', 'a2', 'a3', 'a4', 'a5', 'a6', 'a7', 'a8', 'a9', 'a10', 'a11', 'a12', 'a13', 'a14', 'a15', 'a16',
                    'a17', 'a18', 'a19', 'a20', 'a21', 'a22', 'a23', 'a24', 'a25', 'a26', 'a27', 'a28', 'a29', 'a30',
                    'a31', 'a32', 'a33', 'a34', 'a35', 'a35', 'a37', 'a38', 'a39', 'a40', 'a41', 'a42',
                    'azart', 'kanton', 'gbeadmgeb', 'prtyp', 'prtypnr',
                    'epberuf', 'eperweart', 'epgebudat', 'epsta', 'eplnamecd', 'epsexcd', 'epzivstd',
                    'tkbeg', 'tkend', 'tkexkzwei', 'tkstacd', 'tkleist', 'tkinkprl', 'tkinkpre', 'tktarpra', 'tkuebvwsysp', 'tkuebvwsysl',
                    'tkprfin', 'tkdyncd', 'tkausbcd', 'tkrauv', 'tksmed', 'tkrizucd', 'tktodleista', 'tkerlleista', 'tkrenleista',
                    'tkeuleista', 'tkunfleista', 'trteceinal', 'truntcd', 'trklauscd', 'trstafcd', 'trricd',
                    'vvstacd', 'vvinkzwei', 'vvbeg', 'vvend', 'vvinkprl', 'vvinkpre', 'vvwae', 'vvversart', 'vvaendart', 'vvaendat', 'vvabvb',
                    'vvabga', 'vvstifcd', 'vvvorscd', 'vvbvgcd', 'vveucd',
                    'atomtype', 'charge']

input_only_domains = ['pic', 'drug', 'key', 'gene']
