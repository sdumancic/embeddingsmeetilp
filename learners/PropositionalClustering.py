from learners.PropositionalLearner import PropositionalLearner
from sklearn.metrics.cluster import adjusted_rand_score
from sklearn.cluster import SpectralClustering
from sklearn.cluster import AgglomerativeClustering
import numpy as np
from learners.hybrid.EmbeddingAbstractor import EmbeddingAbstractor


class PropositionalClustering(PropositionalLearner):

    def __init__(self, parameters):
        super().__init__(parameters)
        self.score_distance = EmbeddingScoreDistance(parameters.get_parameter('embedding_method'))
        self.entity_order = []

    def load_relation_embedding(self, embedding_map):
        self.score_distance.load_relation_embeddings(embedding_map)

    def read_relation_embedding(self, embedding_file):
        relation_map = {}

        for line in open(embedding_file).readlines():
            if len(line) < 3:
                pass
            ent, emb = line.strip().split()
            emb = [float(x) for x in emb.split(",")]
            relation_map[ent] = np.array(emb)

        self.score_distance.load_relation_embeddings(relation_map)

    def get_affinity_matrix(self, type='distance'):
        mat = np.zeros((len(self.entity_order), len(self.entity_order)))

        for i in range(len(self.entity_order) - 1):
            for j in range(i+1, len(self.entity_order)):
                to_compare_with = [x for x in self.entity_order if self.entity_order.index(x) not in [i, j]]
                score = 0.0
                if type == 'distance':
                    score = self.score_distance.get_distance(self.entity_order[i], self.entity_order[j], to_compare_with)
                else:
                    score = self.score_distance.get_similarity(self.entity_order[i], self.entity_order[j], to_compare_with)

                mat[i, j] = score
                mat[j, i] = score

        return mat

    def _fit_with_params(self, training_data, labels):
        return

    def learn_and_evaluate(self, training_data_file, training_labels_file, test_data_file="", test_labels_file="",
                           definitions=""):
        try:
            self._read_embeddings(training_data_file)
            self._read_labels(training_labels_file)

            data = []
            labels = []
            for elem in self.labels:
                if elem in self.embeddings and elem in self.labels:
                    self.entity_order.append(elem)
                    data.append(self.embeddings[elem])
                    labels.append(self.labels[elem])
                else:
                    print("found element without an embedding: {}".format(elem))

            resulting_clustering = self._fit_with_params(data, labels)

            return adjusted_rand_score(labels, resulting_clustering)
        except ValueError:
            return -1.0
        except Exception:
            return -1.0


class Spectral(PropositionalClustering):

    def __init__(self, settings):
        super().__init__(settings)
        self.n_clusters = settings.get_parameter("n_clusters")
        self.affinity = settings.get_parameter("spectral_affinity")

    def _fit_with_params(self, training_data, labels):
        sc = SpectralClustering(n_clusters=self.n_clusters, affinity=self.affinity)
        if self.affinity == 'score' or self.affinity == 'precomputed':
            sc.fit(self.get_affinity_matrix('similarity'))
        else:
            sc.fit(training_data)

        return sc.labels_


class Hierarchical(PropositionalClustering):

    def __init__(self, parameters):
        super().__init__(parameters)
        self.n_clusters = parameters.get_parameter("n_clusters")
        self.affinity = parameters.get_parameter("hierarchical_distance")

    def _fit_with_params(self, training_data, labels):
        hc = AgglomerativeClustering(n_clusters=self.n_clusters, linkage='average', affinity=self.affinity)

        if self.affinity == 'score' or self.affinity == 'precomputed':
            hc.fit(self.get_affinity_matrix('distance'))
        else:
            hc.fit(training_data)

        return hc.labels_


class EmbeddingScoreDistance:

    def __init__(self, method):
        self.embedding_handler = EmbeddingAbstractor(method)
        self.embedding_method = method

    def load_entity_embeddings(self, embeddings):
        self.embedding_handler.load_entity_embeddings(embeddings)

    def load_relation_embeddings(self, embeddings):
        self.embedding_handler.load_entity_embeddings(embeddings)

    def get_similarity(self, entity1, entity2, entities_for_comparison):
        accumulator = 0.0

        for elem in entities_for_comparison:
            for relation in self.embedding_handler.get_relations():
                try:
                    accumulator += np.fabs(self.embedding_handler.get_score(entity1, elem, relation, self.embedding_method) -
                                           self.embedding_handler.get_score(entity2, elem, relation, self.embedding_method))
                except Exception:
                    pass

        return accumulator

    def get_distance(self, entity1, entity2, entities_for_comparison):
        return np.exp(-self.get_distance(entity1, entity2, entities_for_comparison))



