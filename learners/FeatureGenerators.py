from learners.RelationalLearner import Aleph
import os
import subprocess
import itertools
import signal
from learners.LearnerSettings import constant_domains, input_only_domains
import numpy as np
from problog.engine import DefaultEngine
from problog.logic import *
from problog.program import PrologString
import random


class AlephFeatureGenerator(Aleph):

    def __init__(self, parameters, working_dir):
        super().__init__(parameters, working_dir)
        self.rules = {}
        self.timeout = parameters.get_parameter('timeout')
        self.max_negatives = parameters.get_parameter('max_negatives')
        self.predicate_bias = {}
        self.target_pred = ""

    def set_target(self, predicate_name):
        self.target_pred = predicate_name

    def read_bias(self, bias):
        self.predicate_bias = bias

    def _prepare_knowledge_file(self, definitions, labels, constants, facts_per_predicate, clause_length, min_acc, min_pos, working_directory, override_search=""):
        bg_file = open("{}/train.b".format(working_directory), 'w')

        # settings
        self._prepare_settings(bg_file, clause_length, min_acc, min_pos, override_search)

        self._prepare_bias(bg_file, definitions, labels, constants)

        self._prepare_data_and_types(bg_file, facts_per_predicate)

        bg_file.close()

    def _prepare_settings(self, bg_file, clause_length, min_acc, min_pos, override_search=""):
        # settings
        if override_search == "":
            bg_file.write(":- set(evalfn,{}).\n".format(self.parameters.get_parameter('aleph_evalfn')))
        else:
            bg_file.write(":- set(evalfn,{}).\n".format(override_search))

        bg_file.write(":- set(clauselength,{}).\n".format(clause_length))

        if self.parameters.has_parameter('aleph_depth'):
            bg_file.write(":- set(depth,{}).\n".format(self.parameters.get_parameter('aleph_depth')))

        bg_file.write(":- set(minacc,{}).\n".format(min_acc))

        bg_file.write(":- set(minpos,{}).\n".format(min_pos))

        if self.parameters.has_parameter('aleph_newvars'):
            bg_file.write(":- set(newvars,{}).\n".format(self.parameters.get_parameter('aleph_newvars')))

        if self.parameters.has_parameter('aleph_nodes'):
            bg_file.write(":- set(nodes,{}).\n".format(self.parameters.get_parameter('aleph_nodes')))

        if self.parameters.has_parameter('aleph_noise'):
            bg_file.write(":- set(noise,{}).\n".format(self.parameters.get_parameter('aleph_noise')))

        if self.parameters.has_parameter('aleph_verbosity'):
            bg_file.write(":- set(verbosity,{}).\n".format(self.parameters.get_parameter('aleph_verbosity')))

        if self.parameters.has_parameter('aleph_minscore'):
            bg_file.write(":- set(minscore,{}).\n".format(self.parameters.get_parameter('aleph_minscore')))

        bg_file.write(":- set(explore,true).\n")

        bg_file.write(":- set(max_features,{}).\n".format(self.parameters.get_parameter('aleph_max_features')))

        if self.parameters.has_parameter('aleph_searchtime'):
            bg_file.write(":- set(searchtime,{}).\n".format(self.parameters.get_parameter('aleph_searchtime')))

        # bg_file.write(":- set(test_pos,'test.f').\n")
        # bg_file.write(":- set(test_neg,'test.n').\n")
        # bg_file.write(":- set(dependent,2).\n")

        bg_file.write("\n")

    def _prepare_data_and_types(self, bg_file, facts_per_predicate):
        bg_file.write("% types\n")
        # possible_labels = set(self.labels.values())
        # for cl in possible_labels:
        #    bg_file.write("class({}).\n".format(cl.lower()))

        for dom in self.constants_per_domain:
            bg_file.write(
                " ".join(["{}({}).".format(dom.lower(), c.lower()).replace("%", "").replace("\\", "").replace("=", "") for c in self.constants_per_domain[dom]]) + "\n")
            bg_file.write("\n")

        bg_file.write("% data\n")

        for pred in facts_per_predicate:
            for g in facts_per_predicate[pred]:
                bg_file.write("{}({}).\n".format(pred.lower(), ",".join([x.lower() for x in g])).replace("%", "").replace("\\", "").replace("=", ""))

    def _prepare_bias(self, bg_file, definitions, labels, constants):
        target_pred = list(labels.values())[0]
        if target_pred.lower() in self.predicate_bias:
            bg_file.write(":- modeh(1,{}{}).\n".format(target_pred.lower(), self.predicate_bias[target_pred][0]))
        else:
            bg_file.write(
                ":- modeh(1,{}({})).\n".format(target_pred, ",".join(["+" + x.lower() if x.lower() not in constant_domains else "#" + x.lower() for x in self.predicate_definitions[target_pred]])))

        for pred in definitions:
            if pred.lower() in self.predicate_bias:
                for bitem in self.predicate_bias[pred]:
                    bg_file.write(":- modeb(*,{}{}).\n".format(pred.lower(), bitem))
            elif len(definitions[pred]) == 1:
                bg_file.write(":- modeb(*, {}(+{})).\n".format(pred.lower(), definitions[pred][0]))
            elif pred.lower() == target_pred.lower():
                continue
            else:
                possible = [('+', '-', '#') if len(self.constants_per_domain[x]) <= 10 or x in constant_domains else ('+', '-') for x in
                            definitions[pred]]
                for combo in itertools.product(*possible):
                    bias = list(zip(combo, [x.lower() for x in definitions[pred]]))
                    if len(set([x for x in combo])) == 1 or len([x for x in combo if x == '+']) == 0:
                        continue
                    wrongly_put_as_non_input = [x for x in bias if x[1].lower() in input_only_domains and x[0] != '+']
                    have_correct_equivalence = [1 for x in wrongly_put_as_non_input if ('+', x[1]) in bias]
                    if len(wrongly_put_as_non_input) > 0 and len(wrongly_put_as_non_input) != len(have_correct_equivalence):
                        continue
                    rep = ["{}{}".format(x[0], x[1].lower()) for x in zip(combo, definitions[pred])]
                    bg_file.write(":- modeb(*,{}({})).\n".format(pred.lower(), ",".join(rep)))

        bg_file.write("\n")

        for pred in definitions:
            bg_file.write(":- determination({}/2,{}/{}).\n".format(target_pred.lower(), pred.lower(), len(definitions[pred])))

        bg_file.write("\n")

    def _prepare_positive_examples(self, labels, working_directory):
        pos_file = open("{}/train.f".format(working_directory), 'w')

        for ent in labels:
            pos_file.write("{}({}).\n".format(labels[ent].lower(), ent.lower()).replace("%", "").replace("\\", "").replace("=", ""))

        pos_file.close()

    def _prepare_negative_examples_features(self, labels, working_directory, test_predicate, max_constants_per_domain):
        neg_file = open("{}/train.n".format(working_directory), 'w')

        domains = self.predicate_definitions[test_predicate]
        domains = [list(self.constants_per_domain[d]) for d in domains]

        if max_constants_per_domain:
            for item in domains:
                random.shuffle(item)
            domains = [x[:max_constants_per_domain] for x in domains]

        for comb in itertools.product(*domains):
            gr_item = ",".join([x.lower() for x in comb])
            if gr_item not in labels:
                neg_file.write("{}({}).\n".format(list(labels.values())[0].lower(), gr_item).replace("%", "").replace("\\", "").replace("=", ""))

        neg_file.close()

    def _individual_run(self, conf):
        predicate_definitions = self.predicate_definitions.copy()
        test_predicate = conf['test']
        del predicate_definitions[test_predicate]

        if test_predicate not in self.facts_per_predicates:
            return {}

        final_labels = {}
        for gr_item in self.facts_per_predicates[test_predicate]:
            elem = ','.join([x.lower() for x in gr_item])
            final_labels[elem] = test_predicate

        constants = self.constants_per_domain
        facts = self.facts_per_predicates.copy()
        del facts[test_predicate]
        current_working_directory = self.working_directory + "/aleph_{}".format(conf['test'])

        if not os.path.exists(current_working_directory):
            os.mkdir(current_working_directory)

        min_pos = conf['mp']

        if isinstance(min_pos, float):
            min_pos = int(min_pos * len(final_labels))

        self._prepare_knowledge_file(predicate_definitions, final_labels, constants, facts, conf['cl'], conf['acc'], min_pos, current_working_directory)
        self._prepare_positive_examples(final_labels, current_working_directory)

        if self.parameters.get_parameter('aleph_evalfn') != 'posonly':
            self._prepare_negative_examples_features(final_labels, current_working_directory, test_predicate, self.max_negatives)

        cmd = "echo \"read_all(train). induce_features.\" " + "| yap -l {}/scripts/aleph.pl > log.txt 2> error.txt".format(
            os.path.abspath(os.path.dirname(__file__)))

        if not os.path.exists("{}/log.txt".format(current_working_directory)):
            proc = subprocess.Popen(cmd, shell=True, cwd=current_working_directory, preexec_fn=os.setsid)

            try:
                proc.wait(timeout=self.timeout)

                repeat_run = False

                for line in open("{}/error.txt".format(current_working_directory)).readlines():
                    if 'YAP OOOPS' in line:
                        repeat_run = True

                if repeat_run:
                    self._prepare_knowledge_file(predicate_definitions, final_labels, constants, facts, conf['cl'], conf['acc'], conf['mp'], current_working_directory, override_search="accuracy")
                    self._prepare_negative_examples_features(final_labels, current_working_directory, test_predicate, self.max_negatives)

                    proc = subprocess.Popen(cmd, shell=True, cwd=current_working_directory, preexec_fn=os.setsid)
                    proc.wait()

            except Exception:
                os.killpg(os.getpgid(proc.pid), signal.SIGTERM)

                self._prepare_knowledge_file(predicate_definitions, final_labels, constants, facts, conf['cl'], conf['acc'],
                                             conf['mp'], current_working_directory, override_search="coverage")
                self._prepare_negative_examples_features(final_labels, current_working_directory, test_predicate,
                                                         self.max_negatives)

                proc = subprocess.Popen(cmd, shell=True, cwd=current_working_directory, preexec_fn=os.setsid)
                try:
                    proc.wait(timeout=self.timeout)

                except Exception:
                    os.killpg(os.getpgid(proc.pid), signal.SIGTERM)
                    #proc.kill()

        print("                             reading from {}/log.txt".format(current_working_directory))
        for line in open("{}/log.txt".format(current_working_directory)).readlines():
            if line.startswith('feature'):
                tmp = line.strip().replace("feature(", "", 1).replace(")).", "")
                comma = tmp.find(',')
                tmp = tmp[comma + 2:]
                self.rules[len(self.rules)] = tmp

    def _fit_with_params(self, training_data, labels):
        confs = []
        predicates_to_consider = self.predicate_definitions
        if len(self.target_pred):
            predicates_to_consider = [self.target_pred]

        for pred in predicates_to_consider:
            conf = {'cl': self.parameters.get_parameter('aleph_clause_length')[0],
                    'acc': self.parameters.get_parameter('aleph_min_acc')[0],
                    'mp': self.parameters.get_parameter('aleph_min_pos')[0],
                    'test': pred}
            confs.append(conf)

        for item in confs:
            self._individual_run(item)

        return self.rules

    def find_features(self, training_data_file, definitions):
        self.predicate_definitions = self._read_definitions(definitions)

        for t_file in training_data_file:
            facts, constants = self._read_facts(t_file, self.predicate_definitions)

            for pred in facts:
                if pred not in self.facts_per_predicates:
                    self.facts_per_predicates[pred] = set()
                for elem in facts[pred]:
                    self.facts_per_predicates[pred].add(elem)

            for d in constants:
                if d not in self.constants_per_domain:
                    self.constants_per_domain[d] = set()
                for c in constants[d]:
                    self.constants_per_domain[d].add(c)

        self.entity_order_for_labels = list(self.labels.keys())

        self._enhance_data()
        return self._fit_with_params((self.facts_per_predicates, self.predicate_definitions), [])


class AlephFeatureGeneratorPerEntity(AlephFeatureGenerator):

    def _fit_with_params(self, training_data, labels):
        confs = []
        for dom in self.constants_per_domain:
            conf = {'cl': self.parameters.get_parameter('aleph_clause_length')[0],
                    'acc': self.parameters.get_parameter('aleph_min_acc')[0],
                    'mp': self.parameters.get_parameter('aleph_min_pos')[0],
                    'test': dom}
            confs.append(conf)

        for item in confs:
            self._individual_run(item)

        return self.rules

    def _individual_run(self, conf):
        predicate_definitions = self.predicate_definitions.copy()
        test_predicate = conf['test']

        final_labels = {}
        for elem in self.constants_per_domain[test_predicate]:
            final_labels[elem.lower()] = test_predicate

        constants = self.constants_per_domain
        facts = self.facts_per_predicates.copy()
        current_working_directory = self.working_directory + "/aleph_{}".format(conf['test'])

        if not os.path.exists(current_working_directory):
            os.mkdir(current_working_directory)

        self._prepare_knowledge_file(predicate_definitions, final_labels, constants, facts, conf['cl'], conf['acc'], conf['mp'], current_working_directory)
        self._prepare_positive_examples(final_labels, current_working_directory)
        if self.parameters.get_parameter('aleph_evalfn') != 'posonly':
            self._prepare_negative_examples_features(final_labels, current_working_directory, test_predicate)

        cmd = "echo \"read_all(train). induce_features.\" " + "| yap -l {}/scripts/aleph.pl > log.txt 2> error.txt".format(
            os.path.abspath(os.path.dirname(__file__)))

        proc = subprocess.Popen(cmd, shell=True, cwd=current_working_directory, preexec_fn=os.setsid)

        try:
            proc.wait()

            for line in open("{}/log.txt".format(current_working_directory)).readlines():
                if line.startswith('feature'):
                    tmp = line.strip().replace("feature(", "").replace(")).", "")
                    comma = tmp.find(',')
                    tmp = tmp[comma+2:]
                    self.rules[len(self.rules)] = tmp

        except Exception:
            os.killpg(os.getpgid(proc.pid), signal.SIGTERM)
            #proc.terminate()

    def _prepare_positive_examples(self, labels, working_directory):
        pos_file = open("{}/train.f".format(working_directory), 'w')

        for ent in labels:
            pos_file.write("target_{}({}).\n".format(labels[ent].lower(), ent.lower()).replace("%", "").replace("\\", "").replace("=", ""))

        pos_file.close()

    def _prepare_bias(self, bg_file, definitions, labels, constants):
        target_pred = list(labels.values())[0].lower()
        bg_file.write(
            ":- modeh(1,target_{}(+{})).\n".format(target_pred, target_pred))

        target_pred = "target_" + target_pred

        for pred in definitions:
            if len(definitions[pred]) == 1:
                bg_file.write(":- modeb(*, {}(+{})).\n".format(pred.lower(), definitions[pred][0]))
            elif pred.lower() == target_pred.lower():
                continue
            else:
                possible = [('+', '-', '#') if len(self.constants_per_domain[x]) <= 10 or x in constant_domains else ('+', '-') for x in
                            definitions[pred]]
                for combo in itertools.product(*possible):
                    if len(set([x for x in combo])) == 1 or len([x for x in combo if x == '+']) == 0:
                        continue
                    rep = ["{}{}".format(x[0], x[1].lower()) for x in zip(combo, definitions[pred])]
                    bg_file.write(":- modeb(*,{}({})).\n".format(pred.lower(), ",".join(rep)))

        bg_file.write("\n")

        for pred in definitions:
            bg_file.write(":- determination({}/1,{}/{}).\n".format(target_pred.lower(), pred.lower(), len(definitions[pred])))

        bg_file.write("\n")

    def _prepare_negative_examples_features(self, labels, working_directory, test_predicate, max_constants_per_domain):
        neg_file = open("{}/train.n".format(working_directory), 'w')

        domains = [test_predicate]
        domains = [self.constants_per_domain[d] for d in domains]

        for comb in itertools.product(*domains):
            gr_item = ",".join([x.lower() for x in comb])
            if gr_item not in labels:
                neg_file.write("target_{}({}).\n".format(list(labels.values())[0].lower(), gr_item).replace("%", "").replace("\\", "").replace("=", ""))

        neg_file.close()


class FeatureReader:

    def __init__(self, predicate_defs, features_file, data_files, work_dir, prune=False, timeout=-1):
        self.predicate_defs = {}
        self.features = {}
        self.facts_per_predicate = {}
        self.working_directory = work_dir
        self.constants_per_domain = {}
        self.timeout = int(timeout * 60 * 60)
        self.features_to_keep = []
        self.prune_features = prune

        self._read_predicate_defs(predicate_defs)
        self._read_features(features_file)
        for file in data_files:
            self._load_data(file)

        #self.problog_engine, self.db = self.start_prolog_process()

    def _read_predicate_defs(self, file_to_use):
        for line in open(file_to_use).readlines():
            if len(line) > 2:
                pred, args = line.strip().replace(")", "").split("(")
                args = args.split(",")

                self.predicate_defs[pred.lower()] = [x.lower() for x in args]

    def _read_features(self, file_to_use):
        feat_count = 0
        covered_bodies = set()
        for line in open(file_to_use).readlines():
            if len(line) > 2:
                tmp = line.strip()
                split_ind = tmp.find(":")
                ind, rule = tmp[:split_ind], tmp[split_ind+1:].strip()

                head, body = rule.split(":-")
                head_pred_name, head_pred_args = head.replace(")", "").split("(")
                head_pred_args = head_pred_args.split(",")
                body = body.replace(".", "")

                queriable_vars = set([x for x in head_pred_args if body.count(x) > 0])
                var_domains = {}

                for tup in zip(head_pred_args, self.predicate_defs[head_pred_name]):
                    var_domains[tup[0]] = tup[1]

                if body not in covered_bodies:
                    for v_item in queriable_vars:
                        new_feature = {"head_domain": var_domains[v_item],
                                       "body": body,
                                       "name": "feature{}".format(feat_count),
                                       "queriable": v_item}
                        self.features[feat_count] = new_feature
                        feat_count += 1
                        covered_bodies.add(body)

    def feature_count(self):
        return len(self.features)

    def _load_data(self, data_file):
        for line in open(data_file).readlines():
            if len(line) > 2:
                pred, args = line.strip().replace("%", "").replace("\\", "").replace("=", "").replace(")", "").split("(")
                pred = pred.lower()

                if pred not in self.facts_per_predicate:
                    self.facts_per_predicate[pred] = set()

                self.facts_per_predicate[pred].add(tuple([x.lower() for x in args.split(",")]))

                for (item, dom) in zip(args.split(','), self.predicate_defs[pred]):
                    if dom not in self.constants_per_domain:
                        self.constants_per_domain[dom] = set()
                    self.constants_per_domain[dom].add(item)

    def read_new_data(self, data_files):
        self.facts_per_predicate = {}
        for f in data_files:
            self._load_data(f)

    def get_constants(self):
        return self.constants_per_domain

    def prepare_prolog_file(self, feature, fact_filename, result_filename, entities_to_consider):
        db_file = open(fact_filename, 'w')

        for pred in self.facts_per_predicate:
            for gr in self.facts_per_predicate[pred]:
                db_file.write("{}({}).\n".format(pred, ','.join(gr)).replace("%", "").replace("\\", "").replace("=", ""))
        db_file.write("{}({}) :- {}.\n".format(feature['name'], feature['queriable'], feature['body']))
        #db_file.write("write_list(Stream,[]).\n")
        #db_file.write("write_list(Stream,[Head|List]) :- write(Stream,Head),nl(Stream),write_list(Stream,List).\n")
        #db_file.write(":- findall(X,{}(X),List),open('{}',write,Stream),write_list(Stream,List),close(Stream).\n".format(feature['name'], result_filename))

        for ent in entities_to_consider:
            db_file.write(
                ":- ({}({}) -> (open('{}',append,Stream),write(Stream,'{}'),nl(Stream),close(Stream));true).\n".format(feature['name'], ent.lower(), result_filename, ent.lower()))

        db_file.close()

    def read_prolog_result(self, result_filename):
        if not os.path.exists(result_filename):
            return set()

        entities = set()
        in_file = open(result_filename)

        for line in in_file:
            if len(line) > 2:
                entities.add(line.strip())

        in_file.close()
        return entities

    def run_prolog(self, fact_filename, directory):
        cmd = "yap -L {}".format(fact_filename)

        proc = subprocess.Popen(cmd, shell=True, cwd=directory, preexec_fn=os.setsid)

        if self.timeout:
            try:
                proc.wait(timeout=self.timeout)
            except Exception:
                os.killpg(os.getpgid(proc.pid), signal.SIGTERM)
        else:
            proc.wait()

    def get_feature_vector(self, entity_list, type, start_with_features=False):
        feature_vectors = {}
        feature_coverage = {}
        for ent in entity_list:
            feature_vectors[ent.replace("%", '').replace('\\', '').replace('=', '').lower()] = np.array(
                [False] * len(self.features), dtype=np.bool_)

        features_to_use = self.features
        if start_with_features:
            features_to_use = self.features_to_keep

        for feature_ind in features_to_use:
            feature = self.features[feature_ind]

            if type.lower() == feature['head_domain'].lower():
                data_file = self.working_directory + "/data_{}.pl".format(id(self))
                result_name = "{}_result_{}.txt".format(feature['name'], id(self))
                self.prepare_prolog_file(feature, data_file, result_name, entity_list)
                self.run_prolog(data_file, self.working_directory)
                entities = self.read_prolog_result(self.working_directory + "/" + result_name)
                if os.path.exists(self.working_directory + "/" + result_name):
                    os.remove(self.working_directory + "/" + result_name)
                os.remove(data_file)

                has_equivalent = False

                if not start_with_features and self.prune_features:
                    for item in feature_coverage:
                        if feature_coverage[item] == entities:
                            has_equivalent = True

                if not start_with_features and not has_equivalent:
                    feature_coverage[feature_ind] = entities
                    self.features_to_keep.append(feature_ind)

                if not has_equivalent:
                    for result_entity in entities:
                        if result_entity.lower() in entity_list:
                            feature_vectors[result_entity.lower()][feature_ind] = True

        #for ent in entity_list:
        #    feature_vectors[ent.replace("%", '').replace('\\', '').replace('=', '').lower()] = np.array(
        #        [False] * len(feature_coverage), dtype=np.bool_)

        print("After pruning, {} feature left, out of {}".format(len(feature_coverage), len(self.features)))

        #for ind, feat in enumerate(self.features_to_keep):
        #    for ent in feature_coverage[feat]:
        #        if ent.lower() in entity_list:
        #            feature_vectors[ent.lower()][ind] = True

        return feature_vectors

    """
    def _get_feature_vector(self, entity_list, type):
        feature_vectors = {}
        for ent in entity_list:
            feature_vectors[ent.replace("%", '').replace('\\', '').replace('=', '').lower()] = np.array([False]*len(self.features), dtype=np.bool_)

        print("          started", end='')
        for feature_ind in self.features:
            feature = self.features[feature_ind]

            if feature_ind % 10 == 0:
                print("feat{}/{} ".format(feature_ind, len(self.features)), end='')

            if type.lower() == feature['head_domain'].lower():

                for item in entity_list:
                    problog_query = Term(feature['name'], Term(item.lower()))

                    try:
                        res = self.problog_engine.query(self.db, problog_query)

                        if bool(res):
                            feature_vectors[item.lower()][feature_ind] = True

                    except Exception:
                        pass

        return feature_vectors

    def _get_feature_vector_batch(self, entity_list, type):
        feature_vectors = {}
        for ent in entity_list:
            feature_vectors[ent.replace("%", '').replace('\\', '').replace('=', '').lower()] = np.array([False]*len(self.features), dtype=np.bool_)

        for feature_ind in self.features:
            feature = self.features[feature_ind]

            if type.lower() == feature['head_domain'].lower():
                problog_query = Term(feature['name'], None)

                try:
                    res = self.problog_engine.query(self.db, problog_query)

                    for arg in res:
                        if arg[0] in feature_vectors:
                            feature_vectors[arg[0]][feature_ind] = True
                        else:
                            feature_vectors[arg[0]] = np.array([False]*len(self.features), dtype=np.bool_)
                            feature_vectors[arg[0]][feature_ind] = True
                            # print("cannot find {} feature vector".format(arg[0]))
                except Exception:
                    pass

        return feature_vectors
    """


class AlephFeatureGeneratorDisc(AlephFeatureGenerator):

    def _individual_run(self, conf):
        predicate_definitions = self.predicate_definitions.copy()
        test_predicate = conf['test']
        del predicate_definitions[test_predicate]

        if test_predicate not in self.facts_per_predicates:
            return {}

        final_labels = {}
        for gr_item in self.facts_per_predicates[test_predicate]:
            elem = ','.join([x.lower() for x in gr_item])
            final_labels[elem] = test_predicate

        constants = self.constants_per_domain
        facts = self.facts_per_predicates.copy()
        del facts[test_predicate]
        current_working_directory = self.working_directory + "/aleph_{}".format(conf['test'])

        if not os.path.exists(current_working_directory):
            os.mkdir(current_working_directory)

        min_pos = conf['mp']

        if isinstance(min_pos, float):
            min_pos = int(min_pos * len(final_labels))

        self._prepare_knowledge_file(predicate_definitions, final_labels, constants, facts, conf['cl'], conf['acc'], min_pos, current_working_directory)
        self._prepare_positive_examples(final_labels, current_working_directory)

        if self.parameters.get_parameter('aleph_evalfn') != 'posonly':
            self._prepare_negative_examples_features(final_labels, current_working_directory, test_predicate, self.max_negatives)

        cmd = "echo \"read_all(train). induce.\" " + "| yap -l {}/scripts/aleph.pl > log.txt 2> error.txt".format(
            os.path.abspath(os.path.dirname(__file__)))

        if not os.path.exists("{}/log.txt".format(current_working_directory)):
            proc = subprocess.Popen(cmd, shell=True, cwd=current_working_directory, preexec_fn=os.setsid)

            try:
                proc.wait(timeout=self.timeout)

                repeat_run = False

                for line in open("{}/error.txt".format(current_working_directory)).readlines():
                    if 'YAP OOOPS' in line:
                        repeat_run = True

                if repeat_run:
                    self._prepare_knowledge_file(predicate_definitions, final_labels, constants, facts, conf['cl'], conf['acc'], conf['mp'], current_working_directory, override_search="accuracy")
                    self._prepare_negative_examples_features(final_labels, current_working_directory, test_predicate, self.max_negatives)

                    proc = subprocess.Popen(cmd, shell=True, cwd=current_working_directory, preexec_fn=os.setsid)
                    proc.wait()

            except Exception:
                os.killpg(os.getpgid(proc.pid), signal.SIGTERM)

                self._prepare_knowledge_file(predicate_definitions, final_labels, constants, facts, conf['cl'], conf['acc'],
                                             conf['mp'], current_working_directory, override_search="coverage")
                self._prepare_negative_examples_features(final_labels, current_working_directory, test_predicate,
                                                         self.max_negatives)

                proc = subprocess.Popen(cmd, shell=True, cwd=current_working_directory, preexec_fn=os.setsid)
                try:
                    proc.wait(timeout=self.timeout)

                except Exception:
                    os.killpg(os.getpgid(proc.pid), signal.SIGTERM)
                    #proc.kill()

        print("                             reading from {}/log.txt".format(current_working_directory))
        for line in open("{}/log.txt".format(current_working_directory)).readlines():
            if line.startswith('feature'):
                tmp = line.strip().replace("feature(", "", 1).replace(")).", "")
                comma = tmp.find(',')
                tmp = tmp[comma + 2:]
                self.rules[len(self.rules)] = tmp