from learners.Learner import Learner
import numpy as np


class PropositionalLearner(Learner):

    def __init__(self, parameters):
        Learner.__init__(self, parameters)
        self.embeddings = {}
        self.labels = {}
        self.labels_dictionary = {}
        self.link_prediction = False

    def _get_label_id(self, label):
        if label not in self.labels_dictionary:
            self.labels_dictionary[label] = len(self.labels_dictionary)
        return self.labels_dictionary[label]

    def _read_embeddings(self, embedding_file):
        for line in open(embedding_file).readlines():
            if len(line) < 3:
                pass
            ent, emb = line.strip().split()
            emb = [float(x) for x in emb.split(",")]

            self.embeddings[ent] = np.array(emb)

    def _read_labels(self, labels_file):
        for line in open(labels_file).readlines():
            if len(line) < 3:
                continue
            entity, label = line.strip().split()
            self.labels[entity] = self._get_label_id(label)
            if ',' in entity:
                self.link_prediction = True

    def _get_labels(self, labels_file):
        labels = {}
        for line in open(labels_file).readlines():
            entity, label = line.strip().split()
            labels[entity] = self._get_label_id(label)

            if ',' in entity:
                self.link_prediction = True

        return labels
