from learners.PropositionalLearner import PropositionalLearner
from sklearn.neighbors import KNeighborsClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.svm import SVC
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics.classification import accuracy_score
from sklearn.metrics import roc_auc_score
import numpy as np
#from scoop import futures
import random
import itertools
from progress.bar import Bar


class PropositionalPrediction(PropositionalLearner):

    def __init__(self, parameters):
        super().__init__(parameters)
        self.current_training_data = []
        self.current_training_labels = []
        self.do_sampling = False
        self.evaluation = parameters.get_parameter('class_evaluation')
        self.progress_bar = Bar('Processing')

    def do_subsumple(self, n_samples):
        self.do_sampling = n_samples

    def _fit_with_params(self, training_data, labels):
        pass

    def learn_and_evaluate(self, training_data_file, training_labels_file, test_data_file="", test_labels_file="",
                           definitions=""):
        self._read_embeddings(training_data_file)
        self._read_labels(training_labels_file)
        self._read_labels(test_labels_file)

        data = []
        labels = []
        tmp_labels = self._get_labels(training_labels_file)

        if not self.link_prediction:
            for elem in tmp_labels:
                if elem in self.embeddings and elem in tmp_labels:
                    data.append(self.embeddings[elem])
                    labels.append(tmp_labels[elem])
                else:
                    pass
                    #print("found element {} without embedding or label".format(elem))
        else:
            for elem in tmp_labels:
                ent1, ent2 = elem.split(",")
                ent1_ex = ent1 in self.embeddings
                ent2_ex = ent2 in self.embeddings

                if ent1_ex and ent2_ex:
                    data.append(np.concatenate((self.embeddings[ent1], self.embeddings[ent2])))
                    labels.append(1.)

            if self.do_sampling:
                all_elements = set()
                for ent in tmp_labels:
                    for x in ent.split(","):
                        all_elements.add(x)

                combo = []
                for _ in range(list(tmp_labels.keys())[0].count(',') + 1):
                    random.shuffle(all_elements)
                    combo.append(all_elements[:self.do_sampling])

                for tup in itertools.product(*combo):
                    link = ",".join([x.lower() for x in tup])
                    if link not in tmp_labels:
                        ent1 = tup[0]
                        ent2 = tup[1]
                        ent1_ex = ent1 in self.embeddings
                        ent2_ex = ent2 in self.embeddings

                        if ent1_ex and ent2_ex:
                            data.append(np.concatenate((self.embeddings[ent1], self.embeddings[ent2])))
                            labels.append(0.)

        self.current_training_data = np.array(data)
        self.current_training_labels = np.array(labels)

        model = self._fit_with_params(self.current_training_data, np.array(labels))

        test_data = []
        test_labels = []
        tmp_test_labels = self._get_labels(test_labels_file)

        if not self.link_prediction:
            for elem in tmp_test_labels:
                if elem in self.embeddings and elem in tmp_test_labels:
                    test_data.append(self.embeddings[elem])
                    test_labels.append(tmp_test_labels[elem])
                else:
                    print("found element {} without embedding or label".format(elem))
        else:
            for elem in tmp_test_labels:
                ents = elem.split(",")
                ent1_ex = ents[0] in self.embeddings
                ent2_ex = ents[1] in self.embeddings

                if ent1_ex and ent2_ex:
                    test_data.append(np.concatenate((self.embeddings[ents[0]], self.embeddings[ents[1]])))
                    test_labels.append(1.)

        if self.evaluation == 'accuracy':
            predictions = model.predict(test_data)
            return accuracy_score(test_labels, predictions)
        elif self.evaluation == 'auc':
            prediction_matrix = model.predict_proba(test_data)
            final_labels = []
            num_labels = len(prediction_matrix[0])
            for i in range(len(test_labels)):
                tmp = np.array([0]*num_labels)
                tmp[test_labels[i]] = 1.
                final_labels.append(tmp)

            final_labels = np.array(final_labels)

            return roc_auc_score(final_labels, prediction_matrix)
        else:
            raise Exception("unknown evaluation measure: {}".format(self.evaluation))


class kNN(PropositionalPrediction):

    def __init__(self, parameters):
        super().__init__(parameters)

    def individual_run_knn(self, conf):
        # training_data = shared.getConst('training')
        # labels = shared.getConst('labels')

        X_train = self.current_training_data[conf['training_index']]
        X_test = self.current_training_data[conf['test_index']]
        y_train = self.current_training_labels[conf['training_index']]
        y_test = self.current_training_labels[conf['test_index']]

        dt = KNeighborsClassifier(n_neighbors=conf['n'], weights=conf['w'])
        dt.fit(X_train, y_train)
        acc = 0.0
        try:
            if self.evaluation == 'accuracy':
                predictions = dt.predict(X_test)
                acc = accuracy_score(y_test, predictions)
            elif self.evaluation == 'auc':
                prediction_matrix = dt.predict_proba(X_test)
                final_labels = []
                num_labels = len(prediction_matrix[0])
                for i in range(len(y_test)):
                    tmp = np.array([0] * num_labels)
                    tmp[y_test[i]] = 1.
                    final_labels.append(tmp)

                final_labels = np.array(final_labels)

                acc = roc_auc_score(final_labels, prediction_matrix)
            else:
                raise Exception("unknown evaluation measure: {}".format(self.evaluation))
            #predictions = dt.predict(X_test)
            #acc = accuracy_score(y_test, predictions)
        except Exception:
            pass

        self.progress_bar.next()
        return {"n": conf['n'], 'w': conf['w'], 'accuracy': acc}

    def _fit_with_params(self, training_data, labels):
        results = {}
        kf = StratifiedKFold(n_splits=self.parameters.get_parameter("n_splits"))

        # shared.setConst(training=training_data)
        # shared.setConst(labels=labels)

        confs_knn = []

        for train_index, test_index in kf.split(training_data, labels):
            for n in self.parameters.get_parameter("n_neighbors"):
                for w in self.parameters.get_parameter("weights"):
                    confs_knn.append({"training_index": train_index, "test_index": test_index, "n": n, "w": w})

        self.progress_bar = Bar('Processing', max=len(confs_knn), suffix='%(progress)d  %(percent)d%%  %(avg)d')
        accuracies = map(self.individual_run_knn, confs_knn)
        self.progress_bar.finish()

        for item in accuracies:
            param_dict = (("n_neighbors", item['n']), ('weights', item['w']))

            if param_dict not in results:
                results[param_dict] = []
            results[param_dict].append(item['accuracy'])

        summaries = {}
        for elem in results:
            summaries[elem] = np.array(results[elem]).mean()

        best_params = summaries.keys()
        best_params = dict(sorted(best_params, key=lambda x: summaries[x], reverse=True)[0])

        final_dt = KNeighborsClassifier(n_neighbors=best_params["n_neighbors"], weights=best_params["weights"])
        final_dt.fit(training_data, labels)

        return final_dt


class DecisionTree(PropositionalPrediction):

    def __init__(self, parameters):
        super().__init__(parameters)

    def individual_run_dt(self, conf):
        # training_data = shared.getConst('training')
        # labels = shared.getConst('labels')

        X_train = self.current_training_data[conf['training_index']]
        X_test = self.current_training_data[conf['test_index']]
        y_train = self.current_training_labels[conf['training_index']]
        y_test = self.current_training_labels[conf['test_index']]

        dt = DecisionTreeClassifier(criterion=conf['c'], max_depth=conf['d'], min_samples_leaf=conf['s'],
                                    min_impurity_decrease=conf['i'], presort=True)

        dt.fit(X_train, y_train)
        acc = 0.0
        try:
            if self.evaluation == 'accuracy':
                predictions = dt.predict(X_test)
                acc = accuracy_score(y_test, predictions)
            elif self.evaluation == 'auc':
                prediction_matrix = dt.predict_proba(X_test)
                final_labels = []
                num_labels = len(prediction_matrix[0])
                for i in range(len(y_test)):
                    tmp = np.array([0] * num_labels)
                    tmp[y_test[i]] = 1.
                    final_labels.append(tmp)

                final_labels = np.array(final_labels)

                acc = roc_auc_score(final_labels, prediction_matrix)
            else:
                raise Exception("unknown evaluation measure: {}".format(self.evaluation))
            #predictions = dt.predict(X_test)
            #acc = accuracy_score(y_test, predictions)
        except Exception:
            pass

        self.progress_bar.next()
        return {'c': conf['c'], 'd': conf['d'], 's': conf['s'], 'i': conf['i'], 'accuracy': acc}

    def _fit_with_params(self, training_data, labels):
        results = {}
        kf = StratifiedKFold(n_splits=self.parameters.get_parameter("n_splits"))

        # if shared.getConst('training') is None:
        #    shared.setConst(training=training_data)
        # if shared.getConst('labels') is None:
        #     shared.setConst(labels=labels)

        confs_dt = []

        for train_index, test_index in kf.split(training_data, labels):
            for c in self.parameters.get_parameter("criterion"):
                for d in self.parameters.get_parameter("max_depth"):
                    for s in self.parameters.get_parameter("min_samples_leaf"):
                        for i in self.parameters.get_parameter("min_impurity_decrease"):
                            confs_dt.append({"training_index": train_index, "test_index": test_index, 'c': c, 'd': d,
                                          's': s, 'i': i})

        self.progress_bar = Bar('Processing', max=len(confs_dt), suffix='%(progress)d  %(percent)d%%  %(avg)d')
        accuracies = map(self.individual_run_dt, confs_dt)
        self.progress_bar.finish()

        for item in accuracies:
            param_dict = (("criterion", item['c']), ('max_depth', item['d']), ('min_samples_leaf', item['s']),
                          ('min_impurity_decrease', item['i']))

            if param_dict not in results:
                results[param_dict] = []
            results[param_dict].append(item['accuracy'])

        summaries = {}
        for elem in results:
            summaries[elem] = np.array(results[elem]).mean()

        best_params = summaries.keys()
        best_params = dict(sorted(best_params, key=lambda x: summaries[x], reverse=True)[0])

        final_dt = DecisionTreeClassifier(criterion=best_params["criterion"], max_depth=best_params["max_depth"],
                                          min_impurity_decrease=best_params["min_impurity_decrease"],
                                          min_samples_leaf=best_params["min_samples_leaf"], presort=True)
        final_dt.fit(training_data, labels)

        return final_dt


class SVM(PropositionalPrediction):

    def __init__(self, parameters):
        super().__init__(parameters)

    def individual_run_svm(self, conf):
        X_train = self.current_training_data[conf['training_index']]
        X_test = self.current_training_data[conf['test_index']]
        y_train = self.current_training_labels[conf['training_index']]
        y_test = self.current_training_labels[conf['test_index']]

        dt = SVC(C=conf['c'], kernel=conf['k'], degree=conf['d'], decision_function_shape='ovr')
        dt.fit(X_train, y_train)
        acc = 0.0
        try:
            #predictions = dt.predict(X_test)
            #acc = accuracy_score(y_test, predictions)
            if self.evaluation == 'accuracy':
                predictions = dt.predict(X_test)
                acc = accuracy_score(y_test, predictions)
            elif self.evaluation == 'auc':
                prediction_matrix = dt.predict_proba(X_test)
                final_labels = []
                num_labels = len(prediction_matrix[0])
                for i in range(len(y_test)):
                    tmp = np.array([0] * num_labels)
                    tmp[y_test[i]] = 1.
                    final_labels.append(tmp)

                final_labels = np.array(final_labels)

                acc = roc_auc_score(final_labels, prediction_matrix)
            else:
                raise Exception("unknown evaluation measure: {}".format(self.evaluation))
        except Exception:
            pass

        self.progress_bar.next()
        return {'c': conf['c'], 'k': conf['k'], 'd': conf['d'], 'accuracy': acc}

    def _fit_with_params(self, training_data, labels):
        results = {}
        kf = StratifiedKFold(n_splits=self.parameters.get_parameter("n_splits"))

        confs_svm = []

        for train_index, test_index in kf.split(training_data, labels):
            for k in self.parameters.get_parameter("kernel"):
                for d in self.parameters.get_parameter("degree"):
                    for c in self.parameters.get_parameter("C"):
                        confs_svm.append({"training_index": train_index, "test_index": test_index, "k": k, "d": d, 'c': c})

        self.progress_bar = Bar('Processing', max=len(confs_svm), suffix='%(progress)d  %(percent)d%%  %(avg)d')
        accuracies = map(self.individual_run_svm, confs_svm)
        self.progress_bar.finish()

        for item in accuracies:
            param_dict = (("kernel", item['k']), ('degree', item['d']), ('C', item['c']))

            if param_dict not in results:
                results[param_dict] = []
            results[param_dict].append(item['accuracy'])

        summaries = {}
        for elem in results:
            summaries[elem] = np.array(results[elem]).mean()

        best_params = summaries.keys()
        best_params = dict(sorted(best_params, key=lambda x: summaries[x], reverse=True)[0])

        final_dt = SVC(C=best_params["C"], kernel=best_params["kernel"], degree=best_params["degree"],
                       decision_function_shape='ovr')
        final_dt.fit(training_data, labels)

        return final_dt


class LogisticReg(PropositionalPrediction):

    def __init__(self, parameters):
        super().__init__(parameters)

    def individual_run_svm(self, conf):
        X_train = self.current_training_data[conf['training_index']]
        X_test = self.current_training_data[conf['test_index']]
        y_train = self.current_training_labels[conf['training_index']]
        y_test = self.current_training_labels[conf['test_index']]

        acc = 0.0
        try:
            dt = LogisticRegression(C=conf['C'], dual=conf['dual'], fit_intercept=conf['fit_intercept'], max_iter=1000,
                                    penalty=conf['penalty'], multi_class=conf['multi_class'], solver=conf['solver'])
            dt.fit(X_train, y_train)
            #predictions = dt.predict(X_test)
            #acc = accuracy_score(y_test, predictions)
            if self.evaluation == 'accuracy':
                predictions = dt.predict(X_test)
                acc = accuracy_score(y_test, predictions)
            elif self.evaluation == 'auc':
                prediction_matrix = dt.predict_proba(X_test)
                final_labels = []
                num_labels = len(prediction_matrix[0])
                for i in range(len(y_test)):
                    tmp = np.array([0] * num_labels)
                    tmp[y_test[i]] = 1.
                    final_labels.append(tmp)

                final_labels = np.array(final_labels)

                acc = roc_auc_score(final_labels, prediction_matrix)
            else:
                raise Exception("unknown evaluation measure: {}".format(self.evaluation))
        except Exception:
            pass

        return {'C': conf['C'], 'dual': conf['dual'], 'fit_intercept': conf['fit_intercept'], 'penalty': conf['penalty'],
                'solver': conf['solver'], 'multi_class': conf['multi_class'], 'accuracy': acc}

    def _fit_with_params(self, training_data, labels):
        results = {}
        kf = StratifiedKFold(n_splits=self.parameters.get_parameter("n_splits"))

        confs_svm = []

        for train_index, test_index in kf.split(training_data, labels):
            for c in self.parameters.get_parameter("C"):
                for dual in self.parameters.get_parameter("dual"):
                    for fi in self.parameters.get_parameter("fit_intercept"):
                        for p in self.parameters.get_parameter("penalty"):
                            for s in self.parameters.get_parameter("lr_solver"):
                                for mc in self.parameters.get_parameter("lr_multiclass"):
                                    confs_svm.append({"training_index": train_index, "test_index": test_index, "C": c,
                                                      "dual": dual, 'fit_intercept': fi, 'penalty': p, 'solver': s,
                                                      'multi_class': mc})

        accuracies = map(self.individual_run_svm, confs_svm)

        for item in accuracies:
            param_dict = (("C", item['C']), ('dual', item['dual']), ('fit_intercept', item['fit_intercept']),
                          ('penalty', item['penalty']), ('solver', item['solver']), ('multi_class', item['multi_class']))

            if param_dict not in results:
                results[param_dict] = []
            results[param_dict].append(item['accuracy'])

        summaries = {}
        for elem in results:
            summaries[elem] = np.array(results[elem]).mean()

        best_params = summaries.keys()
        best_params = dict(sorted(best_params, key=lambda x: summaries[x], reverse=True)[0])

        final_dt = LogisticRegression(C=best_params["C"], dual=best_params['dual'], fit_intercept=best_params['fit_intercept'],
                                      penalty=best_params['penalty'], solver=best_params['solver'], multi_class=best_params['multi_class'])
        final_dt.fit(training_data, labels)

        return final_dt