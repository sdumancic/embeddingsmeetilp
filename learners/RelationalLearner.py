from learners.Learner import Learner
import os
import subprocess
from learners.hybrid.EmbeddingAbstractor import EmbeddingAbstractor
import numpy as np
import random
import itertools
import signal
import pickle
from learners.LearnerSettings import constant_domains
from problog.engine import DefaultEngine
from problog.logic import *
from problog.program import PrologString
from sklearn.neighbors import KNeighborsClassifier
from progress.bar import Bar
from boostsrl import boostsrl


class RelationalLearner(Learner):

    def __init__(self, parameters, exclude_negatives=False):
        super().__init__(parameters)
        self.predicate_definitions = {}
        self.facts_per_predicates = {}
        self.constants_per_domain = {}
        self.labels = {}
        self.best_parameters = {}
        self.entity_order_for_labels = []
        self.link_prediction = False
        self.do_sampling = 0
        self.exclude_negatives = exclude_negatives
        self.predicate_bias = {}
        self.progress_bar = Bar('Processing')
        self.lookaheads = []
        self.multiplicative_factor_for_negatives = 2

    def read_bias(self, bias):
        self.predicate_bias = bias

    def do_negative_sampling(self, max_number):
        self.do_sampling = max_number

    def exclude_negative_examples(self):
        self.exclude_negatives = True

    def include_negative_examples(self):
        self.exclude_negatives = False

    def _read_definitions(self, definitions_file):
        predicate_defs = {}

        for line in open(definitions_file).readlines():
            if len(line) < 3:
                continue
            pred, doms = line.strip().replace(")", "").split("(")
            predicate_defs[pred.lower()] = [x.lower() for x in doms.split(",")]

        return predicate_defs

    def _read_facts(self, facts_file, predicate_definitions):
        facts_per_predicate = {}
        constants_per_domain = {}

        for line in open(facts_file).readlines():
            if len(line) < 3:
                continue

            pred, args = line.strip().replace(")", "").split("(")
            args = [x.lower() for x in args.split(",")]
            pred = pred.lower()

            if pred not in facts_per_predicate:
                facts_per_predicate[pred] = set()

            facts_per_predicate[pred].add(tuple(args))

            for (c, d) in zip(args, predicate_definitions[pred]):
                if d not in constants_per_domain:
                    constants_per_domain[d] = set()
                constants_per_domain[d].add(c)

        return facts_per_predicate, constants_per_domain

    def _read_labels(self, labels_file):
        labels = {}

        for line in open(labels_file).readlines():
            if len(line) < 3:
                continue

            pred, args = line.strip().replace(")", "").split("(")
            pred = pred.lower()
            args = args.lower()

            if ',' in args:
                self.link_prediction = True
                args = [x.lower() for x in args.split(",")]
                labels[','.join(args)] = "pos"
            else:
                labels[args] = pred

        if self.link_prediction and not self.exclude_negatives:
            all_entities = set()
            if 'entity' in self.constants_per_domain:
                for item in self.constants_per_domain['entity']:
                    all_entities.add(item)
            else:
                for item in labels:
                    for x in item.split(','):
                        all_entities.add(x)

            ent_comb = [all_entities, all_entities]

            if self.do_sampling:
                ent_comb = []
                all_entities = list(all_entities)
                random.shuffle(all_entities)
                ent_comb.append(all_entities[:self.do_sampling])
                random.shuffle(all_entities)
                ent_comb.append(all_entities[:self.do_sampling])

            neg_count = 0
            max_neg = min(int(len(labels) * self.multiplicative_factor_for_negatives),
                          len(all_entities) * len(all_entities))

            try:
                for tup in random.sample(list(itertools.product(*ent_comb)), max_neg):
                    frmtd = ",".join([x.lower() for x in tup])

                    if frmtd not in labels and neg_count <= max_neg:
                        labels[frmtd] = "neg"
                        neg_count += 1
            except Exception:
                max_neg = len(labels) + 1
                for tup in random.sample(list(itertools.product(*ent_comb)), max_neg):
                    frmtd = ",".join([x.lower() for x in tup])

                    if frmtd not in labels and neg_count <= max_neg:
                        labels[frmtd] = "neg"
                        neg_count += 1

        return labels

    def read_lookaheads(self, values):
        self.lookaheads = values

    # training_data is not (facts_per_pred, pred_definitions); provide labels if you want to test the model - otherwise,
    #        it return the best parameters found so far
    def _fit_with_params(self, training_data, labels):
        pass

    def _enhance_data(self):
        pass

    #  training_data_file and training_labels_file are lists of files
    def learn_and_evaluate(self, training_data_file, training_labels_file, test_data_file="", test_labels_file="",
                           definitions="", exclude_test_negatives=False, append_training_labels=False):
        self.predicate_definitions = self._read_definitions(definitions)

        for t_file in training_data_file:
            facts, constants = self._read_facts(t_file, self.predicate_definitions)

            for pred in facts:
                if pred not in self.facts_per_predicates:
                    self.facts_per_predicates[pred] = set()
                for elem in facts[pred]:
                    self.facts_per_predicates[pred].add(elem)

            for d in constants:
                if d not in self.constants_per_domain:
                    self.constants_per_domain[d] = set()
                for c in constants[d]:
                    self.constants_per_domain[d].add(c)

        for l_file in training_labels_file:
            labels = self._read_labels(l_file)

            for item in labels:
                self.labels[item] = labels[item]

        self.entity_order_for_labels = list(self.labels.keys())

        self._enhance_data()
        self.best_parameters = self._fit_with_params((self.facts_per_predicates, self.predicate_definitions), [])

        test_facts, test_constants = self._read_facts(test_data_file, self.predicate_definitions)

        for pred in test_facts:
            if pred not in self.facts_per_predicates:
                self.facts_per_predicates[pred] = set()
            for elem in test_facts[pred]:
                self.facts_per_predicates[pred].add(elem)

        for d in test_constants:
            for e in test_constants[d]:
                self.constants_per_domain[d].add(e)

        if exclude_test_negatives:
            self.exclude_negative_examples()

        test_labels = self._read_labels(test_labels_file)

        if append_training_labels:
            for item in test_labels:
                self.labels[item] = test_labels[item]

        final_performance = self._fit_with_params((self.facts_per_predicates, self.predicate_definitions), test_labels)

        return final_performance


class TILDE(RelationalLearner):

    def __init__(self, parameters, working_directory, exclude_negatives=False, final_run_induce=False, special_predict=None, keep_log=False):
        # special target: None -- old setup; 1 -- (+,-) without pos/neg; 2 -- (-,+) without pos/neg
        super().__init__(parameters, exclude_negatives)
        self.working_directory = working_directory
        self.timeout = parameters.get_parameter('timeout')*60
        self.final_run_nfold = final_run_induce
        self.special_predict_predicate = special_predict
        self.keep_log = keep_log

    def _find_target_domain(self, labels, constants_per_domain):
        found_domain = ""
        example_object = list(labels)[0].lower()

        for dom in constants_per_domain:
            if example_object in constants_per_domain[dom]:
                found_domain = dom

        if found_domain == "":
            for o in labels:
                if found_domain != "":
                    continue
                for dom in constants_per_domain:
                    if found_domain != "":
                        continue
                    if o.lower() in constants_per_domain[dom]:
                        found_domain = dom

        return found_domain

    def _prepare_settings_file(self, definitions, labels, constants, heuristic, minimal_cases, accuracy, working_directory):
        settings_file = open("{}/train.s".format(working_directory), 'w')
        settings_file.write("load(key).\n")
        settings_file.write("talking(4).\n")
        settings_file.write("output_options([c45,prolog,c45c]).\n")
        settings_file.write("bias(rmode).\n")
        if self.special_predict_predicate is None:
            settings_file.write("classes([{}]).\n".format(",".join(set([x.lower() for x in labels.values()]))))
        settings_file.write("typed_language(yes).\n")
        settings_file.write("heuristic({}).\n".format(heuristic))
        settings_file.write("pruning(none).\n")
        settings_file.write("tilde_mode(classify).\n")
        settings_file.write("minimal_cases({}).\n".format(minimal_cases))
        settings_file.write("accuracy({}).\n".format(accuracy))
        target_doms = []

        if self.special_predict_predicate:
            target_doms = ["{}".format(self._find_target_domain([y.split(",")[x] for y in labels], constants).lower()) for x in range(list(labels.keys())[0].count(",") + 1)]
            assert(len(target_doms) == 2, "")
            if self.special_predict_predicate == 1:
                settings_file.write("predict(targetpred(+{},-{})).\n".format(target_doms[0], target_doms[1]))
            else:
                settings_file.write("predict(targetpred(-{},+{})).\n".format(target_doms[0], target_doms[1])) 
        else:
            if self.link_prediction:
                target_doms = ["+{}".format(self._find_target_domain([y.split(",")[x] for y in labels], constants).lower()) for x in range(list(labels.keys())[0].count(",") + 1)]
            else:
                target_doms = ["+{}".format(self._find_target_domain(list(labels.keys()), constants).lower())]
            settings_file.write("predict(targetpred({},-class)).\n".format(','.join(target_doms)))
        settings_file.write("\n")

        for pred in definitions:
            settings_file.write("type({}({})).\n".format(pred.lower(), ",".join([x.lower() for x in definitions[pred]])))

        settings_file.write("\n")

        for pred in definitions:
            rep = []
            if len(definitions[pred]) == 1:
                rep = ["rmode({}(+V)).\n".format(pred.lower)]
            else:
                possible = [
                    ('+', '-', '#') if len(self.constants_per_domain[x]) < 10 or x in constant_domains else ('+', '-')
                    for x in definitions[pred]]
                for combo in itertools.product(*possible):
                    if len([x for x in combo if x == '+']) == 0:  # (len(set([x for x in combo])) == 1) or
                        continue
                    t_rep = ["{}V{}".format(x[0], x[1]) if x[0] != '#' else '#' for x in zip(combo, range(len(definitions[pred])))]
                    rep.append("rmode({}({})).\n".format(pred.lower(), ",".join(t_rep)))
                #vars = zip(["#" if x in constant_domains else "+-V" for x in definitions[pred]], range(len(definitions[pred])))
                #vars = ["{}{}".format(x[0], x[1]) if x[0] != '#' else '#' for x in vars]
                #rep = ["rmode({}({})).\n".format(pred.lower(), ",".join(vars))]

            for item in rep:
                settings_file.write(item)

        for item in self.lookaheads:
            settings_file.write("auto_lookahead({}).\n".format(item))

        settings_file.close()

    def _prepare_facts_and_labels_file(self, facts_per_predicate, training_labels, test_labels, working_directory):
        kbfile = open("{}/train.kb".format(working_directory), 'w')

        for pred in facts_per_predicate:
            for elem in facts_per_predicate[pred]:
                kbfile.write("{}({}).\n".format(pred.lower(), ",".join([x.lower() for x in elem])).replace("%", "").replace("\\", "").replace("=", ""))

        # not necessary, assumes that the test data is provided in facts_per_predicate
        # if len(test_data) > 0:
        #    for pred in test_data:
        #        for elem in test_data[pred]:
        #            kbfile.write("{}({}).\n".format(pred.lower(), ",".join([x.lower() for x in elem])))

        for item in training_labels:
            if self.special_predict_predicate:
                kbfile.write("targetpred({}).\n".format(item.lower()).replace("%", "").replace("\\", "").replace("=", ""))
            else:
                kbfile.write("targetpred({},{}).\n".format(item.lower(), training_labels[item].lower()).replace("%", "").replace("\\", "").replace("=", ""))

        if len(test_labels):
            for item in test_labels:
                if self.exclude_negatives and test_labels[item].lower() == 'neg':
                    pass
                else:
                    if self.special_predict_predicate:
                        kbfile.write("targetpred({}).\n".format(item.lower()).replace("%", "").replace("\\", "").replace("=", ""))
                    else:
                        kbfile.write("targetpred({},{}).\n".format(item.lower(), test_labels[item].lower()).replace("%", "").replace("\\", "").replace("=", ""))

        kbfile.close()

    def _prepare_background_file_training(self, working_directory):
        pass

    def _prepare_background_file(self, test_labels, working_directory):
        bg_file = open("{}/train.bg".format(working_directory), 'w')

        for item in test_labels:
            bg_file.write("testid({},testset).\n".format(item.lower()))

        bg_file.close()

    def _individual_run(self, conf):
        predicate_definitions = self.predicate_definitions
        final_labels = self.labels
        constants = self.constants_per_domain
        facts = self.facts_per_predicates
        current_working_directory = self.working_directory + "/tilde_{}_{}_{}".format(conf['h'], conf['mc'], conf['acc'])

        if not os.path.exists(current_working_directory):
            os.mkdir(current_working_directory)

        self._prepare_settings_file(predicate_definitions, final_labels, constants, conf['h'], conf['mc'], conf['acc'],
                                    current_working_directory)
        self._prepare_facts_and_labels_file(facts, final_labels, [], current_working_directory)

        self._prepare_background_file_training(current_working_directory)

        cmd = "echo \"nfold(tilde,4)\" | {}/bin/ace > log.txt 2> error.txt ".format(os.environ['ACE_ILP_ROOT']) 

        #if self.keep_log:
        #    cmd = cmd + " > log.txt 2> error.txt"
        #else: 
        #    cmd = cmd + " &> /dev/null"

        acc = 0.0

        if os.path.exists("{}/tilde/train.summary.uB".format(current_working_directory)):
            start_parsing_test = False
            for line in open("{}/tilde/train.summary.uB".format(current_working_directory)).readlines():
                if 'Testing' in line:
                    start_parsing_test = True
                elif start_parsing_test and 'Accuracy' in line:
                    tmp = line.split("(")[0]
                    acc = float(tmp.split(":")[1].strip())
            self.progress_bar.next()
            return {'h': conf['h'], 'mc': conf['mc'], 'acc': conf['acc'], 'accuracy': acc}

        proc = subprocess.Popen(cmd, shell=True, cwd=current_working_directory, preexec_fn=os.setsid)

        try:
            proc.wait(timeout=self.timeout)

            start_parsing_test = False
            for line in open("{}/tilde/train.summary.uB".format(current_working_directory)).readlines():
                if 'Testing' in line:
                    start_parsing_test = True
                elif start_parsing_test and 'Accuracy' in line:
                    tmp = line.split("(")[0]
                    acc = float(tmp.split(":")[1].strip())
            self.progress_bar.next()

            if not self.keep_log and os.path.exists("{}/log.txt".format(current_working_directory)):
                os.remove("{}/log.txt".format(current_working_directory))

            return {'h': conf['h'], 'mc': conf['mc'], 'acc': conf['acc'], 'accuracy': acc}

        except Exception:
            os.killpg(os.getpgid(proc.pid), signal.SIGTERM)
            self.progress_bar.next()
            return {'h': conf['h'], 'mc': conf['mc'], 'acc': conf['acc'], 'accuracy': 0.0}

    def _fit_with_params(self, training_data, labels):
        #  optimize TILDE only
        if len(labels) < 1:
            results = {}

            confs = []

            for h in self.parameters.get_parameter("heuristics"):
                for acc in self.parameters.get_parameter("accuracy"):
                    for mc in self.parameters.get_parameter("minimal_cases"):
                        confs.append({'h': h, 'acc': acc, 'mc': mc})

            self.progress_bar = Bar('Processing', max=len(confs), suffix='%(index)d / %(max)d  %(percent)d%%  avg time: %(avg)ds ETA: %(eta)ds')
            accuracies = map(self._individual_run, confs)
            self.progress_bar.finish()

            for item in accuracies:
                param_dict = (("heuristics", item['h']), ('accuracy', item['acc']), ('minimal_cases', item['mc']))

                if param_dict not in results:
                    results[param_dict] = []
                results[param_dict].append(item['accuracy'])

            summaries = {}
            for elem in results:
                summaries[elem] = np.array(results[elem]).mean()

            best_params = summaries.keys()
            best_params = sorted(best_params, key=lambda x: summaries[x], reverse=True)[0]

            print("FOUND BEST PARAMETERS: ", best_params)
            return dict(best_params)

        #  use the best parameters for the final evaluation
        else:
            predicate_definitions = training_data[1]
            final_labels = self.labels
            # for item in labels:
            #    final_labels[item] = labels[item]
            constants = self.constants_per_domain
            facts = training_data[0]

            print(" READING BEST PARAMETERS: ", self.best_parameters)
            h = self.best_parameters['heuristics']
            mc = self.best_parameters['minimal_cases']
            acc = self.best_parameters['accuracy']

            self._prepare_settings_file(predicate_definitions, final_labels, constants, h, mc, acc, self.working_directory)
            self._prepare_facts_and_labels_file(facts, final_labels, labels, self.working_directory)
            self._prepare_background_file(labels, self.working_directory)

            cmd = "echo \"loofl(tilde, [testset])\" | {}/bin/ace > log.txt 2> error.txt".format(os.environ['ACE_ILP_ROOT'])
            if self.final_run_nfold:
                cmd = "echo \"induce(tilde)\" | {}/bin/ace > log.txt 2> error.txt".format(os.environ['ACE_ILP_ROOT'])

            #if self.keep_log:
            #    cmd = cmd + " > log.txt 2> error.txt"
            #else:
            #    cmd = cmd + " &> /dev/null"

            if not os.path.exists("{}/tilde/train.summary.uL".format(self.working_directory)):
                proc = subprocess.Popen(cmd, shell=True, cwd=self.working_directory)
                proc.wait()

            if not self.keep_log and os.path.exists("{}/log.txt".format(self.working_directory)):
                os.remove("{}/log.txt".format(self.working_directory))

            acc = 0.0
            start_parsing_test = False
            read_file = "{}/tilde/train.summary.uL".format(self.working_directory)
            if self.final_run_nfold:
                read_file = "{}/tilde/train.out".format(self.working_directory)
            for line in open(read_file).readlines():
                if 'Testing' in line:
                    start_parsing_test = True
                elif start_parsing_test and 'Accuracy' in line:
                    tmp = line.split("(")[0]
                    acc = float(tmp.split(":")[1].strip())

            return acc


class ETilde(TILDE):

    def __init__(self, parameters, working_directory, method, exclude_negatives=False):
        super().__init__(parameters, working_directory, exclude_negatives)
        self.embeddingHandler = EmbeddingAbstractor(method, parameters.get_parameter("threshold"))
        self.subset_val = parameters.get_parameter('subset')

    def read_entity_embeddings(self, entity_embeddings_file):
        self.embeddingHandler.read_entity_embeddings(entity_embeddings_file)

    def load_entity_embeddings(self, entity_embedding_map):
        self.embeddingHandler.load_entity_embeddings(entity_embedding_map)

    def _prepare_background_file_training(self, working_directory):
        bg_file = open("{}/train.bg".format(working_directory), 'w')

        # euclidean_distance = [
        #    "euclidean_dist(List1,List2,Dist) :- euclidean_acc(List1,List2,0,SquaredDist), Dist is sqrt(SquaredDist).",
        #    "euclidean_acc([],[],Dist,Dist).",
        #    "euclidean_acc([X1|List1],[X2|List2],Current,SquaredDist) :- New is Current + (X1-X2)**2,euclidean_acc(List1,List2,New,SquaredDist)."
        # ]
        # bg_file.write("\n".join(euclidean_distance) + "\n")
        # bg_file.write("\n")

        # euclidean_alternative = [
        #    "euclidean_dist([P|Ps], [Q|Qs], D) :- sum_diff_sq(Ps, Qs, (P - Q)**2, R), D is sqrt(R).",
        #    "sum_diff_sq([], [], V, V).",
        #    "sum_diff_sq([P|Ps], [Q|Qs], V0, V+V0) :- sum_diff_sq(Ps, Qs, (P - Q)**2, V)."
        # ]
        # bg_file.write("\n".join(euclidean_alternative))
        # bg_file.write("\n")
        # edistance = []

        # for dom in self.constants_per_domain:
        #    edistance.append("edistance_{}(X,Y,Z) :- embedding_{}(X,E1),embedding_{}(Y,E2),X\==Y,euclidean_dist(E1,E2,Z).".format(dom, dom, dom))

        # bg_file.write("\n".join(edistance) + "\n")
        # bg_file.write("\n")

        bg_file.close()

    def _enhance_data(self):
        for dom in self.constants_per_domain:
            pred_name = "edistance_{}".format(dom.lower())
            pred_types = [dom.lower(), dom.lower(), "number"]

            if pred_name not in self.predicate_definitions:
                self.predicate_definitions[pred_name] = pred_types

            if pred_name not in self.facts_per_predicates:
                self.facts_per_predicates[pred_name] = set()

            constants = list(self.constants_per_domain[dom])

            distances = []

            for i in range(len(constants) - 1):
                for j in range(i+1, len(constants)):
                    head = constants[i]
                    tail = constants[j]

                    try:
                        distance = self.embeddingHandler.get_entity_distance(head, tail)
                        distances.append((head, tail, round(distance, 2)))
                    except Exception:
                        pass

            distances = sorted(distances, key=lambda x: x[2])[:max(1, int(self.subset_val*len(distances)))]

            for elem in distances:
                self.facts_per_predicates[pred_name].add(tuple([str(x) for x in elem]))

    def _prepare_settings_file(self, definitions, labels, constants, heuristic, minimal_cases, accuracy, working_directory):
        settings_file = open("{}/train.s".format(working_directory), 'w')
        settings_file.write("load(key).\n")
        settings_file.write("talking(4).\n")
        settings_file.write("output_options([c45,prolog]).\n")
        settings_file.write("bias(rmode).\n")
        settings_file.write("classes([{}]).\n".format(",".join(set([x.lower() for x in labels.values()]))))
        settings_file.write("typed_language(yes).\n")
        settings_file.write("heuristic({}).\n".format(heuristic))
        settings_file.write("pruning(none).\n")
        settings_file.write("tilde_mode(classify).\n")
        settings_file.write("minimal_cases({}).\n".format(minimal_cases))
        settings_file.write("accuracy({}).\n".format(accuracy))
        target_doms = []
        if self.link_prediction:
            target_doms = ["+{}".format(self._find_target_domain([y.split(",")[x] for y in labels], constants).lower())
                           for x in range(list(labels.keys())[0].count(",") + 1)]
        else:
            target_doms = ["+{}".format(self._find_target_domain(list(labels.keys()), constants).lower())]
        settings_file.write("predict(targetpred({},-class)).\n".format(','.join(target_doms)))
        settings_file.write("\n")

        for pred in definitions:
            settings_file.write("type({}({})).\n".format(pred.lower(), ",".join([x.lower() for x in definitions[pred]])))

        for dom in self.constants_per_domain:
            settings_file.write("type(edistance_{}({},{},number)).\n".format(dom, dom, dom))

        settings_file.write("type(number < number).\n")
        settings_file.write("type(number > number).\n")
        settings_file.write("\n")

        for pred in definitions:
            rep = ""
            if len(definitions[pred]) == 1:
                rep = "rmode({}(+V)).\n".format(pred.lower)
            elif 'edistance' in pred:
                pass
            else:
                vars = zip(["+-V" for x in definitions[pred]], range(len(definitions[pred])))
                vars = ["{}{}".format(x[0], x[1]) for x in vars]
                rep = "rmode({}({})).\n".format(pred.lower(), ",".join(vars))

            settings_file.write(rep)

        for dom in self.constants_per_domain:
            settings_file.write("rmode((edistance_{}(+V1,-V2,Y),Y < #Y)).\n".format(dom))
            settings_file.write("rmode((edistance_{}(-V1,+V2,Y),Y < #Y)).\n".format(dom))
            settings_file.write("rmode((edistance_{}(+V1,+V2,Y),Y < #Y)).\n".format(dom))

        settings_file.close()


class DETilde(ETilde):

    def __init__(self, parameters, working_directory, method, exclude_negatives=False):
        super().__init__(parameters, working_directory, method, exclude_negatives)
        self.bounds = parameters.get_parameter("n_bounds")

    def _prepare_settings_file(self, definitions, labels, constants, heuristic, minimal_cases, accuracy, working_directory):
        settings_file = open("{}/train.s".format(working_directory), 'w')
        settings_file.write("load(key).\n")
        settings_file.write("talking(4).\n")
        settings_file.write("output_options([c45,prolog]).\n")
        settings_file.write("bias(rmode).\n")
        settings_file.write("classes([{}]).\n".format(",".join(set([x.lower() for x in labels.values()]))))
        settings_file.write("typed_language(yes).\n")
        settings_file.write("heuristic({}).\n".format(heuristic))
        settings_file.write("pruning(none).\n")
        settings_file.write("tilde_mode(classify).\n")
        settings_file.write("minimal_cases({}).\n".format(minimal_cases))
        settings_file.write("accuracy({}).\n".format(accuracy))
        target_doms = []
        if self.link_prediction:
            target_doms = ["+{}".format(self._find_target_domain([y.split(",")[x] for y in labels], constants).lower())
                           for x in range(list(labels.keys())[0].count(",") + 1)]
        else:
            target_doms = ["+{}".format(self._find_target_domain(list(labels.keys()), constants).lower())]
        settings_file.write("predict(targetpred({},-class)).\n".format(','.join(target_doms)))
        settings_file.write("discretize(entropy).\n")
        settings_file.write("discretization(bounds({})).\n".format(self.bounds))
        settings_file.write("\n")

        for dom in self.constants_per_domain:
            settings_file.write("to_be_discretized(edistance_{}(V1,V2,C), [C]).\n".format(dom))

        for pred in definitions:
            settings_file.write("type({}({})).\n".format(pred.lower(), ",".join([x.lower() for x in definitions[pred]])))

        for dom in self.constants_per_domain:
            settings_file.write("type(edistance_{}({},{},number)).\n".format(dom, dom, dom))

        settings_file.write("type(number < number).\n")
        settings_file.write("type(number > number).\n")
        settings_file.write("\n")

        for pred in definitions:
            rep = ""
            if len(definitions[pred]) == 1:
                rep = "rmode({}(+V)).\n".format(pred.lower)
            elif 'edistance' in pred:
                pass
            else:
                vars = zip(["+-V" for x in definitions[pred]], range(len(definitions[pred])))
                vars = ["{}{}".format(x[0], x[1]) for x in vars]
                rep = "rmode({}({})).\n".format(pred.lower(), ",".join(vars))

            settings_file.write(rep)

        for dom in self.constants_per_domain:
            settings_file.write(
                "rmode(#(1*10*C: threshold(edistance_{}(_,_,Value),[Value],C), (edistance_{}(+V1,-V2,Y),Y < C) ) ).\n".format(dom, dom))
            settings_file.write(
                "rmode(#(1*10*C: threshold(edistance_{}(_,_,Value),[Value],C), (edistance_{}(-V1,+V2,Y),Y < C) ) ).\n".format(
                    dom, dom))
            settings_file.write(
                "rmode(#(1*10*C: threshold(edistance_{}(_,_,Value),[Value],C), (edistance_{}(+V1,-V2,Y),Y > C) ) ).\n".format(
                    dom, dom))
            settings_file.write(
                "rmode(#(1*10*C: threshold(edistance_{}(_,_,Value),[Value],C), (edistance_{}(-V1,+V2,Y),Y > C) ) ).\n".format(
                    dom, dom))

        settings_file.close()


class Aleph(RelationalLearner):

    def __init__(self, parameters, working_directory, exclude_negatives=False):
        super().__init__(parameters, exclude_negatives)
        self.working_directory = working_directory
        self.n_folds = self.parameters.get_parameter('aleph_folds')

    def _find_target_domain(self, labels, constants_per_domain):
        found_domain = ""
        example_object = list(labels)[0].lower()

        for dom in constants_per_domain:
            if example_object in constants_per_domain[dom]:
                found_domain = dom

        if found_domain == "":
            for o in labels:
                if found_domain != "":
                    continue
                for dom in constants_per_domain:
                    if found_domain != "":
                        continue
                    if o.lower() in constants_per_domain[dom]:
                        found_domain = dom

        return found_domain

    def _prepare_settings(self, bg_file, clause_length, min_acc, min_pos):
        # settings
        bg_file.write(":- set(search,{}).\n".format(self.parameters.get_parameter("aleph_search")))
        bg_file.write(":- set(check_useless,true).\n")
        bg_file.write(":- set(clauselength,{}).\n".format(clause_length))

        if self.parameters.has_parameter('aleph_construct_bottom'):
            bg_file.write(
                ":- set(construct_bottom,{}).\n".format(self.parameters.get_parameter('aleph_construct_bottom')))
        else:
            bg_file.write(":- set(construct_bottom,saturation).\n")

        if self.parameters.has_parameter('aleph_depth'):
            bg_file.write(":- set(depth,{}).\n".format(self.parameters.get_parameter('aleph_depth')))

        if self.parameters.has_parameter('aleph_evalfn'):
            bg_file.write(":- set(evafn,{}).\n".format(self.parameters.get_parameter('aleph_evalfn')))

        bg_file.write(":- set(minacc,{}).\n".format(min_acc))

        bg_file.write(":- set(minpos,{}).\n".format(min_pos))

        if self.parameters.has_parameter('aleph_newvars'):
            bg_file.write(":- set(newvars,{}).\n".format(self.parameters.get_parameter('aleph_newvars')))

        if self.parameters.has_parameter('aleph_nodes'):
            bg_file.write(":- set(nodes,{}).\n".format(self.parameters.get_parameter('aleph_nodes')))

        if self.parameters.has_parameter('aleph_noise'):
            bg_file.write(":- set(noise,{}).\n".format(self.parameters.get_parameter('aleph_noise')))

        if self.parameters.has_parameter('aleph_proof_strategy'):
            bg_file.write(":- set(proof_strategy,{}).\n".format(self.parameters.get_parameter('aleph_proof_Strategy')))

        if self.parameters.has_parameter('aleph_resample'):
            bg_file.write(":- set(resample,{}).\n".format(self.parameters.get_parameter('aleph_resample')))

        if self.parameters.has_parameter('aleph_searchtime'):
            bg_file.write(":- set(searchtime,{}).\n".format(self.parameters.get_parameter('aleph_searchtime')))

        if self.parameters.has_parameter('aleph_verbosity'):
            bg_file.write(":- set(verbosity,{}).\n".format(self.parameters.get_parameter('aleph_verbosity')))

        if self.parameters.has_parameter('aleph_samplesize'):
            bg_file.write(":- set(samplesize,{}).\n".format(self.parameters.get_parameter('aleph_samplesize')))

        bg_file.write(":- set(test_pos,'test.f').\n")
        bg_file.write(":- set(test_neg,'test.n').\n")

        bg_file.write("\n")

    def _prepare_bias(self, bg_file, definitions, labels, constants):
        target_pred_arity = list(labels.keys())[0].count(',') + 1
        if self.link_prediction and self.parameters.get_parameter('aleph_evalfn'):
            target_pred_arity += 1

        target_doms = []
        if self.link_prediction:
            target_doms = ["+{}".format(self._find_target_domain([y.split(",")[x] for y in labels], constants).lower())
                           for x in range(list(labels.keys())[0].count(",")+1)]
        else:
            target_doms = ["+{}".format(self._find_target_domain(list(labels.keys()), constants).lower())]

        if self.link_prediction and self.parameters.get_parameter('aleph_evalfn') == 'posonly':
            bg_file.write(":- modeh(1,targetpred({})).\n".format(",".join(target_doms)))
        else:
            bg_file.write(":- modeh(1,targetpred({},#class)).\n".format(",".join(target_doms)))

        for pred in definitions:
            if len(definitions[pred]) == 1:
                bg_file.write(":- modeb(*, {}(+{})).\n".format(pred.lower(), definitions[pred][0]))
            else:
                possible = [('+', '-', '#') if len(self.constants_per_domain[x]) < 10 or x in constant_domains else ('+', '-') for x in
                            definitions[pred]]
                for combo in itertools.product(*possible):
                    if len(set([x for x in combo])) == 1 or len([x for x in combo if x == '+']) == 0:
                        continue
                    rep = ["{}{}".format(x[0], x[1].lower()) for x in zip(combo, definitions[pred])]
                    bg_file.write(":- modeb(*,{}({})).\n".format(pred.lower(), ",".join(rep)))

        bg_file.write("\n")

        for pred in definitions:
            bg_file.write(":- determination(targetpred/{},{}/{}).\n".format(target_pred_arity, pred.lower(), len(definitions[pred])))

        bg_file.write("\n")

    def _prepare_data_and_types(self, bg_file, facts_per_predicate):
        bg_file.write("% types\n")
        possible_labels = set(self.labels.values())
        for cl in possible_labels:
            bg_file.write("class({}).\n".format(cl.lower()))

        for dom in self.constants_per_domain:
            bg_file.write(
                " ".join(["{}({}).".format(dom.lower(), c.lower()).replace("%", "").replace("\\", "").replace("=", "") for c in self.constants_per_domain[dom]]) + "\n")
            bg_file.write("\n")

        bg_file.write("% data\n")

        for pred in facts_per_predicate:
            for g in facts_per_predicate[pred]:
                bg_file.write("{}({}).\n".format(pred.lower(), ",".join([x.lower() for x in g])).replace("%", "").replace("\\", "").replace("=", ""))

    def _prepare_knowledge_file(self, definitions, labels, constants, facts_per_predicate, clause_length, min_acc, min_pos, working_directory):
        bg_file = open("{}/train.b".format(working_directory), 'w')

        # settings
        self._prepare_settings(bg_file, clause_length, min_acc, min_pos)

        self._prepare_bias(bg_file, definitions, labels, constants)

        self._prepare_data_and_types(bg_file, facts_per_predicate)

        bg_file.close()

    def _prepare_positive_examples(self, labels, working_directory):
        pos_file = open("{}/train.f".format(working_directory), 'w')

        if not self.link_prediction:
            for ent in labels:
                pos_file.write("targetpred({},{}).\n".format(ent.lower(), labels[ent].lower()).replace("%", "").replace("\\", "").replace("=", ""))
        else:
            for ent in labels:
                if labels[ent] == "neg":
                    continue
                elif self.link_prediction and self.parameters.get_parameter('aleph_evalfn') == 'posonly':
                    pos_file.write(
                        "targetpred({}).\n".format(ent.lower()).replace("%", "").replace("\\","").replace("=", ""))
                else:
                    pos_file.write("targetpred({},{}).\n".format(ent.lower(), labels[ent].lower()).replace("%", "").replace("\\", "").replace("=", ""))

        pos_file.close()

    def _prepare_negative_examples(self, labels, working_directory):
        neg_file = open("{}/train.n".format(working_directory), 'w')

        possible_labels = set(self.labels.values())
        if not self.link_prediction:
            for entity in labels:
                for lab in [x for x in possible_labels if x != labels[entity]]:
                    neg_file.write("targetpred({},{}).\n".format(entity.lower(), lab.lower()).replace("%", "").replace("\\", "").replace("=", ""))
        else:
            for entity in labels:
                if labels[entity] == "pos":
                    continue
                elif self.parameters.get_parameter('aleph_evalfn') == 'posonly':
                    neg_file.write("targetpred({}).\n".format(entity.lower()).replace("%", "").replace("\\", "").replace("=", ""))
                else:
                    neg_file.write("targetpred({},{}).\n".format(entity.lower(), labels[entity].lower()).replace("%", "").replace("\\", "").replace("=", ""))

        neg_file.close()

    def _prepare_test_examples(self, test_labels, working_directory):
        pos_file = open("{}/test.f".format(working_directory), 'w')
        neg_file = open("{}/test.n".format(working_directory), 'w')

        possible_labels = set(self.labels.values())

        if not self.link_prediction:
            for example in test_labels:
                pos_file.write("targetpred({},{}).\n".format(example.lower(), test_labels[example].lower()).replace("%", "").replace("\\", "").replace("=", ""))

                for lab in possible_labels:
                    if lab != test_labels[example]:
                        neg_file.write("targetpred({},{}).\n".format(example.lower(), lab.lower()).replace("%", "").replace("\\", "").replace("=", ""))
        else:
            for example in test_labels:
                if test_labels[example] == "pos" and self.parameters.get_parameter('aleph_evalfn') == 'posonly':
                    pos_file.write("targetpred({}).\n".format(example.lower()).replace("%", "").replace("\\", "").replace("=", ""))
                elif test_labels[example] == "pos":
                    pos_file.write("targetpred({},{}).\n".format(example.lower(), test_labels[example].lower()).replace("%", "").replace("\\", "").replace("=", ""))
                elif test_labels[example] == 'neg' and self.parameters.get_parameter('aleph_evalfn') == 'posonly':
                    neg_file.write("targetpred({}).\n".format(example.lower()).replace("%", "").replace("\\", "").replace("=", ""))
                else:
                    neg_file.write("targetpred({},{}).\n".format(example.lower(), test_labels[example].lower()).replace("%", "").replace("\\", "").replace("=", ""))

        pos_file.close()
        neg_file.close()

    def _individual_run(self, conf):
        predicate_definitions = self.predicate_definitions
        test_entities = [self.entity_order_for_labels[x] for x in conf['test_indices']]

        final_test_labels = {}
        for ent in test_entities:
            final_test_labels[ent] = self.labels[ent]

        final_labels = {}
        for ent in self.entity_order_for_labels:
            if ent not in test_entities:
                final_labels[ent] = self.labels[ent]

        constants = self.constants_per_domain
        facts = self.facts_per_predicates
        current_working_directory = self.working_directory + "/aleph_{}_{}_{}".format(conf['cl'], conf['mp'],
                                                                                      conf['acc'])

        if not os.path.exists(current_working_directory):
            os.mkdir(current_working_directory)

        self._prepare_knowledge_file(predicate_definitions, final_labels, constants, facts, conf['cl'], conf['acc'], conf['mp'], current_working_directory)
        self._prepare_positive_examples(final_labels, current_working_directory)
        if not self.exclude_negatives:
            self._prepare_negative_examples(final_labels, current_working_directory)
        self._prepare_test_examples(final_test_labels, current_working_directory)

        cmd = "echo \"read_all(train). induce.\" " + "| yap -l {}/scripts/aleph.pl > log.txt 2> error.txt".format(
            os.path.abspath(os.path.dirname(__file__)))

        proc = subprocess.Popen(cmd, shell=True, cwd=current_working_directory)

        try:
            proc.wait()

            acc = 0.0
            start_parsing_test = False

            for line in open("{}/log.txt".format(current_working_directory)).readlines():
                if '[Test set performance]' in line:
                    start_parsing_test = True
                elif start_parsing_test and 'Accuracy' in line:
                    acc = float(line.strip().split("=")[1].strip())

            self.progress_bar.next()

            return {'cl': conf['cl'], 'mp': conf['mp'], 'acc': conf['acc'], 'accuracy': acc}
        except Exception:
            os.killpg(os.getpgid(proc.pid), signal.SIGTERM)
            self.progress_bar.next()
            return {'cl': conf['cl'], 'mp': conf['mp'], 'acc': conf['acc'], 'accuracy': 0.0}

    def _fit_with_params(self, training_data, labels):
        if len(labels) < 1:
            results = {}

            random.shuffle(self.entity_order_for_labels)

            chunk_size = int(len(self.entity_order_for_labels)/self.n_folds)

            for batch in range(self.n_folds):
                test_indices = range(batch * chunk_size, (batch + 1) * chunk_size)

                confs = []
                for c in self.parameters.get_parameter("aleph_clause_length"):
                    for acc in self.parameters.get_parameter("aleph_min_acc"):
                        for mc in self.parameters.get_parameter("aleph_min_pos"):
                            confs.append({'cl': c, 'acc': acc, 'mp': mc, 'test_indices': test_indices})

                self.progress_bar = Bar('Processing', max=len(confs), suffix='%(index)d / %(max)d  %(percent)d%%  avg time: %(avg)ds ETA: %(eta)ds')
                accuracies = map(self._individual_run, confs)
                self.progress_bar.finish()

                for item in accuracies:
                    param_dict = (("aleph_clause_length", item['cl']), ('aleph_min_acc', item['acc']),
                                  ('aleph_min_pos', item['mp']))

                    if param_dict not in results:
                        results[param_dict] = []
                    results[param_dict].append(item['accuracy'])

            summaries = {}
            for elem in results:
                summaries[elem] = np.array(results[elem]).mean()

            best_params = summaries.keys()
            best_params = sorted(best_params, key=lambda x: summaries[x], reverse=True)[0]

            print("FOUND BEST PARAMETERS: ", best_params)
            return dict(best_params)
        else:
            predicate_definitions = training_data[1]
            final_labels = self.labels
            # for item in labels:
            #    final_labels[item] = labels[item]
            constants = self.constants_per_domain
            facts = training_data[0]

            print(" READING BEST PARAMETERS: ", self.best_parameters)
            cl = self.best_parameters['aleph_clause_length']
            acc = self.best_parameters['aleph_min_acc']
            mp = self.best_parameters['aleph_min_pos']

            self._prepare_knowledge_file(predicate_definitions, final_labels, constants, facts, cl, acc, mp, self.working_directory)
            self._prepare_positive_examples(final_labels, self.working_directory)
            self._prepare_negative_examples(final_labels, self.working_directory)
            self._prepare_test_examples(labels, self.working_directory)

            cmd = "echo \"read_all(train). induce.\" " + "| yap -l {}/scripts/aleph.pl > log.txt 2> error.txt".format(
                os.path.abspath(os.path.dirname(__file__)))

            proc = subprocess.Popen(cmd, shell=True, cwd=self.working_directory)
            proc.wait()

            acc = 0.0
            start_parsing_test = False

            for line in open("{}/log.txt".format(self.working_directory)).readlines():
                if '[Test set performance]' in line:
                    start_parsing_test = True
                elif start_parsing_test and 'Accuracy' in line:
                    acc = float(line.strip().split("=")[1].strip())

            return acc


class AlephEm(Aleph):

    def __init__(self, parameters, work_dir, method, exclude_negatives=False):
        super().__init__(parameters, work_dir, exclude_negatives)
        self.embeddingHandler = EmbeddingAbstractor(method, parameters.get_parameter("threshold"))
        self.subset_val = parameters.get_parameter('subset')

    def read_entity_embeddings(self, entity_embeddings_file):
        self.embeddingHandler.read_entity_embeddings(entity_embeddings_file)

    def load_entity_embeddings(self, entity_embedding_map):
        self.embeddingHandler.load_entity_embeddings(entity_embedding_map)

    def _enhance_data(self):
        for dom in self.constants_per_domain:
            pred_name = "embedding_{}".format(dom.lower())
            pred_types = [dom.lower(), "list"]

            if pred_name not in self.predicate_definitions:
                self.predicate_definitions[pred_name] = pred_types

            if pred_name not in self.facts_per_predicates:
                self.facts_per_predicates[pred_name] = set()

            for elem in self.constants_per_domain[dom]:
                try:
                    emb = "[{}]".format(",".join([str(round(x, 2)) for x in self.embeddingHandler.get_entity_embedding(elem)]))
                    self.facts_per_predicates[pred_name].add((elem.lower(), emb))
                except Exception:
                    pass

    def _provide_background_knowledge(self):
        knowledge = ""

        euclidean_alternative = [
            "euclidean_dist([P|Ps], [Q|Qs], D) :- sum_diff_sq(Ps, Qs, (P - Q)**2, R), D is sqrt(R).",
            "sum_diff_sq([], [], V, V).",
            "sum_diff_sq([P|Ps], [Q|Qs], V0, V+V0) :- sum_diff_sq(Ps, Qs, (P - Q)**2, V)."
        ]
        knowledge += "\n".join(euclidean_alternative) + "\n"

        edistance = []
        for dom in self.constants_per_domain:
            edistance.append("edistance_{}(X,Y,Z) :- embedding_{}(X,E1),embedding_{}(Y,E2),X\==Y,euclidean_dist(E1,E2,Z).".format(dom, dom, dom))

        knowledge += "\n".join(edistance) + "\n"

        bounding = [
            "lteq(X,Y):- number(X), number(Y), X =< Y, !.",
            "lteq(X,X):- number(X).",
            "lte(X,Y):- number(X), not(var(Y)), number(Y), !, X =< Y.",
            "lte(X,Y1):- ubound(X,Y1).",
            "ubound(0.0,5.0):- !.",
            "ubound(X,X):- X \= 0.0."
        ]

        knowledge += "\n".join(bounding) + "\n"

        return knowledge

    def _prepare_bias(self, bg_file, definitions, labels, constants):
        target_pred_arity = list(labels.keys())[0].count(',') + 1
        if self.link_prediction and self.parameters.get_parameter('aleph_evalfn'):
            target_pred_arity += 1

        target_doms = []
        if self.link_prediction:
            target_doms = ["+{}".format(self._find_target_domain([y.split(",")[x] for y in labels], constants).lower())
                           for x in range(list(labels.keys())[0].count(",") + 1)]
        else:
            target_doms = ["+{}".format(self._find_target_domain(list(labels.keys()), constants).lower())]

        if self.link_prediction and self.parameters.get_parameter('aleph_evalfn') == 'posonly':
            bg_file.write(":- modeh(1,targetpred({})).\n".format(",".join(target_doms)))
        else:
            bg_file.write(":- modeh(1,targetpred({},#class)).\n".format(",".join(target_doms)))

        for pred in definitions:
            if len(definitions[pred]) == 1:
                bg_file.write(":- modeb(*, {}(+V)).\n".format(pred.lower()))
                rep = "+{}".format(definitions[pred][0])
            elif 'embedding' in pred:
                bg_file.write(":- modeb(2,{}(+{},-list)).\n".format(pred.lower(), definitions[pred][0].lower()))
            else:
                possible = [('+', '-', '#') if len(self.constants_per_domain[x]) < 5 else ('+', '-') for x in
                            definitions[pred]]
                for combo in itertools.product(*possible):
                    if len(set([x for x in combo])) == 1 or len([x for x in combo if x == '+']) == 0:
                        continue
                    rep = ["{}{}".format(x[0], x[1].lower()) for x in zip(combo, definitions[pred])]
                    bg_file.write(":- modeb(*,{}({})).\n".format(pred.lower(), ",".join(rep)))

        bg_file.write(":- modeb(1,lteq(+number,#number)).\n\n")

        for dom in self.constants_per_domain:
            bg_file.write(":- modeb(1,edistance_{}(+{},+{},-number)).\n".format(dom, dom, dom))

        bg_file.write("\n")

        bg_file.write(":- lazy_evaluate(lteq/2).\n")

        bg_file.write("\n")

        for pred in definitions:
            if 'embedding' in pred:
                pass
            else:
                bg_file.write(":- determination(targetpred/{},{}/{}).\n".format(target_pred_arity, pred.lower(), len(definitions[pred])))

        for dom in self.constants_per_domain:
            bg_file.write(":- determination(targetpred/{},edistance_{}/2).\n".format(target_pred_arity, dom))

        bg_file.write(":- determination(targetpred/{},lteq/2).\n".format(target_pred_arity))
        bg_file.write("\n")

        bg_file.write(self._provide_background_knowledge() + "\n")


class AMIE(RelationalLearner):

    def __init__(self, parameters, working_directory, exclude_negatives=False):
        super().__init__(parameters, exclude_negatives)
        self.working_directory = working_directory
        self.n_folds = self.parameters.get_parameter('amie_folds')
        self.include_negatives = self.parameters.get_parameter('amie_include_negatives')

    def _prepare_file(self, facts, labels, working_dir):
        data_file = open("{}/data.tsv".format(working_dir), 'w')

        for pred in facts:
            for gr in facts[pred]:
                data_file.write("<{}>\t<{}>\t<{}>\n".format(gr[0].lower(), pred.lower(), gr[1].lower()))

        if not self.link_prediction:
            for elem in labels:
                data_file.write("<{}>\t<targetpred>\t<{}>\n".format(elem.lower(), labels[elem].lower()))
        else:
            for elem in labels:
                if labels[elem] == "pos":
                    arg1, arg2 = [x.lower() for x in elem.split(",")]
                    data_file.write("<{}>\t<targetpred>\t<{}>\n".format(arg1, arg2))

        data_file.close()

    def _learn_model(self, conf, working_dir):
        cmd = "{}/bin/java -jar {}/amie_plus.jar ".format(os.environ['JAVA_HOME'],
                                                          os.path.abspath(os.path.dirname(__file__)) + "/scripts")
        # cmd += "-bexr \"<targetpred>\" "
        cmd += "-const -htr \"<targetpred>\" -minis 2 "
        cmd += "-maxad {} ".format(conf['maxad'])
        cmd += "-minc {} ".format(conf['minc'])
        cmd += "-mins {} ".format(conf['mins'])
        # cmd += "-rl {} ".format(conf['rl'])
        cmd += "-minpca {} ".format(conf['minpca'])
        cmd += "data.tsv > amie.log 2> amie.err "

        cmdf = open("{}/amie.cmd".format(working_dir), 'w')
        cmdf.write(cmd)
        cmdf.close()

        proc = subprocess.Popen(cmd, shell=True, cwd=working_dir)
        proc.wait()

        rules = []

        for line in open("{}/amie.log".format(working_dir)).readlines():
            if '=>' in line:
                tmp = line[:line.find(".") - 1]
                body, head = tmp.strip().split("=>")
                head = head.split()
                head = [x[1:].capitalize() if '?' in x else x[1:-1] for x in head]
                head = "{}({},{})".format(head[1].lower(), head[0], head[2])

                body = body.split()
                atom_count = int(len(body) / 3)
                body_atoms = []

                is_incorrectly_grounded = False

                for atom_id in range(atom_count):
                    ctmp = body[atom_id * 3:(atom_id + 1) * 3]
                    var1, pred, var2 = [x[1:].capitalize() if '?' in x.lower() else x[1:-1] for x in ctmp]
                    body_atoms.append("{}({},{})".format(pred.lower(), var1, var2))

                    if pred == 'targetpred':
                        is_incorrectly_grounded = True
                        continue

                    domains = self.predicate_definitions[pred]

                    if var1.lower() == var1 and domains[0] not in constant_domains:
                        is_incorrectly_grounded = True

                    if var2.lower() == var2 and domains[1] not in constant_domains:
                        is_incorrectly_grounded = True

                if not is_incorrectly_grounded:
                    rules.append("{} :- {}.".format(head, ','.join(body_atoms)))

        rules_file = open("{}/amie.rules".format(working_dir), 'w')

        for rule in rules:
            rules_file.write(rule + "\n")
        rules_file.close()

        return rules

    def _evaluate_test_labels(self, rules, facts, test_labels, work_dir="."):
        if len(rules) == 0:
            return 0.0

        model = ""

        for pred in facts:
            for gr in facts[pred]:
                model += "{}({}).\n".format(pred.lower(), ','.join([x.lower() for x in gr]))

        for rule in rules:
            model += "{}\n".format(rule)

        pl = PrologString(model)
        engine = DefaultEngine()
        db = engine.prepare(pl)

        correct_predictions = 0
        example_count = 0

        report = open("{}/amie.report".format(work_dir), 'w')

        all_labels = set(test_labels.values())

        for elem in test_labels:
            if not self.link_prediction:
                query = Term('targetpred', Term(elem.lower()), Term(test_labels[elem].lower()))

                res = engine.query(db, query)

                if bool(res):
                    correct_predictions += 1
                example_count += 1

                if self.include_negatives:
                    neg_labels = [x.lower() for x in all_labels if x.lower() != test_labels[elem].lower()]
                    for n_label in neg_labels:
                        query = Term('targetpred', Term(elem.lower()), Term(n_label))

                        res = engine.query(db, query)

                        if not bool(res):
                            correct_predictions += 1
                        example_count += 1

            else:
                if not self.include_negatives and test_labels[elem] == "neg":
                    continue
                query_args = ['targetpred'] + [Term(x) for x in elem.split(",")]
                query = Term(*query_args)

                res = engine.query(db, query)

                if bool(res):
                    if test_labels[elem] == "pos":
                        correct_predictions += 1
                else:
                    if test_labels[elem] == "neg":
                        correct_predictions += 1

                example_count += 1

        report.write("\nCORRECT: {}\n".format(correct_predictions))
        report.write("\nTOTAL: {}\n".format(example_count))
        report.close()
        if not example_count:
            return 0.0

        return float(correct_predictions)/example_count

    def _individual_run(self, conf):
        predicate_definitions = self.predicate_definitions
        test_entities = [self.entity_order_for_labels[x] for x in conf['test_indices']]

        final_test_labels = {}
        for ent in test_entities:
            final_test_labels[ent] = self.labels[ent]

        final_labels = {}
        for ent in self.entity_order_for_labels:
            if ent not in test_entities:
                final_labels[ent] = self.labels[ent]

        facts = self.facts_per_predicates
        current_working_directory = self.working_directory + "/amie_{}_{}_{}_{}_{}".format(conf['maxad'], conf['minc'],
                                                                                           conf['mins'], conf['rl'],
                                                                                           conf['minpca'])

        if not os.path.exists(current_working_directory):
            os.mkdir(current_working_directory)

        self._prepare_file(facts, final_labels, current_working_directory)
        rules = self._learn_model(conf, current_working_directory)

        acc = self._evaluate_test_labels(rules, facts, final_test_labels)

        self.progress_bar.next()
        return {'maxad': conf['maxad'], 'minc': conf['minc'], 'mins': conf['mins'], 'rl': conf['rl'],
                'minpca': conf['minpca'], 'accuracy': acc}

    def _fit_with_params(self, training_data, labels):
        if len(labels) < 1:
            results = {}

            random.shuffle(self.entity_order_for_labels)

            chunk_size = int(len(self.entity_order_for_labels)/self.n_folds)

            for batch in range(self.n_folds):
                test_indices = range(batch * chunk_size, (batch + 1) * chunk_size)

                confs = []
                for maxad in self.parameters.get_parameter("amie_maxad"):
                    for minhc in self.parameters.get_parameter("amie_minc"):
                        for mins in self.parameters.get_parameter("amie_mins"):
                            for rl in self.parameters.get_parameter('amie_rl'):
                                for minpca in self.parameters.get_parameter('amie_minpca'):
                                    confs.append({'maxad': maxad,
                                                  'minc': minhc,
                                                  'mins': mins,
                                                  'rl': rl,
                                                  'minpca': minpca,
                                                  'test_indices': test_indices})

                self.progress_bar = Bar('Processing', max=len(confs), suffix='%(index)d / %(max)d  %(percent)d%%  %(avg)ds')
                accuracies = map(self._individual_run, confs)
                self.progress_bar.finish()

                for item in accuracies:
                    param_dict = (("amie_maxad", item['maxad']), ('amie_minc', item['minc']),
                                  ('amie_mins', item['mins']), ('amie_rl', item['rl']),
                                  ('amie_minpca', item['minpca']))

                    if param_dict not in results:
                        results[param_dict] = []
                    results[param_dict].append(item['accuracy'])

            summaries = {}
            for elem in results:
                summaries[elem] = np.array(results[elem]).mean()

            best_params = summaries.keys()
            best_params = sorted(best_params, key=lambda x: summaries[x], reverse=True)[0]

            print("FOUND BEST PARAMETERS: ", best_params)
            return dict(best_params)
        else:
            predicate_definitions = training_data[1]
            final_labels = self.labels
            # for item in labels:
            #    final_labels[item] = labels[item]
            constants = self.constants_per_domain
            facts = training_data[0]

            print(" READING BEST PARAMETERS: ", self.best_parameters)
            maxad = self.best_parameters['amie_maxad']
            minhc = self.best_parameters['amie_minc']
            mins = self.best_parameters['amie_mins']
            rl = self.best_parameters['amie_rl']
            minpca = self.best_parameters['amie_minpca']

            conf = {'maxad': maxad, 'minc': minhc, 'mins': mins, 'rl': rl, 'minpca': minpca}

            self._prepare_file(facts, self.labels, self.working_directory)
            rules = self._learn_model(conf, self.working_directory)
            acc = self._evaluate_test_labels(rules, facts, labels)

            return acc


class Metagol(AMIE):

    def __init__(self, parameters, working_directory, exclude_negatives=False):
        super().__init__(parameters, working_directory, exclude_negatives)
        self.include_negatives = self.parameters.get_parameter('metagol_include_negatives')
        self.n_folds = self.parameters.get_parameter('metagol_folds')
        self.metarules = self.parameters.get_parameter('metagol_metarules')
        self.timeout = self.parameters.get_parameter('timeout')*60

    def _get_negatives(self, labels):
        negatives = []

        if self.exclude_negatives:
            return negatives

        possible_labels = set(labels.values())

        for elem in labels:
            for n_label in [x for x in possible_labels if x != labels[elem]]:
                negatives.append("targetpred({},{})".format(elem.lower(), n_label.lower()))

        return negatives

    def _prepare_metagol_file(self, facts, labels, working_dir, max_clauses):
        metagol_file = open("{}/train.pl".format(working_dir), 'w')

        metagol_file.write(":- use_module(\'{}/scripts/metagol\').\n".format(os.path.abspath(os.path.dirname(__file__))))
        metagol_file.write("\n")

        metagol_file.write("metagol:max_clauses({}).\n".format(max_clauses))
        metagol_file.write("\n")

        metagol_file.write("%%  tell metagol to use the BK\n")
        for pred in self.predicate_definitions:
            metagol_file.write("prim({}/{}).\n".format(pred.lower(), len(self.predicate_definitions[pred])))

        metagol_file.write("\n")

        metagol_file.write("%% metarules\n")

        for metarule in self.metarules:
            metagol_file.write(metarule + "\n")

        metagol_file.write("\n")

        metagol_file.write("%% background knowledge\n")

        for pred in facts:
            for inst in facts[pred]:
                metagol_file.write("{}({}).\n".format(pred.lower(), ','.join([x.lower() for x in inst])).replace("%", "").replace("\\", "").replace("=", ""))

        metagol_file.write("\n")

        metagol_file.write("a :- \n")
        metagol_file.write("\tPos = [{}],\n".format(','.join(["targetpred({},{})".format(x.lower(), labels[x].lower()) for x in labels])))

        if self.include_negatives:
            metagol_file.write("\tNeg = [{}],\n".format(",".join(self._get_negatives(labels))))

        if self.include_negatives:
            metagol_file.write("\tlearn(Pos,Neg).\n")
        else:
            metagol_file.write("\tlearn(Pos,[]).\n")

    def _learn_model(self, conf, working_dir):
        cmd = "echo \"a.\" | yap -l {}/train.pl > metagol.log 2> metagol.err".format(working_dir)

        cmdf = open("{}/metagol.cmd".format(working_dir), 'w')
        cmdf.write(cmd)
        cmdf.close()

        proc = subprocess.Popen(cmd, shell=True, cwd=working_dir, preexec_fn=os.setsid)

        rules = []

        try:
            proc.wait(timeout=self.timeout)

            for line in open("{}/metagol.log".format(working_dir)):
                if ':-' in line:
                    rules.append(line.strip())
        except Exception:
            os.killpg(os.getpgid(proc.pid), signal.SIGTERM)

        return rules

    def _individual_run(self, conf):
        predicate_definitions = self.predicate_definitions
        test_entities = [self.entity_order_for_labels[x] for x in conf['test_indices']]

        final_test_labels = {}
        for ent in test_entities:
            final_test_labels[ent] = self.labels[ent]

        final_labels = {}
        for ent in self.entity_order_for_labels:
            if ent not in test_entities:
                final_labels[ent] = self.labels[ent]

        facts = self.facts_per_predicates
        current_working_directory = self.working_directory + "/metagol_{}".format(conf['max_clauses'])

        if not os.path.exists(current_working_directory):
            os.mkdir(current_working_directory)

        self._prepare_metagol_file(facts, final_labels, current_working_directory, conf['max_clauses'])
        rules = self._learn_model(conf, current_working_directory)

        acc = self._evaluate_test_labels(rules, facts, final_test_labels)

        self.progress_bar.next()

        return {'max_clauses': conf['max_clauses'], 'accuracy': acc}

    def _fit_with_params(self, training_data, labels):
        if len(labels) < 1:
            results = {}

            random.shuffle(self.entity_order_for_labels)

            chunk_size = int(len(self.entity_order_for_labels)/self.n_folds)

            for batch in range(self.n_folds):
                test_indices = range(batch * chunk_size, (batch + 1) * chunk_size)

                confs = []
                for max_cl in self.parameters.get_parameter("metagol_max_clauses"):
                    confs.append({"max_clauses": max_cl, 'test_indices': test_indices})

                self.progress_bar = Bar('Processing', max=len(confs), suffix='%(index)d / %(max)d  %(percent)d%%  avg time: %(avg)ds ETA: %(eta)ds')
                accuracies = map(self._individual_run, confs)
                self.progress_bar.finish()

                for item in accuracies:
                    param_dict = (("metagol_max_clauses", item['max_clauses']))

                    if param_dict not in results:
                        results[param_dict] = []
                    results[param_dict].append(item['accuracy'])

            summaries = {}
            for elem in results:
                summaries[elem] = np.array(results[elem]).mean()

            best_params = summaries.keys()
            best_params = sorted(best_params, key=lambda x: summaries[x], reverse=True)[0]

            print("FOUND BEST PARAMETERS: ", best_params)
            return dict(best_params)
        else:
            predicate_definitions = training_data[1]
            final_labels = self.labels
            # for item in labels:
            #    final_labels[item] = labels[item]
            constants = self.constants_per_domain
            facts = training_data[0]

            print(" READING BEST PARAMETERS: ", self.best_parameters)
            maxad = self.best_parameters['metagol_max_clauses']

            conf = {'max_clauses': maxad}

            self._prepare_metagol_file(facts, self.labels, self.working_directory, maxad)
            rules = self._learn_model(conf, self.working_directory)
            acc = self._evaluate_test_labels(rules, facts, labels)

            return acc


class KFoil(RelationalLearner):

    def __init__(self, parameters, working_directory,  path_suffix, exclude_negatives=False):
        super().__init__(parameters, exclude_negatives)
        self.working_directory = working_directory
        self.n_folds = self.parameters.get_parameter('n_folds')
        self.working_directory = os.environ['KFOIL_HOME']
        self.timeout = parameters.get_parameter('timeout')*60

        if not os.path.exists(self.working_directory + "/" + path_suffix):
            os.mkdir(self.working_directory + "/" + path_suffix)
        self.working_directory = self.working_directory + "/" + path_suffix

        if not os.path.exists(self.working_directory + "/" + working_directory):
            os.mkdir(self.working_directory + "/" + working_directory)
        self.working_directory = self.working_directory + "/" + working_directory

    def _find_target_domain(self, labels, constants_per_domain):
        found_domain = ""
        example_object = list(labels)[0].lower()

        for dom in constants_per_domain:
            if example_object in constants_per_domain[dom]:
                found_domain = dom

        if found_domain == "":
            for o in labels:
                if found_domain != "":
                    continue
                for dom in constants_per_domain:
                    if found_domain != "":
                        continue
                    if o.lower() in constants_per_domain[dom]:
                        found_domain = dom

        return found_domain

    def _prepare_data_file(self, wd):
        data_file = open("{}/learningtask.pl".format(wd), 'w')

        for pred in self.facts_per_predicates:
            for elem in self.facts_per_predicates[pred]:
                data_file.write("{}({}).\n".format(pred.lower(), ",".join([x.lower() for x in elem]))
                                .replace("%", "")
                                .replace("\\", "")
                                .replace("=", ""))

        for ind, elem in enumerate(self.entity_order_for_labels):
            data_file.write("{}({}).\n".format(self.labels[elem].lower(), elem.lower()))
            data_file.write("testnr({},{}).\n".format(elem.lower(), elem.lower()))

        data_file.close()

    def _prepare_settings_file(self, wd):
        ff = open("{}/learningtask_language.pl".format(wd), 'w')

        ff.write("tasks([learningtask]).\n\n")

        ff.write("classes([{}]).\n".format(",".join(set(self.labels.values()))))

        for l in set(self.labels.values()):
            ff.write("label(learningtask,C,{}) :- {}(C).\n".format(l.lower(), l.lower()))

        ff.write("\n")

        ff.write("type(class({})).\n".format(self._find_target_domain(self.labels, self.constants_per_domain)))

        for pred in self.predicate_definitions:
            ff.write("type({}({})).\n".format(pred.lower(), ','.join([x.lower() for x in self.predicate_definitions[pred]])))

        ff.write("\n")

        for pred in self.predicate_definitions:
            for b in self.predicate_bias[pred]:
                tmpb = b[1:-1].split(",")
                bbs = [[x[0][0]] if '#' not in x[0] else self.constants_per_domain[x[1]] for x in zip(tmpb, self.predicate_definitions[pred])]

                for comn in itertools.product(*bbs):
                    ff.write("rmode({}({})).\n".format(pred.lower(), ','.join([x.lower() for x in comn])))

        ff.close()

    def _prepare_run_file(self, train_ids, test_ids, max_clauses, max_literals, beam_size, wd, t, d, g, s, r, b, c, N=False, M=False):
        ff = open("{}/run_learningtask.pl".format(wd), 'w')

        #ff.write("#!/usr/bin/yap -L\n\n")
        ff.write(":- compile('../../../kfoil.pl').\n")
        ff.write(":- compile([learningtask,learningtask_language]).\n\n")

        svm_param_string = "-c {} -t {} -R -S 1".format(c, t)

        if t == 0:
            svm_param_string = "-c {} -t {} -b {} -S 1 -R".format(c, t, b)
        elif t == 1:
            svm_param_string = "-c {} -t {} -s {} -r {} -d {} -b {} -S 1 -R".format(c, t, s, r, d, b)
        elif t == 2:
            svm_param_string = "-c {} -t {} -g {} -b {} -S 1 -R".format(c, t, g, b)
        else:
            svm_param_string = "-c {} -t {} -s {} -r {} -b {} -S 1 -R".format(c, t, s, r, b)

        ff.write("svm_params(\'{}\').\n".format(svm_param_string))
        ff.write("optimize_c(none).\n")
        ff.write("training_set([{}]).\n\n".format(",".join([self.entity_order_for_labels[x] for x in train_ids])))
        ff.write("test_set([{}]).\n\n".format(",".join([self.entity_order_for_labels[x] for x in test_ids])))
        ff.write(":- run_train_test(auc,{},{},{}).\n".format(max_clauses, max_literals, beam_size))

        ff.close()

    def _individual_run(self, conf):
        predicate_definitions = self.predicate_definitions
        final_labels = self.labels
        constants = self.constants_per_domain
        facts = self.facts_per_predicates
        current_working_directory = self.working_directory + "/kfoil_{}_{}_{}_{}_{}_{}_{}_{}_{}_{}_{}_{}".format(conf['max_clauses'],
                                                                                                        conf['max_literals'], conf['beam_size'], conf['t'],
                                                                                                        conf['d'], conf['g'], conf['s'], conf['r'],
                                                                                                        conf['b'], conf['c'], conf['N'], conf['M'])
        if 'cwd' in conf:
            current_working_directory = conf['cwd']
        test_entities = conf['test_indices']
        train_entities = [x for x in range(len(self.entity_order_for_labels)) if x not in test_entities]

        if not os.path.exists(current_working_directory):
            os.mkdir(current_working_directory)

        self._prepare_settings_file(current_working_directory)
        self._prepare_data_file(current_working_directory)
        self._prepare_run_file(train_entities, test_entities, conf['max_clauses'], conf['max_literals'], conf['beam_size'], current_working_directory, conf['t'],
                               conf['d'], conf['g'], conf['s'], conf['r'], conf['b'], conf['c'], conf['N'], conf['M'])

        #cmd = "chmod +x " + "run_learningtask.pl"
        #proc = subprocess.Popen(cmd, shell=True, cwd=current_working_directory)

        cmd = "yap -L run_learningtask.pl > log.txt 2> err.txt"

        if not os.path.exists(current_working_directory + "/log.txt"):
            proc = subprocess.Popen(cmd, shell=True, cwd=current_working_directory, preexec_fn=os.setsid)
            try:
                proc.wait(timeout=self.timeout)
            except Exception:
                os.killpg(os.getpgid(proc.pid), signal.SIGTERM)

        acc = 0.0
        start_parsing_test = False
        for line in open("{}/log.txt".format(current_working_directory)).readlines():
            if line.startswith('Test set accuracy'):
                acc = float(line.strip().split(":")[-1])

        #print("acc", acc)
        self.progress_bar.next()

        return {'max_clauses': conf['max_clauses'], 'max_literals': conf['max_literals'], 'beam_size': conf['beam_size'], 'accuracy': acc, 't': conf['t'],
                                                                                                        'd': conf['d'], 'g': conf['g'], 's': conf['s'], 'r': conf['r'],
                                                                                                        'b': conf['b'], 'N': conf['N'], 'M': conf['M'], 'c': conf['c']}

    def _fit_with_params(self, training_data, labels):
        #  optimize kFoil only
        if len(labels) < 1:
            results = {}

            random.shuffle(self.entity_order_for_labels)

            chunk_size = int(len(self.entity_order_for_labels) / self.n_folds)

            for batch in range(self.n_folds):
                test_indices = range(batch * chunk_size, (batch + 1) * chunk_size)

                confs = []
                for mc in self.parameters.get_parameter("kfoil_max_clauses"):
                    for ml in self.parameters.get_parameter("kfoil_max_literals"):
                        for bs in self.parameters.get_parameter("kfoil_beam_size"):
                            for t in self.parameters.get_parameter('kfoil_t'):
                                for d in self.parameters.get_parameter('kfoil_d'):
                                    for g in self.parameters.get_parameter('kfoil_g'):
                                        for s in self.parameters.get_parameter('kfoil_s'):
                                            for r in self.parameters.get_parameter('kfoil_r'):
                                                for b in self.parameters.get_parameter('kfoil_b'):
                                                    for N in self.parameters.get_parameter('kfoil_N'):
                                                        for M in self.parameters.get_parameter('kfoil_M'):
                                                            for c in self.parameters.get_parameter('kfoil_c'):
                                                                confs.append({'max_clauses': mc, 'max_literals': ml, 'beam_size': bs, 'test_indices': test_indices, 't': t,
                                                                             'd': d, 'g': g, 's': s, 'r': r, 'b': b, 'N': N, 'M': M, 'c': c})

                self.progress_bar = Bar('Processing', max=len(confs), suffix='%(index)d / %(max)d  %(percent)d%%  avg time: %(avg)ds ETA: %(eta)ds')
                accuracies = map(self._individual_run, confs)
                self.progress_bar.finish()

                for item in accuracies:
                    param_dict = (("kfoil_max_clauses", item['max_clauses']), ('kfoil_max_literals', item['max_literals']),
                                  ('kfoil_beam_size', item['beam_size']), ('kfoil_t', item['t']), ('kfoil_d', item['d']),
                                  ('kfoil_g', item['g']), ('kfoil_s', item['s']), ('kfoil_r', item['r']), ('kfoil_b', item['b']),
                                  ('kfoil_N', item['N']), ('kfoil_M', item['M']), ('kfoil_c', item['c'])
                                  )

                    if param_dict not in results:
                        results[param_dict] = []
                    results[param_dict].append(item['accuracy'])

            summaries = {}
            for elem in results:
                summaries[elem] = np.array(results[elem]).mean()

            best_params = summaries.keys()
            best_params = sorted(best_params, key=lambda x: summaries[x], reverse=True)[0]

            print("FOUND BEST PARAMETERS: ", best_params)
            return dict(best_params)

        #  use the best parameters for the final evaluation
        else:
            predicate_definitions = training_data[1]
            final_labels = self.labels
            # for item in labels:
            #    final_labels[item] = labels[item]
            constants = self.constants_per_domain
            facts = training_data[0]

            print(" READING BEST PARAMETERS: ", self.best_parameters)
            mc = self.best_parameters['kfoil_max_clauses']
            ml = self.best_parameters['kfoil_max_literals']
            bs = self.best_parameters['kfoil_beam_size']
            t = self.best_parameters['kfoil_t']
            d = self.best_parameters['kfoil_d']
            g = self.best_parameters['kfoil_g']
            s = self.best_parameters['kfoil_s']
            r = self.best_parameters['kfoil_r']
            b = self.best_parameters['kfoil_b']
            N = self.best_parameters['kfoil_N']
            M = self.best_parameters['kfoil_M']
            c = self.best_parameters['kfoil_c']

            train_indices = range(len(self.entity_order_for_labels))

            self.entity_order_for_labels = self.entity_order_for_labels + list(labels.keys())

            test_indices = range(len(train_indices), len(self.entity_order_for_labels))
            cwd = self.working_directory + "/final"
            if not os.path.exists(cwd):
                os.mkdir(cwd)

            conf = {'max_clauses': mc, 'max_literals': ml, 'beam_size': bs, 'test_indices': test_indices,
                    'cwd': cwd, 't': t, 'd': d, 'g': g, 's': s, 'r': r, 'b': b, 'N': N, 'M': M, 'c': c}

            for elem in labels:
                self.labels[elem] = labels[elem]

            res = self._individual_run(conf)
            return res['accuracy']


class RelKNN(RelationalLearner):

    def __init__(self, parameters, working_directory, base_similarities_root, target_domain_name, weights_step=5):
        super().__init__(parameters, working_directory)
        self.recent_code = os.environ.get('ReCeNT_HOME')
        self.base_similarities_folder = base_similarities_root
        self.target_domain = target_domain_name
        self.n_folds = self.parameters.get_parameter('n_folds')
        self.working_directory = working_directory
        self.w_Step = weights_step

    def _get_base_similarity_filenames(self, depth):
        return [
            "{}_depth{}_parameters1.0,0.0,0.0,0.0,0.0_comparechiSquared_localRepofalse.txt".format(self.target_domain,
                                                                                                   depth),
            "{}_depth{}_parameters0.0,1.0,0.0,0.0,0.0_comparechiSquared_localRepofalse.txt".format(self.target_domain,
                                                                                                   depth),
            "{}_depth{}_parameters0.0,0.0,1.0,0.0,0.0_comparechiSquared_localRepofalse.txt".format(self.target_domain,
                                                                                                   depth),
            "{}_depth{}_parameters0.0,0.0,0.0,1.0,0.0_comparechiSquared_localRepofalse.txt".format(self.target_domain,
                                                                                                   depth),
            "{}_depth{}_parameters0.0,0.0,0.0,0.0,1.0_comparechiSquared_localRepofalse.txt".format(self.target_domain,
                                                                                                   depth)
        ]

    def _get_element_order(self, sim_file):
        inp = open(sim_file)
        tmp = inp.readline()
        inp.close()
        return [x.lower() for x in tmp.replace("#", "").strip().split(";")]

    def _transform_matrix_into_list(self, sim_matrix, elem_order):
        similarities = {}
        dims = sim_matrix.shape
        for i in range(0, dims[0]):
            similarities[elem_order[i]] = {}
            for j in range(0, dims[1]):
                if i == j:
                    similarities[elem_order[i]][elem_order[j]] = 1.0
                else:
                    similarities[elem_order[i]][elem_order[j]] = sim_matrix[i, j]

        return similarities

    def _individual_run(self, conf):
        predicate_definitions = self.predicate_definitions
        final_labels = self.labels
        constants = self.constants_per_domain
        facts = self.facts_per_predicates
        current_working_directory = self.working_directory + "/knn_{}_{}_{}_{}_{}_{}".format(conf['d'], conf['w1'], conf['w2'], conf['w3'], conf['w4'], conf['w5'])
        if 'cwd' in conf:
            current_working_directory = conf['cwd']
        test_entities = [self.entity_order_for_labels[x] for x in conf['test_indices']]
        train_entities = [x for x in self.entity_order_for_labels if x not in test_entities]

        similarities = {}
        w = [conf['w1'], conf['w2'], conf['w3'], conf['w4'], conf['w5']]

        for ind, sim_file in enumerate(self._get_base_similarity_filenames(conf['d'])):
            pickled_file_name = sim_file.replace(".txt", ".pkl")

            if os.path.exists(self.base_similarities_folder + "/" + pickled_file_name):
                pickle_file = open(self.base_similarities_folder + "/" + pickled_file_name, 'rb')
                similarity_dict = pickle.load(pickle_file)
                element_order = pickle.load(pickle_file)
                pickle_file.close()
            else:
                matrix = np.loadtxt(self.base_similarities_folder + "/" + sim_file, delimiter=";", comments="#", dtype=np.float64)
                element_order = self._get_element_order(self.base_similarities_folder + "/" + sim_file)
                similarity_dict = self._transform_matrix_into_list(matrix, element_order)

                pickle_file = open(self.base_similarities_folder + "/" + pickled_file_name, 'wb')
                pickle.dump(similarity_dict, pickle_file)
                pickle.dump(element_order, pickle_file)
                pickle_file.close()

            sims_file = sim_file.replace(".txt", ".sims.pkl")

            if not os.path.exists(self.base_similarities_folder + "/" + sims_file):
                all_entities = element_order  # train_entities + test_entities

                train_entities = [x for x in train_entities if x in element_order]
                test_entities = [x for x in test_entities if x in element_order]

                for i1 in all_entities:
                    for i2 in all_entities:
                        ind_sim = w[ind] * similarity_dict[i1][i2]

                        if i1 not in similarities:
                            similarities[i1] = {}

                        if i2 not in similarities[i1]:
                            similarities[i1][i2] = 0.0

                        similarities[i1][i2] += ind_sim

                pick_file = open(self.base_similarities_folder + "/" + sims_file, 'wb')
                pickle.dump(all_entities, pick_file)
                pickle.dump(similarities, pick_file)
                pick_file.close()
            else:
                pick_file = open(self.base_similarities_folder + "/" + sims_file, 'rb')
                all_entities = pickle.load(pick_file)

                train_entities = [x for x in train_entities if x in element_order]
                test_entities = [x for x in test_entities if x in element_order]

                similarities = pickle.load(pick_file)

                pick_file.close()


            #for te in test_entities:
            #    for tre in train_entities:
            #        if te not in similarities:
            #            similarities[te] = {}
            #        if tre not in similarities[te]:
            #            similarities[te][tre] = 0
            #        similarities[te][tre] += w[ind] * similarity_dict[te][tre]

        #train_labels = [final_labels[x] for x in train_entities]
        #train_distance = np.zeros((len(train_entities), len(train_entities)))

        #for i1 in range(len(train_entities)):
        #    for i2 in range(len(train_entities)):
        #        if i1 == i2:
        #            train_distance[i1, i2] = 0.0
        #        if similarities[train_entities[i1]][train_entities[i2]] == 0:
        #            train_distance[i1, i2] = 1000.0
        #        else:
        #            train_distance[i1, i2] = 1./similarities[train_entities[i1]][train_entities[i2]]

        #knn = KNeighborsClassifier(n_neighbors=conf['k'], metric='precomputed')
        #knn.fit(train_distance, train_labels)

        test_ents_to_use = [x for x in test_entities if x in self.labels]
        train_to_use = [x for x in train_entities if x in self.labels]

        ground_truth_labels = [self.labels[x] for x in test_ents_to_use]
        predicted_labels = []

        for te in test_ents_to_use:
            te_sims = sorted([(similarities[te][tr], self.labels[tr]) for tr in train_to_use], key=lambda x: x[0], reverse=True)[:conf['k']]
            labels = [x[1] for x in te_sims]
            label_counts = [(x, labels.count(x)) for x in set(labels)]
            max_lab = max(label_counts, key=lambda x: x[1])

            predicted_labels.append(max_lab)


        #test_distances = np.zeros((len(test_entities), len(train_entities)))

        #for i1 in range(len(test_entities)):
        #    for i2 in range(len(train_entities)):
        #        if similarities[test_entities[i1]][train_entities[i2]] == 0:
        #            test_distances[i1, i2] = 1000.0
        #        else:
        #            test_distances[i1, i2] = 1./similarities[test_entities[i1]][train_entities[i2]]

        #predicted_labels = knn.predict(test_distances)

        #predicted_labels = {}
        #for te in similarities:
        #    similarities[te] = [(similarities[te][x], self.labels[x]) for x in similarities[te]]

        #for te in test_entities:
        #    sorted_dists = sorted(similarities[te], key=lambda x: x[0])[:conf['k']]
        #    sorted_dists = [x[1] for x in sorted_dists]
        #    class_count = {}
        #    for cl in set(sorted_dists):
        #        class_count[cl] = sorted_dists.count(cl)
        #
        #    predicted_labels[te] = max(class_count, key=lambda x: class_count[x])

        correct = 0
        count = 0
        #for te in test_entities:
        #    if te not in self.labels:
        #        continue
        #    if predicted_labels[te] == self.labels[te]:
        #        correct += 1
        #    count += 1

        for pred, gtr in zip(predicted_labels, ground_truth_labels):
            if pred == gtr:
                correct += 1
            count += 1

        #for i in range(len(test_entities)):
        #    if test_entities[i] not in self.labels:
        #        continue
        #    if self.labels[test_entities[i]] == predicted_labels[i]:
        #        correct += 1
        #    count += 1

        self.progress_bar.next()

        return {'k': conf['k'], 'w1': conf['w1'], 'w2': conf['w2'], 'w3': conf['w3'], 'w4': conf['w4'],
                'w5': conf['w5'], 'd': conf['d'], 'accuracy': float(correct)/float(count)}

    def _fit_with_params(self, training_data, labels):
        if len(labels) < 1:
            results = {}

            random.shuffle(self.entity_order_for_labels)

            chunk_size = int(len(self.entity_order_for_labels) / self.n_folds)

            for batch in range(self.n_folds):
                test_indices = range(batch * chunk_size, (batch + 1) * chunk_size)

                confs = []
                for d in self.parameters.get_parameter('recent_depth'):
                    for w1 in range(0, 100, self.w_Step):
                        for w2 in range(0, 100, self.w_Step):
                            for w3 in range(0, 100, self.w_Step):
                                for w4 in range(0, 100, self.w_Step):
                                    for w5 in range(0, 100, self.w_Step):
                                        for k in self.parameters.get_parameter('n_neighbors'):
                                            if w1 + w2 + w3 + w4 + w5 != 100:
                                                continue
                                            else:
                                                confs.append({'d': d, 'w1': float(w1)/100.0, 'w2': float(w2)/100.0,
                                                              'w3': float(w3)/100.0, 'w4': float(w4)/100.0,
                                                              'w5': float(w5)/100.0, 'k': k,
                                                              'test_indices': test_indices})

                self.progress_bar = Bar('Processing', max=len(confs), suffix='%(index)d / %(max)d  %(percent)d%%  avg time: %(avg)ds ETA: %(eta)ds')
                accuracies = map(self._individual_run, confs)
                self.progress_bar.finish()

                for item in accuracies:
                    param_dict = (
                    ('d', item['d']), ('w1', item['w1']), ('w2', item['w2']), ('w3', item['w3']),
                    ('w4', item['w4']), ('w5', item['w5']), ('k', item['k']))

                    if param_dict not in results:
                        results[param_dict] = []
                    results[param_dict].append(item['accuracy'])

            summaries = {}
            for elem in results:
                summaries[elem] = np.array(results[elem]).mean()

            best_params = summaries.keys()
            best_params = sorted(best_params, key=lambda x: summaries[x], reverse=True)[0]

            print("FOUND BEST PARAMETERS: ", best_params)
            return dict(best_params)
        else:
            predicate_definitions = training_data[1]
            final_labels = self.labels
            # for item in labels:
            #    final_labels[item] = labels[item]
            constants = self.constants_per_domain
            facts = training_data[0]

            print(" READING BEST PARAMETERS: ", self.best_parameters)
            d = self.best_parameters['d']
            w1 = self.best_parameters['w1']
            w2 = self.best_parameters['w2']
            w3 = self.best_parameters['w3']
            w4 = self.best_parameters['w4']
            w5 = self.best_parameters['w5']
            k = self.best_parameters['k']

            train_indices = range(len(self.entity_order_for_labels))

            self.entity_order_for_labels = self.entity_order_for_labels + list(labels.keys())

            test_indices = range(len(train_indices), len(self.entity_order_for_labels))
            cwd = self.working_directory + "/final"
            if not os.path.exists(cwd):
                os.mkdir(cwd)

            conf = {'d': d, 'w1': w1, 'w2': w2, 'w3': w3, 'w4': w4, 'w5': w5, 'test_indices': test_indices,
                    'cwd': cwd, 'k': k}

            for elem in labels:
                self.labels[elem] = labels[elem]

            res = self._individual_run(conf)
            return res['accuracy']


class BoostSRL(RelationalLearner):

    def __init__(self, parameters, working_directory, exclude_negatives=False):
        super().__init__(parameters, exclude_negatives)
        self.working_directory = working_directory
        self.n_folds = self.parameters.get_parameter('n_folds')
        self.positive_class = ""

    def set_positive_class(self, label):
        self.positive_class = label

    def _find_target_domain(self, labels, constants_per_domain):
        found_domain = ""
        example_object = list(labels)[0].lower()

        for dom in constants_per_domain:
            if example_object in constants_per_domain[dom]:
                found_domain = dom

        if found_domain == "":
            for o in labels:
                if found_domain != "":
                    continue
                for dom in constants_per_domain:
                    if found_domain != "":
                        continue
                    if o.lower() in constants_per_domain[dom]:
                        found_domain = dom

        return found_domain

    def _prepare_background(self, depth, size, clauses, definitions, labels, constants, focus_label, working_dir):
        modes = []
        bk_filename = "background.txt"
        if len(focus_label) > 1:
            bk_filename = "background_{}.txt".format(focus_label)
        bk_file = open("{}/{}".format(working_dir, bk_filename), 'w')

        bk_file.write("//Parameters\n")
        bk_file.write("setParam: maxTreeDepth={}\n".format(depth))
        bk_file.write("setParam: nodeSize={}\n".format(size))
        bk_file.write("setParam: numOfClauses={}\n".format(clauses))

        bk_file.write("//Modes\n")

        for pred in definitions:
            if pred in self.predicate_bias:
                for item in self.predicate_bias[pred]:
                    bk_file.write("mode: {}{}.\n".format(pred.lower(), item))
                    modes.append("{}{}.".format(pred.lower(), item))
            elif len(definitions[pred]) == 1:
                bk_file.write("mode: {}(+{}).\n".format(pred.lower(), definitions[pred][0]))
                #modes.append("{}(+{}).".format(pred.lower(), definitions[pred][0]))
            else:
                possible = [('+', '-', '#') if len(self.constants_per_domain[x]) < 10 or x in constant_domains else ('+', '-') for x in definitions[pred]]
                for combo in itertools.product(*possible):
                    if len(set([x for x in combo])) == 1 or len([x for x in combo if x == '+']) == 0:
                        continue
                    rep = ["{}{}".format(x[0], x[1].lower()) for x in zip(combo, definitions[pred])]
                    bk_file.write("mode: {}({}).\n".format(pred.lower(), ",".join(rep)))
                    #modes.append("{}({}).".format(pred.lower(), ",".join(rep)))

        target_doms = []
        if self.link_prediction:
            target_doms = ["+{}".format(self._find_target_domain([y.split(",")[x] for y in labels], constants).lower()) for x in range(list(labels.keys())[0].count(",") + 1)]
        else:
            target_doms = ["+{}".format(self._find_target_domain(list(labels.keys()), constants).lower())]

        if self.link_prediction:
            bk_file.write("mode: targetpred({}).\n".format(', '.join(target_doms)))
            #modes.append("targetpred({}).".format(', '.join(target_doms)))
        else:
            possible_labels = set(self.labels.values())
            for lab in possible_labels:
                if len(focus_label) < 1 or focus_label.lower() == lab.lower():
                    bk_file.write("mode: {}({}).\n".format(lab.lower(), target_doms[0]).replace("%", "").replace("\\", "").replace("=", "").replace("_", "XX"))
                    #modes.append("{}({}).".format(lab.lower(), target_doms[0]).replace("%", "").replace("\\", "").replace("=", "").replace("_", "XX"))

        bk_file.close()

        #return modes

    def _prepare_training_data(self, facts_per_predicate, training_labels, focus_label, working_dir):
        train_data = []
        train_pos = []
        train_neg = []
        bk_file = open("{}/train_bk.txt".format(working_dir), 'w')

        bk_filename = "background.txt"
        if len(focus_label) > 1:
            bk_filename = "background_{}.txt".format(focus_label)

        bk_file.write('import: "../' + bk_filename + '"' + "\n")
        bk_file.close()

        facts_file = open("{}/train_facts.txt".format(working_dir), 'w')
        for pred in facts_per_predicate:
            for elem in facts_per_predicate[pred]:
                facts_file.write("{}({}).\n".format(pred.lower(), ", ".join([x.lower() for x in elem])).replace("%", "").replace("\\", "").replace("=", "").replace("_", "XX"))
                #train_data.append("{}({}).".format(pred.lower(), ", ".join([x.lower() for x in elem])).replace("%", "").replace("\\", "").replace("=", "").replace("_", "XX"))

        facts_file.close()

        if not self.link_prediction:
            pos_ex_file = open("{}/train_pos.txt".format(working_dir), 'w')
            neg_ex_file = open("{}/train_neg.txt".format(working_dir).format(working_dir), 'w')

            if len(focus_label) > 1:
                for item in training_labels:
                    if training_labels[item].lower() == focus_label.lower():
                        pos_ex_file.write(
                            "{}({}).\n".format(focus_label.lower(), item.lower()).replace("%", "").replace(
                                "\\", "").replace("=", "").replace("_", "XX"))
                        #train_pos.append("{}({}).".format(focus_label.lower(), item.lower()).replace("%", "").replace("\\", "").replace("=", "").replace("_", "XX"))
                    else:
                        neg_ex_file.write(
                            "{}({}).\n".format(focus_label.lower(), item.lower()).replace("%", "").replace(
                                "\\", "").replace("=", "").replace("_", "XX"))
                        #train_neg.append("{}({}).".format(focus_label.lower(), item.lower()).replace("%", "").replace("\\", "").replace(  "=", "").replace("_", "XX"))

            else:
                for item in training_labels:
                    pos_ex_file.write("{}({}).\n".format(training_labels[item].lower(), item.lower()).replace("%", "").replace("\\", "").replace("=", "").replace("_", "XX"))
                    #train_pos.append("{}({}).".format(training_labels[item].lower(), item.lower()).replace("%", "").replace("\\", "").replace("=", "").replace("_", "XX"))

                possible_labels = set(self.labels.values())
                for entity in training_labels:
                    for lab in [x for x in possible_labels if x != training_labels[entity]]:
                        neg_ex_file.write("{}({}).\n".format(lab.lower(), entity.lower()).replace("%", "").replace("\\", "").replace("=", "").replace("_", "XX"))
                        #train_neg.append("{}({}).".format(lab.lower(), entity.lower()).replace("%", "").replace("\\", "").replace("=", "").replace("_", "XX"))

            pos_ex_file.close()
            neg_ex_file.close()
        else:
            pos_ex_file = open("{}/train_pos.txt".format(working_dir), 'w')
            neg_ex_file = open("{}/train_neg.txt".format(working_dir), 'w')

            for item in training_labels:
                elems = ', '.join([x.capitalize() for x in item.split(",")])
                if training_labels[item] == "pos":
                    pos_ex_file.write("targetpred({}).\n".format(elems))
                    #train_pos.append("targetpred({}).".format(elems))
                else:
                    neg_ex_file.write("targetpred({}).\n".format(elems))
                    #train_neg.append("targetpred({}).".format(elems))

            pos_ex_file.close()
            neg_ex_file.close()

        #return train_data, train_pos, train_neg

    def _prepare_test_data(self, facts_per_predicate, test_labels, focus_label, working_dir):
        test_data = []
        test_pos = []
        test_neg = []
        bk_file = open("{}/test_bk.txt".format(working_dir), 'w')

        bk_filename = "background.txt"
        if len(focus_label) > 1:
            bk_filename = "background_{}.txt".format(focus_label)

        bk_file.write('import: "../' + bk_filename + '"' + "\n")

        bk_file.close()

        facts_file = open("{}/test_facts.txt".format(working_dir), 'w')
        for pred in facts_per_predicate:
            for elem in facts_per_predicate[pred]:
                facts_file.write("{}({}).\n".format(pred.lower(), ", ".join([x.lower() for x in elem])).replace("%", "").replace("\\", "").replace("=", "").replace("_", "XX"))
                test_data.append("{}({}).".format(pred.lower(), ", ".join([x.lower() for x in elem])).replace("%", "").replace("\\", "").replace("_", "XX"))

        facts_file.close()

        neg_ex_file = open("{}/test_neg.txt".format(working_dir), 'w')
        pos_ex_file = open("{}/test_pos.txt".format(working_dir), 'w')

        for item in test_labels:
            if not self.link_prediction:
                if len(focus_label) < 1 or focus_label.lower() == test_labels[item]:
                    pos_ex_file.write("{}({}).\n".format(test_labels[item].lower(), item.lower()).replace("%", "").replace("\\", "").replace("=", "").replace("_", "XX"))
                    #test_pos.append("{}({}).".format(test_labels[item].lower(), item.lower()).replace("%", "").replace("\\", "").replace("=", "").replace("_", "XX"))
            else:
                if test_labels[item] == "pos":
                    elems = ', '.join([x.capitalize() for x in item.split(",")])
                    pos_ex_file.write("targetpred({}).\n".format(elems))
                    #test_pos.append("targetpred({}).".format(elems))
                else:
                    elems = ', '.join([x.capitalize() for x in item.split(",")])
                    neg_ex_file.write("targetpred({}).\n".format(elems))
                    #test_neg.append("targetpred({}).".format(elems))

        possible_labels = set(self.labels.values())
        for item in test_labels:
            if not self.link_prediction:
                if len(focus_label) < 1:
                    for lab in [x for x in possible_labels if x != test_labels[item]]:
                        neg_ex_file.write("{}({}).\n".format(lab.lower(), item.lower()).replace("%", "").replace("\\", "").replace("=", "").replace("_", "XX"))
                        #test_neg.append("{}({}).".format(lab.lower(), item.lower()).replace("%", "").replace("\\", "").replace("=", "").replace("_", "XX"))
                else:
                    if focus_label != test_labels[item]:
                        neg_ex_file.write(
                            "{}({}).\n".format(focus_label.lower(), item.lower()).replace("%", "").replace("\\", "").replace(
                                "=", "").replace("_", "XX"))
                        #test_neg.append("{}({}).".format(focus_label.lower(), item.lower()).replace("%", "").replace("\\", "").replace("=","").replace("_", "XX"))


        pos_ex_file.close()
        neg_ex_file.close()

    def _individual_run(self, conf):
        predicate_definitions = self.predicate_definitions
        constants = self.constants_per_domain
        facts = self.facts_per_predicates

        possible_labels = set(self.labels.values())
        if self.link_prediction:
            possible_labels = set("targetpred")
        if len(self.positive_class) > 1:
            possible_labels = set([self.positive_class])

        current_working_directory = self.working_directory + "/boostsrl_{}_{}_{}_{}".format(conf['nt'], conf['mtd'], conf['ns'], conf['nc'])

        if 'cwd' in conf:
            current_working_directory = conf['cwd']

        if not os.path.exists(current_working_directory):
            os.mkdir(current_working_directory)

        test_entities = [self.entity_order_for_labels[x] for x in conf['test_indices']]

        final_test_labels = {}
        for ent in test_entities:
            final_test_labels[ent] = self.labels[ent]

        final_labels = {}
        for ent in self.entity_order_for_labels:
            if ent not in test_entities:
                final_labels[ent] = self.labels[ent]

        test_entities = [self.entity_order_for_labels[x] for x in conf['test_indices']]
        train_entities = [x for x in self.entity_order_for_labels if x not in test_entities]

        where_was_I = os.getcwd()
        os.chdir(current_working_directory)

        if not os.path.exists(current_working_directory):
            os.mkdir(current_working_directory)

        for label_to_focus in possible_labels:

            train_dir = current_working_directory + "/train_" + label_to_focus
            test_dir = current_working_directory + "/test_" + label_to_focus
            model_dor = current_working_directory + "/model_" + label_to_focus

            if not os.path.exists(train_dir):
                os.mkdir(train_dir)

            if not os.path.exists(test_dir):
                os.mkdir(test_dir)

            # preparing data
            self._prepare_background(conf['mtd'], conf['ns'], conf['nc'], predicate_definitions, final_labels, constants, label_to_focus, current_working_directory)
            self._prepare_training_data(facts, final_labels, label_to_focus, train_dir)
            self._prepare_test_data(facts, final_test_labels, label_to_focus, test_dir)

            cmd = ""
            if self.link_prediction:
                cmd = "java -jar {}/BoostSRL.jar -l -combine -i -train {} -test {} -model {} -target targetpred -trees {}".format(os.path.dirname(__file__) + "/exec", train_dir, test_dir, model_dor, conf['nt'])
            else:
                possible_labels = set(self.labels.values())
                cmd = "java -jar {}/BoostSRL.jar -l -combine -i -save -train {} -test {} -model {} -target {} -trees {} -aucJarPath {}".format(
                    os.path.dirname(__file__) + "/exec", train_dir, test_dir,
                    test_dir, ','.join([x.lower() for x in possible_labels]).replace("%", "").replace("\\", "").replace("=", "").replace("_", "XX"), conf['nt'],
                    os.path.dirname(__file__) + "/exec")

            cmd_file = open("{}/cmd".format(current_working_directory), 'w')
            cmd_file.write(cmd)
            cmd_file.close()

            p = subprocess.Popen(cmd, shell=True, cwd=current_working_directory)
            p.wait()

            # train model
            # background = boostsrl.modes(bk, ['targetpred'], maxTreeDepth=conf['mtd'], nodeSize=conf['ns'], numOfClauses=conf['nc'], usePrologVariables=True)
            # model = boostsrl.train(background, train_pos, train_neg, train_facts)

            # test model
            # test = boostsrl.test(model, test_pos, [], test_facts)

            count_correct = 0
            count_total = 0
            """
            for (item, prob) in test.inference_results('targetpred'):
                if prob >= 0.5:
                    count_correct += 1
                count_total += 1
            """
            acc = float(count_correct)/float(count_total)

            os.chdir(where_was_I)

        return {'nt': conf['nt'], 'mtd': conf['mtd'], 'ns': conf['ns'], 'nc': conf['nc'], 'accuracy': acc}

    def _fit_with_params(self, training_data, labels):
        if len(labels) < 1:
            results = {}

            random.shuffle(self.entity_order_for_labels)

            chunk_size = int(len(self.entity_order_for_labels) / self.n_folds)

            for batch in range(self.n_folds):
                test_indices = range(batch * chunk_size, (batch + 1) * chunk_size)

                confs = []
                for nt in self.parameters.get_parameter('ntrees'):
                    for mtd in self.parameters.get_parameter('max_tree_depth'):
                        for ns in self.parameters.get_parameter('node_size'):
                            for nc in self.parameters.get_parameter('max_of_clauses'):
                                confs.append({'nt': nt, 'mtd': mtd, 'ns': ns, 'nc': nc, 'test_indices': test_indices})

                self.progress_bar = Bar('Processing', max=len(confs),
                                        suffix='%(index)d / %(max)d  %(percent)d%%  avg time: %(avg)ds ETA: %(eta)ds')
                accuracies = map(self._individual_run, confs)
                self.progress_bar.finish()

                for item in accuracies:
                    param_dict = (
                    ('nt', item['nt']), ('mtd', item['mtd']), ('ns', item['ns']), ('nc', item['nc']))

                    if param_dict not in results:
                        results[param_dict] = []
                    results[param_dict].append(item['accuracy'])
            summaries = {}
            for elem in results:
                summaries[elem] = np.array(results[elem]).mean()

            best_params = summaries.keys()
            best_params = sorted(best_params, key=lambda x: summaries[x], reverse=True)[0]

            print("FOUND BEST PARAMETERS: ", best_params)
            return dict(best_params)
        else:
            predicate_definitions = training_data[1]
            final_labels = self.labels
            # for item in labels:
            #    final_labels[item] = labels[item]
            constants = self.constants_per_domain
            facts = training_data[0]

            print(" READING BEST PARAMETERS: ", self.best_parameters)
            nt = self.best_parameters['nt']
            mtd = self.best_parameters['mtd']
            ns = self.best_parameters['ns']
            nc = self.best_parameters['nc']

            train_indices = range(len(self.entity_order_for_labels))

            self.entity_order_for_labels = self.entity_order_for_labels + list(labels.keys())

            test_indices = range(len(train_indices), len(self.entity_order_for_labels))
            cwd = self.working_directory + "/final"
            if not os.path.exists(cwd):
                os.mkdir(cwd)

            conf = {'nt': nt, 'mtd': mtd, 'ns': ns, 'nc': nc, 'test_indices': test_indices, 'cwd': cwd}

            for elem in labels:
                self.labels[elem] = labels[elem]

            res = self._individual_run(conf)

            return res['accuracy']
