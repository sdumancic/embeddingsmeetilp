import numpy as np
import numpy.linalg as la
import math


class EmbeddingAbstractor:

    def __init__(self, method, threshold=50):
        self.entity_embeddings = {}
        self.relation_embeddings = {}
        self.method = method
        self.constants_per_domain = {}
        self.relation_definitions = {}
        self.threshold = threshold

    def get_entity_embedding(self, entity):
        return self.entity_embeddings[entity]

    def get_relations(self):
        return list(self.relation_embeddings.keys())

    def has_relation_embeddings(self):
        return len(self.relation_embeddings) > 0

    def read_entity_embeddings(self, embeddings_file):
        for line in open(embeddings_file).readlines():
            if len(line) < 3:
                pass
            ent, emb = line.strip().split()
            emb = [float(x) for x in emb.split(",")]

            self.entity_embeddings[ent.lower()] = np.array(emb)

    def load_entity_embeddings(self, entity_emb_map):
        self.entity_embeddings = entity_emb_map

    def read_relation_embeddings(self, embeddings_file):
        for line in open(embeddings_file).readlines():
            if len(line) < 3:
                pass
            ent, emb = line.strip().split()
            emb = [float(x) for x in emb.split(",")]

            self.relation_embeddings[ent.lower()] = np.array(emb)

    def _angle_between_relations(self, relation1, relation2):
        relation1 = self.relation_embeddings[relation1]
        relation2 = self.relation_embeddings[relation2]

        # cosang = np.dot(relation1, relation2)
        # sinang = la.norm(np.cross(relation1, relation2))
        # radian = np.arctan2(sinang, cosang)

        dot = np.dot(relation1, relation2)
        x_modulus = np.sqrt((relation1 * relation1).sum())
        y_modulus = np.sqrt((relation2 * relation2).sum())
        cos_angle = dot / x_modulus / y_modulus
        radian = np.arccos(cos_angle)

        return int(np.rad2deg(radian))

    def get_angle_triplets(self):
        facts = []
        relations = list(self.relation_embeddings.keys())

        for i in range(len(relations) - 1):
            for j in range(i+1, len(relations)):
                score = int(self._angle_between_relations(relations[i], relations[j]))
                facts.append([relations[i], relations[j], str(score)])

        return ["relation", "relation", "number"], facts

    def get_entity_distance(self, entity1, entity2):
        entity1 = self.entity_embeddings[entity1]
        entity2 = self.entity_embeddings[entity2]
        distance = la.norm(entity1 - entity2)

        return round(distance, 2)

    def get_distance_triplets(self):
        facts = []
        entities = list(self.entity_embeddings.keys())

        for i in range(len(entities) -1):
            for j in range(i+1, len(entities)):
                score = self.get_entity_distance(entities[i], entities[j])
                facts.append([entities[i], entities[j], str(score)])

        return ["entity", "entity", "number"], facts

    def _get_relation_distance(self, relation1, relation2):
        relation1 = self.relation_embeddings[relation1]
        relation2 = self.relation_embeddings[relation2]
        distance = la.norm(relation1 - relation2)

        return round(distance, 2)

    def get_relation_distance_triplets(self):
        facts = []
        relations = list(self.relation_embeddings.keys())

        for i in range(len(relations)-1):
            for j in range(i+1, len(relations)):
                score = self._get_relation_distance(relations[i], relations[j])
                facts.append([relations[i], relations[j], str(score)])

        return ["relation", "relation", "number"], facts

    def _get_distmult_score(self, entity1, entity2, relation):
        score = np.dot(np.multiply(self.entity_embeddings[entity1], self.entity_embeddings[entity2]),
                       self.relation_embeddings[relation])
        return score

    def _get_transe_score(self, entity1, entity2, relation):
        difference = la.norm(self.entity_embeddings[entity1] + self.relation_embeddings[relation] - self.entity_embeddings[entity2])
        return np.exp(-difference)

    def get_score(self, entity1, entity2, relation, method):
        if method.lower() == 'transe':
            return self._get_transe_score(entity1, entity2, relation)
        elif method.lower() == 'distmult':
            return self._get_distmult_score(entity1, entity2, relation)
        else:
            return self._get_transe_score(entity1, entity2, relation)

    def get_score_triplets(self, method='transe'):
        entities = list(self.entity_embeddings.keys())
        facts = []

        for i in range(len(entities) -1):
            for j in range(i+1, len(entities)):
                for rel in self.relation_embeddings:
                    score = self.get_score(entities[i], entities[j], rel, method)
                    facts.append([entities[i], rel, entities[j], str(score)])

        return ["entity", "relation", "entity", "number"], facts



