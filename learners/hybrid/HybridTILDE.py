from learners.hybrid.EmbeddingAbstractor import EmbeddingAbstractor
from learners.relational.TILDE import TILDE
import os
import subprocess


class HybridTILDE(TILDE):

    def __init__(self, params, working_directory, method):
        TILDE.__init__(self, params, working_directory)
        self.embeddingHandler = EmbeddingAbstractor(method, params.get_parameter("threshold"))
        self.r_triplet_names = {}
        self.embedding_method = method
        self.exclude_from_bias = [] # ["etriplet"]

    def read_entity_embeddings(self, embeddings_file):
        self.embeddingHandler.read_entity_embeddings(embeddings_file)

    def read_relation_embeddings(self, embeddings_file):
        self.embeddingHandler.read_relation_embeddings(embeddings_file)

    def _read_facts(self, facts_file, predicate_definitions):
        facts_per_predicate = {}
        constants_per_domain = {}

        for line in open(facts_file).readlines():
            if len(line) < 3:
                continue

            pred, args = line.strip().replace(")", "").split("(")
            args = [x.lower() for x in args.split(",")]
            pred = pred.lower()

            relation = "rtriplet"
            # if (args[0], pred, args[1]) in self.r_triplet_names:
            #    relation = self.r_triplet_names[(args[0], pred, args[1])]
            # else:
            #    self.r_triplet_names[(args[0], pred, args[1])] = "rtriplet" + str(len(self.r_triplet_names))
            #    relation = self.r_triplet_names[(args[0], pred, args[1])]

            if relation not in facts_per_predicate:
                facts_per_predicate[relation] = set()

            facts_per_predicate[relation].add((args[0], pred, args[1]))

            for (c, d) in zip(args, ["entity", "entity"]):  # predicate_definitions[pred]):
                if d not in constants_per_domain:
                    constants_per_domain[d] = set()
                constants_per_domain[d].add(c)

            if 'relation' not in constants_per_domain:
                constants_per_domain['relation'] = set()
            constants_per_domain['relation'].add(relation)

        return facts_per_predicate, constants_per_domain

    def _enhance_data_with_embeddings(self):
        print(".... processing angles")
        angle_pred, angle_facts = self.embeddingHandler.get_angle_triplets()
        self.predicate_definitions["angle"] = angle_pred
        self.facts_per_predicates["angle"] = angle_facts

        print(".... processing relation distances")
        rel_distance_pred, rel_distance_facts = self.embeddingHandler.get_relation_distance_triplets()
        self.predicate_definitions["rdistance"] = rel_distance_pred
        self.facts_per_predicates["rdistance"] = rel_distance_facts

        print(".... processing entity distances")
        entity_distance_pred, entity_distance_facts = self.embeddingHandler.get_distance_triplets()
        self.predicate_definitions["edistance"] = entity_distance_pred
        self.facts_per_predicates["edistance"] = entity_distance_facts

        print(".... processing scores")
        scores_pred, scores_fact = self.embeddingHandler.get_score_triplets(self.embedding_method)
        self.predicate_definitions["etriplet"] = scores_pred
        self.facts_per_predicates["etriplet"] = scores_fact

        # self.predicate_definitions["etriplet_conf"] = ["entity", "relation", "entity", "number"]
        # self.predicate_definitions["translatable"] = ["entity", "entity", "number"]

    def _prepare_background_file(self, test_labels, working_directory):
        bg_file = open("{}/train.bg".format(working_directory), 'w')

        # bg_file.write("etriplet_conf(X,Y,Z,C) :- etriplet(X,Y,Z,Confidence),Confidence > C.\n")
        # bg_file.write("translatable(X,Y,C) :- etriplet_conf(X,Z,Y,C).\n")
        # bg_file.write("translatable(X,Y,C) :- etriplet_conf(X,M,Z,C),translatable(Z,Y,C).\n")

        for item in test_labels:
            bg_file.write("testid({},testset).\n".format(item))

        bg_file.close()

    def _prepare_background_file_training(self, working_directory):
        bg_file = open("{}/train.bg".format(working_directory), 'w')

        # bg_file.write("etriplet_conf(X,Y,Z,C) :- etriplet(X,Y,Z,Confidence),Confidence > C.\n")
        # bg_file.write("translatable(X,Y,C) :- etriplet(X,Z,Y,Conf),Conf>C.\n")
        # bg_file.write("translatable(X,Y,C) :- etriplet(X,M,Z,Conf),Conf>C,translatable(Z,Y,C).\n")

        bg_file.close()

    def _prepare_settings_file(self, definitions, labels, constants, heuristic, minimal_cases, accuracy, working_directory):
        settings_file = open("{}/train.s".format(working_directory), 'w')
        settings_file.write("load(key).\n")
        settings_file.write("talking(4).\n")
        settings_file.write("autoload packages(on).\n")
        settings_file.write("huge(on).\n")
        settings_file.write("output_options([c45,prolog]).\n")
        settings_file.write("bias(rmode).\n")
        settings_file.write("classes([{}]).\n".format(",".join(set([x.lower() for x in labels.values()]))))
        settings_file.write("typed_language(yes).\n")
        settings_file.write("heuristic({}).\n".format(heuristic))
        settings_file.write("pruning(none).\n")
        settings_file.write("tilde_mode(classify).\n")
        settings_file.write("minimal_cases({}).\n".format(minimal_cases))
        settings_file.write("accuracy({}).\n".format(accuracy))
        settings_file.write("predict(targetpred(+{},-class)).\n".format(self._find_target_domain(labels, constants).lower()))
        settings_file.write("\n")

        for pred in definitions:
            settings_file.write("type({}({})).\n".format(pred.lower(), ",".join([x.lower() for x in definitions[pred]])))

        settings_file.write("type(rtriplet(entity,relation,entity)).\n")
        settings_file.write("type(number < number).\n")
        settings_file.write("type(number > number).\n")
        settings_file.write("\n")

        #for pred in definitions:
        #    rep = ""
        #    if pred in self.exclude_from_bias:
        #        continue
        #    if len(definitions[pred]) == 1:
        #        rep = "rmode({}(+V)).\n".format(pred.lower)
        #    else:
        #        vars = zip(["#" if x in ['relation', 'number'] else "+-V" for x in definitions[pred]], range(len(definitions[pred])))
        #        vars = ["{}{}".format(x[0], x[1]) if x[0] != '#' else x[0] for x in vars]
        #        rep = "rmode({}({})).\n".format(pred.lower(), ",".join(vars))

        #    settings_file.write(rep)

        settings_file.write("rmode(rtriplet(+-V1,#,+-V2)).\n")
        if self.embedding_method.lower() == 'transe':
            settings_file.write("rmode((etriplet(+-V1,#,+-V2,Y), Y > #Y)).\n")
        elif self.embedding_method.lower == 'distmult':
            settings_file.write("rmode((etriplet(+-V1,#,+-V2,Y), Y < #Y)).\n")
        settings_file.write("rmode((edistance(+-V1,+-V2,Y),Y < #Y)).\n")
        settings_file.write("rmode((rdistance(+V1,#,Y),Y < #Y)).\n")
        settings_file.write("rmode((rdistance(#,+V,Y),Y < #Y)).\n")
        settings_file.write("rmode((rdistance(+V1,-V2,Y),Y < #Y)).\n")
        settings_file.write("rmode((rdistance(-V1,+V2,Y),Y < #Y)).\n")
        settings_file.write("rmode((angle(+V1,-V2,Y),Y < #Y)).\n")
        settings_file.write("rmode((angle(-V1,+V2,Y),Y < #Y)).\n")
        settings_file.write("rmode((angle(+V1,#,Y),Y < #Y)).\n")
        settings_file.write("rmode((angle(#,+V2,Y),Y < #Y)).\n")
        settings_file.write("rmode((angle(+V1,-V2,Y),Y > #Y)).\n")
        settings_file.write("rmode((angle(-V1,+V2,Y),Y > #Y)).\n")
        settings_file.write("rmode((angle(+V1,#,Y),Y > #Y)).\n")
        settings_file.write("rmode((angle(#,+V,Y),Y > #Y)).\n")
        # settings_file.write("rmode(translatable(+V1,-V2,Y)).\n")
        # settings_file.write("rmode(+number < +number).\n")

        settings_file.close()

    def _individual_run(self, conf):
        predicate_definitions = self.predicate_definitions
        final_labels = self.labels
        constants = self.constants_per_domain
        facts = self.facts_per_predicates
        current_working_directory = self.working_directory + "/tilde_{}_{}_{}".format(conf['h'], conf['mc'], conf['acc'])

        if not os.path.exists(current_working_directory):
            os.mkdir(current_working_directory)

        self._prepare_settings_file(predicate_definitions, final_labels, constants, conf['h'], conf['mc'], conf['acc'],
                                    current_working_directory)
        self._prepare_facts_and_labels_file(facts, final_labels, [], current_working_directory)
        self._prepare_background_file_training(current_working_directory)

        cmd = "echo \"nfold(tilde,4)\" | {}/bin/ace > log.txt".format(os.environ['ACE_ILP_ROOT'])

        proc = subprocess.Popen(cmd, shell=True, cwd=current_working_directory)
        proc.wait()

        acc = 0.0
        start_parsing_test = False
        for line in open("{}/tilde/train.summary.uB".format(current_working_directory)).readlines():
            if 'Testing' in line:
                start_parsing_test = True
            elif start_parsing_test and 'Accuracy' in line:
                tmp = line.split("(")[0]
                acc = float(tmp.split(":")[1].strip())

        return {'h': conf['h'], 'mc': conf['mc'], 'acc': conf['acc'], 'accuracy': acc}

    def learn_and_evaluate(self, training_data_file, training_labels_file, test_data_file="", test_labels_file="",
                           definitions=""):
        self.predicate_definitions = self._read_definitions(definitions)

        for t_file in training_data_file:
            facts, constants = self._read_facts(t_file, self.predicate_definitions)

            for pred in facts:
                if pred not in self.facts_per_predicates:
                    self.facts_per_predicates[pred] = set()
                for elem in facts[pred]:
                    self.facts_per_predicates[pred].add(elem)

            for d in constants:
                if d not in self.constants_per_domain:
                    self.constants_per_domain[d] = set()
                for c in constants[d]:
                    self.constants_per_domain[d].add(c)

        for l_file in training_labels_file:
            labels = self._read_labels(l_file)

            for item in labels:
                self.labels[item] = labels[item]

        self._enhance_data_with_embeddings()
        self.best_parameters = self._fit_with_params((self.facts_per_predicates, self.predicate_definitions), [])

        test_facts, test_constants = self._read_facts(test_data_file, self.predicate_definitions)

        for pred in test_facts:
            for elem in test_facts[pred]:
                self.facts_per_predicates[pred].add(elem)

        for d in test_constants:
            for e in test_constants[d]:
                self.constants_per_domain[d].add(e)

        test_labels = self._read_labels(test_labels_file)

        final_performance = self._fit_with_params((self.facts_per_predicates, self.predicate_definitions), test_labels)

        return final_performance
