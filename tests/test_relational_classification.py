import os
from learners.RelationalLearner import TILDE, Aleph, AMIE, Metagol, KFoil, BoostSRL
from learners.LearnerSettings import LearnerSettings


DATA_ROOT = os.path.dirname(__file__) + "/../data/classification/2_folds/mutagenesis/mutagenesis.1.db"
TRAINING_FILE = DATA_ROOT + "/training.db"
TRAINING_LABELS = DATA_ROOT + "/training.labels"
TEST_FILE = DATA_ROOT + "/test.db"
TEST_LABELS = DATA_ROOT + "/test.labels"
DEFINITIONS = os.path.dirname(__file__) + "/../data/classification/1_raw/mutagenesis/predicate.defs"

PREDICATE_BIAS = {
    'mutagenesis': {
        'bond_bondtype1': ['(+aatom, -aatom)', '(-aatom, +aatom)'],
        'bond_bondtype2': ['(+aatom, -aatom)', '(-aatom, +aatom)'],
        'bond_bondtype3': ['(+aatom, -aatom)', '(-aatom, +aatom)'],
        'bond_bondtype4': ['(+aatom, -aatom)', '(-aatom, +aatom)'],
        'bond_bondtype5': ['(+aatom, -aatom)', '(-aatom, +aatom)'],
        'bond_bondtype7': ['(+aatom, -aatom)', '(-aatom, +aatom)'],
        'element': ['(+aatom, #typee)'],
        'atype': ['(+aatom, #typea)'],
        'logp': ['(+mol, #typlelog)'],
        'inda': ['(+mol, #typeia)'],
        'mol2atm': ['(+mol, -aatom)', '(-mol, +aatom)'],
        'ind1': ['(+mol, #typeio)'],
        'lumo': ['(+mol, #typel)'],
        'charge': ['(+aatom, #typec)']
    }
}

if __name__ == '__main__':
    parameters = LearnerSettings()
    parameters.add_parameter('aleph_nodes', 15000)
    parameters.add_parameter('kfoild_max_clauses', [100])
    parameters.add_parameter('kfoil_max_literals', [2])
    parameters.add_parameter('kfoil_beam_size', [3])
    parameters.add_parameter('n_folds', 2)
    parameters.add_parameter('ntrees', [5])
    parameters.add_parameter('max_tree_depth', [3])
    parameters.add_parameter('node_size', [1])
    parameters.add_parameter('max_of_clauses', [8])

    # t = KFoil(parameters, os.environ['KFOIL_HOME'], "test")
    t = BoostSRL(parameters, "/home/seb/Documents/workspace/python/embeddingsmeetilp/tests/test")
    t.read_bias(PREDICATE_BIAS['mutagenesis'])
    res = t.learn_and_evaluate([TRAINING_FILE], [TRAINING_LABELS], test_data_file=TEST_FILE, test_labels_file=TEST_LABELS,
                               definitions=DEFINITIONS)
    print("BoostSRL: ", res)
