import os
from learners.LearnerSettings import LearnerSettings
from learners.PropositionalPrediction import DecisionTree, kNN, SVM

EMBEDDING_FILE = os.path.dirname(__file__) + "/data/iris/entities.emb"
TRAIN_LABELS = os.path.dirname(__file__) + "/data/iris/train.labels"
TEST_LABELS = os.path.dirname(__file__) + "/data/iris/test.labels"

if __name__ == '__main__':
    parameters = LearnerSettings()

    dt = kNN(parameters)
    print("kNN: ", dt.learn_and_evaluate(EMBEDDING_FILE, TRAIN_LABELS, test_labels_file=TEST_LABELS))

    dt = DecisionTree(parameters)
    print("DecisionTree: ", dt.learn_and_evaluate(EMBEDDING_FILE, TRAIN_LABELS, test_labels_file=TEST_LABELS))

    dt = SVM(parameters)
    print("SVM: ", dt.learn_and_evaluate(EMBEDDING_FILE, TRAIN_LABELS, test_labels_file=TEST_LABELS))
