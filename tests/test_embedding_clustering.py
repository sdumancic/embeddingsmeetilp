import os
from learners.LearnerSettings import LearnerSettings
from learners.PropositionalClustering import Hierarchical, Spectral

EMBEDDING_FILE = os.path.dirname(__file__) + "/data/iris/entities.emb"
LABELS = os.path.dirname(__file__) + "/data/iris/training.labels"

parameters = LearnerSettings()
parameters.add_parameter("n_clusters", 3)

spectral = Spectral(parameters)
print(spectral.learn_and_evaluate(EMBEDDING_FILE, LABELS))

hierarchical = Hierarchical(parameters)
print(hierarchical.learn_and_evaluate(EMBEDDING_FILE, LABELS))
