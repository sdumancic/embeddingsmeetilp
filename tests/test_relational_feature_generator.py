import os
from learners.FeatureGenerators import AlephFeatureGenerator, AlephFeatureGeneratorPerEntity
from learners.LearnerSettings import LearnerSettings


DATA_ROOT = os.path.dirname(__file__) + "/../data/classification/2_folds/mutagenesis/mutagenesis.3.db"
TRAINING_FILE = DATA_ROOT + "/training.db"
TRAINING_LABELS = DATA_ROOT + "/ltraining.labels"
TEST_FILE = DATA_ROOT + "/test.db"
TEST_LABELS = DATA_ROOT + "/test.labels"
DEFINITIONS = os.path.dirname(__file__) + "/../data/classification/1_raw/mutagenesis/predicate.defs"

if __name__ == '__main__':
    parameters = LearnerSettings()
    parameters.add_parameter('aleph_nodes', 15000)
    parameters.add_parameter('aleph_clause_length', [10])
    parameters.add_parameter('aleph_min_acc', [0.1])
    parameters.add_parameter('aleph_min_pos', [2])

    t = AlephFeatureGeneratorPerEntity(parameters, "/Users/seb/Documents/workspace/test")
    res = t.find_features([TRAINING_FILE], DEFINITIONS)
    print("Aleph: ", res)
