import os
from learners.RelationalLearner import ETilde, DETilde, AlephEm
from learners.LearnerSettings import LearnerSettings

DATA_ROOT = os.path.abspath(os.path.dirname(__file__)) + "/../data/classification"

REL_DATA_ROOT = DATA_ROOT + "/2_folds/hepatitis/hep.1.db"
EMB_DATA_ROOT = DATA_ROOT + "/4_embeddings/hepatitis/hep.1.db"

REL_DATA_TRAINING = REL_DATA_ROOT + "/training.db"
REL_DATA_TRAINING_LABELS = REL_DATA_ROOT + "/training.labels"

REL_DATA_TEST = REL_DATA_ROOT + "/test.db"
REL_DATA_TEST_LABELS = REL_DATA_ROOT + "/test.labels"

ENT_EMB_DATA = EMB_DATA_ROOT + "/transE_dim10_epoch20.entity.emb"
REL_EMB_DATA = EMB_DATA_ROOT + "/transE_dim10_epoch20.relation.emb"

WORK_DIR = os.path.abspath(os.path.dirname(__file__) + "/../results/scratch")

if not os.path.exists(WORK_DIR):
    os.mkdir(WORK_DIR)

WORK_DIR = WORK_DIR + "/hepatitis"

if not os.path.exists(WORK_DIR):
    os.mkdir(WORK_DIR)

DEFINITIONS_FILE = DATA_ROOT + "/1_raw/hepatitis/predicate.defs"

parameters = LearnerSettings()
parameters.add_parameter("minimal_cases", [2, 6, 8])
parameters.add_parameter("accuracy", [0.8, 1.0])
parameters.add_parameter('subset', 0.01)

learner = AlephEm(parameters, WORK_DIR, "transe")

learner.read_entity_embeddings(ENT_EMB_DATA)
# learner.read_relation_embeddings(REL_EMB_DATA)

res = learner.learn_and_evaluate([REL_DATA_TRAINING], [REL_DATA_TRAINING_LABELS], test_data_file=REL_DATA_TEST,
                                 test_labels_file=REL_DATA_TEST_LABELS, definitions=DEFINITIONS_FILE)

print(res)
