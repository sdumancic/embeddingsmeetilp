import os
import itertools
import subprocess
import pickle
import datetime
import itertools
import networkx as nx

RELATIONAL_DATA = ['fb15k-237-lf'] # ['fb15k-237', 'wn18-rr']  # 'yago10'
RELATIONAL_ALGO = ['TILDE']  # , 'AMIE', 'Aleph']

DATA_ROOT = os.path.abspath(os.path.dirname(__file__)) + "/../data/kbc/2_process"
OUT_ROOT = os.path.abspath(os.path.dirname(__file__)) + "/../data/kbc/3_results"

SCRATCH_SPACE = os.path.abspath(os.path.dirname(__file__)) + "/../results/scratch/KBC"

LOCAL_PROLOG = "/cw/dtailocal/seba/embvsilp/KBC"
TILDE_LEARNING_WITH_INDUCE = False
RESULT_COLLECTION = False
TRAINED_WITH_LABELS = True  # whether [pos] and [neg] were used as labels
RANKINGS_FILE_APPENDIX = "full"


def arguments_connected(arg_list, head_arguments):
    g = nx.Graph()
    for edge in arg_list:
        g.add_edge(edge[0], edge[1])

    return nx.has_path(g, source=head_arguments[0], target=head_arguments[1])


def is_rule_bounded(rule):
    head, tail = rule.split(":-")
    head_args = head.strip().replace(')', '').split('(')[1].split(',')
    if '[pos]' in head_args or '[neg]' in head_args:
        head_args = [x for x in head_args if x not in ['[pos]', '[neg]']]

    tail = tail.replace('),!.', '').replace('), !.', '').split("),")
    tail = [x.split('(')[1].split(',') for x in tail]
    tail_args = set(itertools.chain(*tail))

    result = all([True if x in tail_args else False for x in head_args]) and arguments_connected(tail, head_args)
    print("clause: ", rule, "result: ", result)
    return result


def generalise_rule(rule):
    head, tail = rule.split(":-")
    head_args = head.strip().replace(')', '').split('(')[1].split(',')
    if '[pos]' in head_args or '[neg]' in head_args:
        head_args = [x for x in head_args if x not in ['[pos]', '[neg]']]

    tail = tail.replace('),!.', '').replace('), !.', '').split("),")
    tail = [x.split('(')[1].split(',') for x in tail]
    tail_args = set(itertools.chain(*tail))

    head_consts = [x for x in head_args if '[' in x and ']' in x]
    tail_consts = [x for x in tail_args if '[' in x and ']' in x]

    if len(head_consts) == len(tail_consts):
        new_rule = rule
        substitutes = [chr(ord('M') + x) for x in range(len(head_consts))]
        substitutes = dict(zip(head_consts, substitutes))

        for x in substitutes:
            new_rule = new_rule.replace(x, substitutes[x])

        return new_rule
    else:
        return rule


def get_program(file, relation_name, override_unbounded=True, remove_cut=True, with_labels=True):
    """
    Reads TILDE program
    :param file:  *.uLtestset file containing the TILDE program
    :param relation_name: the rel relation name
    :return: a list of tuples (rule in prolog format, confidence of the rule)
    """
    rules = []
    rule_stats = []
    all_used_predicates = set()

    if os.path.exists(file):

        summary_file = open(file)

        reading_flag = False

        current_rule = ""
        current_probability = ""
        current_coverage = ''

        update_probability = False
        updating_negative = False

        for line in summary_file.readlines():
            if not reading_flag and line.startswith('Equivalent prolog program:'):
                reading_flag = True
            elif reading_flag and with_labels and line.find('[pos]', 0, 20) > 0 and line.find(':-', 10, 30) > 0:
                current_rule = line.strip()
                update_probability = True
            elif reading_flag and with_labels and line.find('[neg]', 0, 20) > 0 and line.find(':-', 10, 30) > 0:
                current_rule = line.strip().replace('[neg]', '[pos]')
                update_probability = True
                updating_negative = True
            elif reading_flag and not with_labels and line.find(":-") > 0:
                current_rule = line.strip()
                current_rule = current_rule.replace(") :-", ",[pos]) :-")
                current_rule = generalise_rule(current_rule)
                update_probability = True
            elif reading_flag and line.startswith('%') and update_probability:
                current_probability = float(line.strip().split("=")[1])
                current_coverage = float(line.strip().split("=")[0].split("/")[1])

                rtype = "pos"
                if updating_negative:
                    rtype = "neg"

                rule_stats.append({
                    "rtype": rtype,
                    "purity": current_probability if is_rule_bounded(current_rule) else 0.0,
                    "coverage": current_coverage,
                    "length": current_rule.split(":-")[1].replace(' !', '').count('),'),
                    "number_of_predicates": len(
                        set([x.split('(')[0] for x in current_rule.split(":-")[1].replace(', !.', '').split('),')])),
                    "relation": relation_name
                })

                for p in [x.split('(')[0] for x in current_rule.split(":-")[1].replace(', !.', '').split('),')]:
                    all_used_predicates.add(p)

                if updating_negative:
                    current_probability = 1.0 - current_probability
                    updating_negative = False

                if remove_cut:
                    current_rule = current_rule.replace(', !', '').replace(',!', '')

                rules.append((current_rule, current_probability if is_rule_bounded(current_rule) else 0.0))

                current_rule = ''
                current_probability = ''
                current_coverage = ''
                update_probability = False
            elif reading_flag and line.startswith('Equivalent prolog program with probability distributions:'):
                reading_flag = False

        summary_file.close()

    if remove_cut:
        rules = [x for x in rules if x[1] != 0]

    rules.append(("targetpred(A,B,[pos]).", 0.0))



    return rules, rule_stats, all_used_predicates

"""
def corrupt(ground_truth, entities, relation_name):
    corruptions = set()

    for triple in itertools.product([entities, entities]):
        if "{}({},{})".format(relation_name.lower(), triple[0], triple[1]) not in ground_truth:
            corruptions.add("{}({},{})".format(relation_name.lower(), triple[0],triple[1]))

    return corruptions
"""


def get_triplet_corruptions(test_triplet, ground_truth, entities, relation_name):
    """
    computes the subject and object corruptions of a test triplet
    :param test_triplet: triplet as a string e.g. 'relation(entity1,entity2)'
    :param ground_truth: a set of available facts
    :param entities: a set of all entities
    :param relation_name:
    :return: a tuple (list of subject corruptions, list of object corruptions)
    """
    pred, arguments = test_triplet.replace(")", "").split("(")
    subject_e, object_e = arguments.split(",")

    object_corruptions = set(["{}({},{})".format(relation_name, subject_e, x) for x in entities])
    object_corruptions = object_corruptions.difference(ground_truth)
    subject_corruptions = set(["{}({},{})".format(relation_name, x, object_e) for x in entities])
    subject_corruptions = subject_corruptions.difference(ground_truth)

    return subject_corruptions, object_corruptions


def calculate_confidence(background, rules, to_evaluate, relation_name, wdir, filename):
    """
    Calculates the confidence of a triplet being true
    :param background: available background knowledge as a set of facts
    :param rules: a list of tuples (rule, confidence)
    :param to_evaluate:facts to evaluate, given as a set of tuples (argument1, argument2) and no relation name
    :param relation_name: relation name
    :param wdir: working directory
    :param filename: filename to save intermediate results
    :return: a dict  triplet -> confidence
    """
    prolog_file = open('{}/prolog_test_{}.pl'.format(wdir, relation_name), 'w')

    prolog_file.write(":- use_module(library(lists)).\n\n")

    for t in background:
        prolog_file.write(t + ".\n")

    prolog_file.write("\n")

    for rt in rules:
        rule, confidence = rt[0], rt[1]
        rule = rule.replace("[pos]", str(confidence))

        prolog_file.write(rule + "\n")

    prolog_file.write("\n")

    for t in to_evaluate:
        query = [":- findall(X,targetpred({},{},X),List)".format(t[0], t[1]),
                 "max_list(List,Conf)",
                 "open('{}/{}',append,Stream)".format(wdir, filename),
                 "write(Stream,'{}({},{}):')".format(relation_name, t[0], t[1]),
                 "write(Stream,Conf)",
                 "nl(Stream)",
                 "close(Stream)"]
        query = ",".join(query) + "."
        prolog_file.write(query + "\n")

    prolog_file.close()

    cmd = "yap -L prolog_test_{}.pl".format(relation_name)
    proc = subprocess.Popen(cmd, shell=True, cwd=wdir)
    proc.wait()

    test_confidence = {}

    for line in open("{}/{}".format(wdir, filename)).readlines():
        if len(line) > 3:
            tripel, confidence = line.strip().split(":")
            test_confidence[tripel] = float(confidence)

    # os.remove("{}/test_confidence_{}.txt".format(wdir, relation_name))

    return test_confidence


def calculate_left_and_right_rank(test_confidence, subject_corruptions_confidence, object_corruptions_confidence):
    """
    Calculates the left and the right rank
    :param test_confidence: confidence of the uncorrupted triplet (float)
    :param subject_corruptions_confidence: a dict with subject corruption confidences triplet -> confidence (string -> float)
    :param object_corruptions_confidence: same as subject corruptions
    :return: (int, int) being (left rank, right rank)
    """
    # left rank
    # How many subject corruptions have higher confidence than the test triplet?
    left_rank = 1 + 0.5 * sum([test_confidence < subject_corruptions_confidence[x] for x in subject_corruptions_confidence]) + 0.5 * sum([test_confidence <= subject_corruptions_confidence[x] for x in subject_corruptions_confidence])

    # right rank
    # How many object corruptions have higher confidence than the test triplet?
    right_rank = 1 + 0.5 * sum([test_confidence < object_corruptions_confidence[x] for x in object_corruptions_confidence]) + 0.5 * sum([test_confidence <= object_corruptions_confidence[x] for x in object_corruptions_confidence])

    return left_rank, right_rank


def calculate_hits_at_k(ranks_list, k):
    """
    Calculates the Hits@k metrics
    :param ranks_list: ranks of test triplets - list of tuples (left rank, right rank)
    :param k: k parameter in evaluation
    :return: formula from the ConvE paper
    """
    return sum([(1 if x[0] <= k else 0) + (1 if x[1] <= k else 0) for x in ranks_list]) * 100./(2 * len(ranks_list))


def prepare_data(train, test, valid, target):
    train_facts = set()
    validation_facts = set()
    test_facts = set()
    all_labels = set()

    for pred in train:
        for inst in train[pred]:
            if pred.lower() != target.lower():
                train_facts.add("{}({},{})".format(pred.lower(), inst[0].lower(), inst[1].lower()))
            else:
                all_labels.add("{}({},{})".format(pred.lower(), inst[0].lower(), inst[1].lower()))

    for pred in valid:
        for inst in valid[pred]:
            if pred.lower() != target.lower():
                validation_facts.add("{}({},{})".format(pred.lower(), inst[0].lower(), inst[1].lower()))
            else:
                all_labels.add("{}({},{})".format(pred.lower(), inst[0].lower(), inst[1].lower()))

    for pred in test:
        for inst in test[pred]:
            if pred.lower() == target.lower():
                test_facts.add("{}({},{})".format(pred.lower(), inst[0].lower(), inst[1].lower()))
                all_labels.add("{}({},{})".format(pred.lower(), inst[0].lower(), inst[1].lower()))

    return train_facts, validation_facts, test_facts, all_labels


def extract_all_entities(train, valid, test):
    entities = set()

    for pred in train:
        for inst in train[pred]:
            entities.add(inst[0])
            entities.add(inst[1])

    for pred in valid:
        for inst in train[pred]:
            entities.add(inst[0])
            entities.add(inst[1])

    for pred in test:
        for inst in train[pred]:
            entities.add(inst[0])
            entities.add(inst[1])

    return entities


if __name__ == '__main__':
    for data in RELATIONAL_DATA:

        rule_props = []

        rules_count = []

        print("processing data " + data)
        data_dir = DATA_ROOT + "/" + data
        print("       data folder: " + data_dir)

        train_f = open("{}/train.pkl".format(data_dir), 'rb')
        test_f = open("{}/test.pkl".format(data_dir), 'rb')
        valid_f = open("{}/valid.pkl".format(data_dir), 'rb')

        train = pickle.load(train_f)
        test = pickle.load(test_f)
        valid = pickle.load(valid_f)

        all_entities = extract_all_entities(train, valid, test)
        global_ranks = []

        for target_rel in train:
            print("    relation {} {}".format(target_rel, datetime.datetime.now()))
            trainf, validf, testf, all_labels = prepare_data(train, test, valid, target_rel)

            print("             data prepared! {}".format(datetime.datetime.now()))

            all_training_data = trainf.union(validf)
            print("             training data prepared")

            scratch_dir = SCRATCH_SPACE + "/" + data

            prolog_path = LOCAL_PROLOG + "/" + data

            if not os.path.exists(prolog_path):
                os.mkdir(prolog_path)

            for rel_algo in RELATIONAL_ALGO:
                print("      with {} {}".format(rel_algo, datetime.datetime.now()))
                scratch_dir_inner = scratch_dir + "/" + rel_algo

                scratch_dir_inner += "/" + target_rel
                if not os.path.exists(scratch_dir_inner):
                    print("               didn't found {}".format(scratch_dir_inner))
                    continue

                scratch_dir_inner += "/tilde"
                # print("              folder " + scratch_dir_inner + " " + str(datetime.datetime.now()))

                prolog_path_inner = prolog_path + "/" + rel_algo
                if not os.path.exists(prolog_path_inner):
                    os.mkdir(prolog_path_inner)

                prolog_path_inner = prolog_path_inner + "/" + target_rel
                if not os.path.exists(prolog_path_inner):
                    os.mkdir(prolog_path_inner)
                print("                  prolog path " + prolog_path_inner)

                ## LOAD THE LOGIC PROGRAM
                lp_file = scratch_dir_inner
                program_file_name = "train.uLtestset"
                if TILDE_LEARNING_WITH_INDUCE:
                    program_file_name = "train.out"
                lp_program, rule_stats, all_predicates_in_rel = get_program(lp_file + "/" + program_file_name, target_rel, with_labels=TRAINED_WITH_LABELS)
                # print("              read the TILDE program " + str(datetime.datetime.now()))

                rules_count.append({"relation": target_rel, "rule_count": len(lp_program) - 1, "total_num_predicates": len(all_predicates_in_rel)})

                rule_props = rule_props + rule_stats

                scratch_dir_inner = scratch_dir_inner + "/rankings_" + str(RANKINGS_FILE_APPENDIX) #removeunbouded"

                if not os.path.exists(scratch_dir_inner) and RESULT_COLLECTION:
                    continue
                elif not os.path.exists(scratch_dir_inner):
                    fold_learn, fold_rank = os.path.split(scratch_dir)
                    fold_rel = os.path.split(fold_learn)[0]

                    if not os.path.exists(fold_rel):
                        os.mkdir(fold_rel)

                    if not os.path.exists(fold_learn):
                        os.mkdir(fold_learn)

                    os.mkdir(scratch_dir_inner)
                    # print("                    created dir " + scratch_dir_inner)
                else:
                    pass

                if os.path.exists("{}/{}.ranks".format(scratch_dir_inner, target_rel)):
                    # READ RANKS FROM FILE IN SAVED FROM PREVIOUS RUNS
                    print("found {}".format("{}/{}.ranks".format(scratch_dir_inner, target_rel)))
                    for line in open("{}/{}.ranks".format(scratch_dir_inner, target_rel)).readlines():
                        tmp = line.strip().split(":")[1]
                        tmp = [float(x) for x in tmp.split(",")]
                        global_ranks.append((tmp[0], tmp[1]))
                    print("             global ranks: " + str(len(global_ranks)))
                else:
                    # DO THE EVALUATION
                    print("didn't found {}, running new".format("{}/{}.ranks".format(scratch_dir_inner, target_rel)))
                    tmp_ranks_file = open("{}/{}.ranks".format(scratch_dir_inner, target_rel), 'w')

                    all_corruptions_confidence = {}

                    for tt in testf:
                        ## CORRUPTION
                        # print("                     getting corruptions! {}".format(datetime.datetime.now()))
                        target_subject_corruptions, target_object_corruptions = get_triplet_corruptions(tt, all_labels,
                                                                                                        all_entities,
                                                                                                        target_rel)
                        # print("                     corrupted! {}".format(datetime.datetime.now()))

                        tsc_to_evaluate = [x for x in target_subject_corruptions if x not in all_corruptions_confidence]
                        toc_to_evaluate = [x for x in target_object_corruptions if x not in all_corruptions_confidence]

                        ## EVALUATIONS
                        if len(tsc_to_evaluate):
                            formatted_to_evaluate = [tuple(x.replace(")", "").split("(")[1].split(",")) for x in
                                                     tsc_to_evaluate]
                            tmp_confidences = calculate_confidence(all_training_data, lp_program, formatted_to_evaluate,
                                                                   target_rel, prolog_path_inner,
                                                                   "corruption_subject_confidence_{}.txt".format(
                                                                       target_rel))
                            all_corruptions_confidence.update(tmp_confidences.copy())

                        if len(toc_to_evaluate):
                            formatted_to_evaluate = [tuple(x.replace(")", "").split("(")[1].split(",")) for x in
                                                     toc_to_evaluate]
                            tmp_confidences = calculate_confidence(all_training_data, lp_program, formatted_to_evaluate,
                                                                   target_rel, prolog_path_inner,
                                                                   "corruption_object_confidence_{}.txt".format(target_rel))
                            all_corruptions_confidence.update(tmp_confidences.copy())

                        # print("                      corrupted confidences calculated! {}".format(datetime.datetime.now()))

                        test_args = tuple(tt.replace(")", "").split("(")[1].split(","))
                        test_confidence = calculate_confidence(all_training_data, lp_program, [test_args], target_rel,
                                                               prolog_path_inner, "confidence_{}_{}.txt".format(target_rel, tt))
                        tmp_subject_corrupts = dict([(x, all_corruptions_confidence[x]) for x in target_subject_corruptions])
                        tmp_object_corrupts = dict([(x, all_corruptions_confidence[x]) for x in target_object_corruptions])

                        ## COMPUTE RANKS
                        left_rank, right_rank = calculate_left_and_right_rank(test_confidence[tt], tmp_subject_corrupts, tmp_object_corrupts)

                        global_ranks.append((left_rank, right_rank))

                        tmp_ranks_file.write("{}:{},{}\n".format(tt, left_rank, right_rank))

                    tmp_ranks_file.close()

        print(global_ranks)
        ranks_file = open("{}_{}.ranks".format(data, "TILDE"), 'w')

        for k in [1, 5, 10]:
            hits = calculate_hits_at_k(global_ranks, k)
            print(" ---- hits at {}: {}".format(k, hits))
            ranks_file.write(" ---- hits at {}: {}\n".format(k, hits))

        ranks_file.close()

        rule_file = open("{}_{}.rules".format(data, "TILDE"), 'w')
        rule_file.write(str(rule_props))
        rule_file.close()

        count_file = open("{}_{}.rulecounts".format(data, "TILDE"), 'w')
        count_file.write(str(rules_count))
        count_file.close()










