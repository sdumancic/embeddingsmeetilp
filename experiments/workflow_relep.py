import os
from learners.PropositionalPrediction import DecisionTree, SVM, kNN, LogisticReg
from learners.LearnerSettings import LearnerSettings
from sklearn.model_selection import RepeatedStratifiedKFold
import numpy as np
import altair as alt
import pickle
import subprocess


CLASSIFICATION_ROOT = os.path.abspath(os.path.dirname(__file__)) + "/../data/classification"
EP_DATA_ROOT = CLASSIFICATION_ROOT + "/5_ep"
FOLDS_DATA_ROOT = CLASSIFICATION_ROOT + "/3_pre-embeddings"

CLASSIFICATION_DATA = ['mutagenesis']
EMBEDDING_ALGORITHMS = ['LR'] #['kNN', 'DecisionTree', 'SVM', 'LR']

DIMENSION_TUPLES = [
                    #(10, 10, 10, 1, 50, 'wr'), (10, 10, 10, 1, 50, 'nr'),
                    #(20, 20, 50, 1, 50, 'wr'), (20, 20, 50, 1, 50, 'nr'),
                    (0.1, 1000, True, 20, 20, 200, 1, 50, 'wr'), (0.1, 1000, True, 20, 20, 200, 1, 50, 'nr'),
                    (0.1, 1000, False, 20, 20, 200, 1, 50, 'wr'), (0.1, 1000, False, 20, 20, 200, 1, 50, 'nr'),
                    (0.1, 10000, True, 20, 20, 200, 1, 50, 'wr'), (0.1, 10000, True, 20, 20, 200, 1, 50, 'nr'),
                    (0.1, 10000, False, 20, 20, 200, 1, 50, 'wr'), (0.1, 10000, False, 20, 20, 200, 1, 50, 'nr'),
                    (0.3, 1000, True, 20, 20, 200, 1, 50, 'wr'), (0.3, 1000, True, 20, 20, 200, 1, 50, 'nr'),
                    (0.3, 1000, False, 20, 20, 200, 1, 50, 'wr'), (0.3, 1000, False, 20, 20, 200, 1, 50, 'nr'),
                    (0.3, 10000, True, 20, 20, 200, 1, 50, 'wr'), (0.3, 10000, True, 20, 20, 200, 1, 50, 'nr'),
                    (0.3, 10000, False, 20, 20, 200, 1, 50, 'wr'), (0.3, 10000, False, 20, 20, 200, 1, 50, 'nr'),
                    (0.5, 1000, True, 20, 20, 200, 1, 50, 'wr'), (0.5, 1000, True, 20, 20, 200, 1, 50, 'nr'),
                    (0.5, 1000, False, 20, 20, 200, 1, 50, 'wr'), (0.5, 1000, False, 20, 20, 200, 1, 50, 'nr'),
                    (0.5, 10000, True, 20, 20, 200, 1, 50, 'wr'), (0.5, 10000, True, 20, 20, 200, 1, 50, 'nr'),
                    (0.5, 10000, False, 20, 20, 200, 1, 50, 'wr'), (0.5, 10000, False, 20, 20, 200, 1, 50, 'nr'),
                    (0.7, 1000, True, 20, 20, 200, 1, 50, 'wr'), (0.7, 1000, True, 20, 20, 200, 1, 50, 'nr'),
                    (0.7, 1000, False, 20, 20, 200, 1, 50, 'wr'), (0.7, 1000, False, 20, 20, 200, 1, 50, 'nr'),
                    (0.7, 10000, True, 20, 20, 200, 1, 50, 'wr'), (0.7, 10000, True, 20, 20, 200, 1, 50, 'nr'),
                    (0.7, 10000, False, 20, 20, 200, 1, 50, 'wr'), (0.7, 10000, False, 20, 20, 200, 1, 50, 'nr'),
                    #(20, 20, 50, 10, 50, 'wr'), (20, 20, 50, 10, 50, 'nr'),
                    ]  # (dimension1, dimension2, dimension3, margin, iteration, wr/nr)
SETUP = ['transductive']

WORKING_DIR = os.path.dirname(__file__) + "/../results"

if not os.path.exists(WORKING_DIR + "/scratch"):
    os.mkdir(WORKING_DIR + "/scratch")

WORKING_DIR = WORKING_DIR + "/scratch"

N_FOLDS = 10
N_REPEATS = 10

GENERATE_NEW = False


def algo_prop_factory(algo, params):
    if algo == 'kNN':
        return kNN(params)
    elif algo == 'DecisionTree':
        return DecisionTree(params)
    elif algo == 'LR':
        return LogisticReg(params)
    else:
        return SVM(params)


if __name__ == '__main__':
    results = []
    parameter = LearnerSettings()
    parameter.add_parameter('class_evaluation', 'auc')

    for data in CLASSIFICATION_DATA:
        print("Processing {} dataset...".format(data))
        intermediate_results = {}
        source = EP_DATA_ROOT + "/" + data

        labels_file = [x for x in os.listdir(source) if x.endswith(".labels") and not x.startswith("training") and not x.startswith("test")][0]
        labels = []
        element_order = []

        for line in open(source + "/" + labels_file).readlines():
            if len(line) < 3:
                continue
            elem, lab = line.strip().split(",")
            element_order.append(elem.lower())
            labels.append(lab.lower())

        element_order = np.array(element_order)
        labels = np.array(labels)

        if len([x for x in os.listdir(source) if x.startswith("training") and x.endswith(".labels")]) < 1:
            print("... preparing splits")

            if not GENERATE_NEW:
                print("... copying from folds folder")
                counter = 1
                data_name = data if data != 'webkb2' else 'webkb'
                to_look = [x for x in os.listdir(FOLDS_DATA_ROOT + "/" + data_name)]
                print(to_look)

                for ind, f in enumerate(to_look):
                    folder = FOLDS_DATA_ROOT + "/" + data + "/" + f
                    out_file_train = open("{}/training.{}.labels".format(source, ind), 'w')
                    for line in open("{}/{}/{}/train.labels".format(FOLDS_DATA_ROOT, data_name, f)):
                        out_file_train.write(line.lower())

                    out_file_test = open("{}/test.{}.labels".format(source, ind), 'w')
                    for line in open("{}/{}/{}/test.labels".format(FOLDS_DATA_ROOT, data_name, f)):
                        out_file_test.write(line.lower())

                    out_file_test.close()
                    out_file_train.close()
            else:
                print("... generating new split: {} {}".format(N_FOLDS, N_REPEATS))
                X = np.zeros(len(labels))
                skf = RepeatedStratifiedKFold(n_splits=N_FOLDS, n_repeats=N_REPEATS)
                counter = 1
                for train_index, test_index in skf.split(X, labels):
                    train_element, train_labs = element_order[train_index], labels[train_index]
                    test_element, test_labs = element_order[test_index], labels[test_index]

                    training_labels = open("{}/training.{}.labels".format(source, counter), 'w')
                    test_labels = open("{}/test.{}.labels".format(source, counter), 'w')

                    for i in range(len(train_element)):
                        training_labels.write("{} {}\n".format(train_element[i], train_labs[i]))

                    for i in range(len(test_element)):
                        test_labels.write("{} {}\n".format(test_element[i], test_labs[i]))

                    training_labels.close()
                    test_labels.close()
                    counter += 1

        for setup in SETUP:
            print("... {} setup".format(setup))
            emb_files = list(os.listdir(source + "/" + setup))

            for emf in DIMENSION_TUPLES:
                print("   ... {} embedding ".format(emf))
                feat_acc = emf[0]
                max_feat = emf[1]
                prune = emf[2]
                dimension1 = emf[3]
                dimension2 = emf[4]
                dimension3 = emf[5]
                margin = emf[6]
                iteration = emf[7]
                rel = emf[8]
                for algo in EMBEDDING_ALGORITHMS:
                    print("      ... {} algorithm".format(algo))
                    for ind, k in enumerate([x for x in os.listdir(source) if x.startswith('training') and x.endswith('labels')]):
                        print("         ... {} fold".format(ind))
                        training_data = "{}/{}/relep_acc{}_mf{}_prune{}_embedd_{}_{}_{}_margin{}_it{}_{}.entity.emb".format(source, setup, feat_acc, max_feat, prune, dimension1, dimension2, dimension3, margin, iteration, rel)
                        training_labels = "{}/{}".format(source, k)
                        test_labels = "{}/{}".format(source, k.replace("training", "test"))

                        palgo = algo_prop_factory(algo, parameter)
                        accuracy = palgo.learn_and_evaluate(training_data, training_labels, test_labels_file=test_labels)

                        #algo_tuple = tuple([algo, emf, setup, dimension1, dimension2, dimension3, margin, rel])
                        #if algo_tuple not in intermediate_results:
                        #    intermediate_results[algo_tuple] = []
                        #intermediate_results[algo_tuple].append(accuracy)

                        results.append({'algo': algo,
                                        'setup': setup,
                                        'dimension1': dimension1,
                                        'dimension2': dimension2,
                                        'dimension3': dimension3,
                                        'prune': prune,
                                        'feat_acc': feat_acc,
                                        'max_feat': max_feat,
                                        'dimension': "no relational" if rel == 'nr' else "{}-{}-{}-{}-{}-{}-{}-{}-{}".format(feat_acc, max_feat, prune, dimension1, dimension2, dimension3, margin, iteration, rel),
                                        'margin': margin,
                                        'iteration': iteration,
                                        'accuracy': accuracy,
                                        'dataset': data,
                                        'relfeat': True if rel == 'wr' else False

                        })

        #for algo_tuple in intermediate_results:
        #    alg = algo_tuple[0]
        #    eemf = algo_tuple[1]
        #    eset = algo_tuple[2]
        #    dim = algo_tuple[3]
        #    margin = algo_tuple[4]
        #    results.append({'algo': alg,
        #                    'setup': eset,
        #                    'embedding': eemf,
        #                    'dimension': dim,
        #                    'margin': margin,
        #                    'mean': np.array(intermediate_results[algo_tuple]).mean(),
        #                    'std': np.array(intermediate_results[algo_tuple]).std(),
        #                    'dataset': data
        #                    })

    data_res = alt.Data(values=results)

    chart = alt.Chart(data_res).mark_bar().encode(
        x='dimension:N',
        y='mean:Q',
        color='embedding:N',
        column='algo:N',
        row='margin:N'
    )

    chart.savechart(
        os.path.dirname(__file__) + "/../results/ep-results-{}.json".format("_".join(CLASSIFICATION_DATA)))

    base_chart = alt.Chart().mark_point(filled=True).encode(
        x='mean:Q',
        y='dimension:N',
        color=alt.value('blue')
    )

    std_chart = alt.Chart().mark_rule().encode(
        x='upper:Q',
        x2='lower:Q',
        y='dimension:N'
    )

    pf = open(
        os.path.dirname(__file__) + "/../results/ep-results-{}.pkl".format("_".join(CLASSIFICATION_DATA)), 'wb')
    pickle.dump(results, pf)
    pf.close()
    print("DONE!!")

    layered_plot = alt.layer(base_chart, std_chart).properties(
        data=data_res
    ).transform_aggregate(
        mean='mean(accuracy)',
        stdev='stdev(accuracy)',
        groupby=["dimension", "algo"]
    ).transform_calculate(
        lower="datum.mean - datum.stdev",
        upper="datum.mean + datum.stdev"
    )
    layered_plot.savechart(
        os.path.dirname(__file__) + "/../results/ep-results-{}-layered.json".format("_".join(CLASSIFICATION_DATA)))

    individual_plots = [layered_plot.transform_filter(
        alt.FieldEqualPredicate(field='algo', equal=x)
    ).properties(title=x) for x in EMBEDDING_ALGORITHMS]

    concat_plot = alt.vconcat(*individual_plots)

    concat_plot.savechart(
        os.path.dirname(__file__) + "/../results/ep-results-{}-layered-concat.json".format("_".join(CLASSIFICATION_DATA)))
