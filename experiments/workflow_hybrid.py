import os
from learners.LearnerSettings import LearnerSettings
from learners.hybrid.HybridTILDE import HybridTILDE
import altair as alt
import numpy as np


def algo_factory(algo, params, embedding_method, work_dir):
    if algo == 'HybridTILDE':
        return HybridTILDE(params, work_dir, embedding_method)
    else:
        return HybridTILDE(params, work_dir, embedding_method)


CLASSIFICATION_ROOT = os.path.dirname(__file__) + "/../data/classification"
RELATIONAL_DATA_ROOT = CLASSIFICATION_ROOT + "/2_folds"
RELATIONAL_DEFINITIONS_ROOT = CLASSIFICATION_ROOT + "/1_raw"
EMBEDDINGS_DATA_ROOT = CLASSIFICATION_ROOT + "/4_embeddings"

CLASSIFICATION_DATA = ['genes', 'hepatitis', 'mutagenesis', 'terrorists', 'webkb']
ALGORITHMS = ['HybridTILDE']

WORKING_DIR = os.path.dirname(__file__) + "/../results"

if not os.path.exists(WORKING_DIR + "/scratch"):
    os.mkdir(WORKING_DIR + "/scratch")

WORKING_DIR = WORKING_DIR + "/scratch"

EMBEDDING_METHOD = 'transe'

if __name__ == '__main__':
    results = []
    parameter = LearnerSettings()

    for data in CLASSIFICATION_DATA:
        intermediate_results = {}
        folds = os.listdir(EMBEDDINGS_DATA_ROOT + "/" + data)

        for fold in folds:
            for rel_algo in ALGORITHMS:
                training_file = RELATIONAL_DATA_ROOT + "/" + data + "/" + fold + "/training.db"
                training_labels = RELATIONAL_DATA_ROOT + "/" + data + "/" + fold + "/training.labels"

                test_file = RELATIONAL_DATA_ROOT + "/" + data + "/" + fold + "/test.db"
                test_labels = RELATIONAL_DATA_ROOT + "/" + data + "/" + fold + "/test.labels"

                definition_file = RELATIONAL_DATA_ROOT + "/" + data + "/predicate.defs"

                entity_embedding_file = EMBEDDINGS_DATA_ROOT + "/" + data + "/" + fold + "/train.emb"
                relation_embedding_file = EMBEDDINGS_DATA_ROOT + "/" + data + "/" + fold + "/train.emb"

                # prepare scratch folder
                current_working_dir = WORKING_DIR + "/" + rel_algo
                if not os.path.exists(current_working_dir):
                    os.mkdir(current_working_dir)

                current_working_dir = current_working_dir + "/" + data
                if not os.path.exists(current_working_dir):
                    os.mkdir(current_working_dir)

                current_working_dir = current_working_dir + "/" + fold
                if not os.path.exists(current_working_dir):
                    os.mkdir(current_working_dir)

                rel_pred = algo_factory(rel_algo, parameter, EMBEDDING_METHOD, current_working_dir)

                rel_pred.read_entity_embeddings(entity_embedding_file)
                rel_pred.read_relation_embeddings(relation_embedding_file)

                accuracy = rel_pred.learn_and_evaluate([training_file], [training_labels], test_data_file=test_file,
                                                       test_labels_file=test_labels, definitions=definition_file)

                if rel_algo not in intermediate_results:
                    intermediate_results[rel_algo] = []

                intermediate_results[rel_algo].append(accuracy)

        for alg in intermediate_results:
            results.append({'algo': alg,
                            'level': 'hybrid',
                            'mean': np.array(intermediate_results[alg]).mean(),
                            'std': np.array(intermediate_results[alg]).std(),
                            'dataset': data
                            })

        data = alt.Data(values=results)

        chart = alt.Chart(data).mark_bar().encode(
            x='algo:N',
            y='mean:Q',
            color='level:N',
            column='dataset:N'
        )

        chart.savechart(os.path.dirname(__file__) + "/../results/classification.json")