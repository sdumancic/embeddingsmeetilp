import os
from learners.PropositionalPrediction import DecisionTree, SVM, kNN
from learners.RelationalLearner import TILDE, Aleph, AMIE, Metagol, KFoil, RelKNN
from learners.LearnerSettings import LearnerSettings
import numpy as np
import altair as alt
import pickle

CLASSIFICATION_ROOT = os.path.abspath(os.path.dirname(__file__)) + "/../data/classification"
RELATIONAL_DATA_ROOT = CLASSIFICATION_ROOT + "/2_folds"
RELATIONAL_DEFINITIONS_ROOT = CLASSIFICATION_ROOT + "/1_raw"
EMBEDDINGS_DATA_ROOT = CLASSIFICATION_ROOT + "/4_embeddings"

SIMILARITIES_DATA_ROOT = CLASSIFICATION_ROOT + "/similarities"

CLASSIFICATION_DATA = ['mutagenesis'] #['mutagenesis', 'terrorists', 'webkb', 'hepatitis']
RELATIONAL_ALGORITHMS = ['TILDE']  # 'Aleph' 'AMIE'
EMBEDDING_ALGORITHMS = ['kNN', 'DecisionTree', 'SVM']

EMBEDDING_METHODS = ['transE', 'DistMult', 'complEXsum', 'complEXavg', 'complEXconcat'] # 'relational'
EMBEDDING_DIMENSION = [10, 20, 30, 50, 80, 100]
EMBEDDING_EPOCHS = [20, 40, 60, 80, 100]

WEIGHT_STEP = 5


TARGET_DOMAIN_NAME = {
    'mutagenesis': 'mol',
    'terrorists': 'event',
    'webkb': 'page',
    'hepatitis': 'mdom',
    'webkb_bck': 'page',
    'uwcse': 'key',
    'cora_reified': 'key',
    'yeast': 'gene',
    'canc': 'drug',
    'canc2': 'drug'
}

LOOKAHEADS = {
    'cora_reified': ['kmap(X,Y,Z),[Y,Z]', 'sametitle(X,Y),[X,X]', 'sameauthor(X,Y),[X,Y]', 'samevenue(X,Y),[X,Y]'],
    'mutagenesis': ['mol2atm(X,Y),[Y]'],
    'uwcse': ['kmap(X,Y,Z),[Y,Z]'],
    'yeast': ['path(X,Y),[X,Y]'],
    'canc2': ['sbond_7_drug0(X,Y), [X]', 'sbond_1_drug0(X,Y), [X]', 'sbond_2_drug0(X,Y), [X]', 'sbond_3_drug0(X,Y), [X]',
              'sbond_1_atomid1(X,Y), [Y]', 'sbond_2_atomid1(X,Y), [Y]', 'sbond_3_atomid1(X,Y), [Y]', 'sbond_7_atomid1(X,Y), [Y]',
              'sbond_1_atomid2(X,Y), [Y]', 'sbond_2_atomid2(X,Y), [Y]', 'sbond_3_atomid2(X,Y), [Y]', 'sbond_7_atomid2(X,Y), [Y]'],
    'canc3': ['sbond_1(X,Y,Z),[Y,Z]', 'sbond_2(X,Y,Z),[Y,Z]', 'sbond_3(X,Y,Z),[Y,Z]', 'sbond_7(X,Y,Z),[Y,Z]', 'mol2atm(X,Y), [Y]']
}


PREDICATE_BIAS = {
    'mutagenesis': {
        'bond_bondtype1': ['(+aatom,-aatom)', '(-aatom,+aatom)'],
        'bond_bondtype2': ['(+aatom,-aatom)', '(-aatom,+aatom)'],
        'bond_bondtype3': ['(+aatom,-aatom)', '(-aatom,+aatom)'],
        'bond_bondtype4': ['(+aatom,-aatom)', '(-aatom,+aatom)'],
        'bond_bondtype5': ['(+aatom,-aatom)', '(-aatom,+aatom)'],
        'bond_bondtype7': ['(+aatom,-aatom)', '(-aatom,+aatom)'],
        'element': ['(+aatom,#typee)'],
        'atype': ['(+aatom,#typea)'],
        'logp': ['(+mol,#typlelog)'],
        'inda': ['(+mol,#typeia)'],
        'mol2atm': ['(+mol,-aatom)', '(-mol,+aatom)'],
        'ind1': ['(+mol,#typeio)'],
        'lumo': ['(+mol,#typel)'],
        'charge': ['(+aatom,#typec)']
    },
    'webkb_bck': {
        'hasanchor': ['(+link,#word)'],
        'hasneighborhood': ['(+link,#word)'],
        'hasword': ['(+page,#word)', '(+page,-word)'],
        'instructorsof': ['(+page,-page)', '(-page,+page)', '(+page,+page)'],
        'linkprop': ['(+link,#propertylink)'],
        'linkto': ['(+link,+page,-page)', '(+link,-page,-page)', '(-link,+page,+page)', '(-link,-page,+page)'],
        'membersofproject': ['(+page,-page)', '(-page,+page)', '(+page,+page)']
    },
    'bongard4': {
        'circle': ['(+pic,-obj)', '(-pic,+obj)'],
        'config': ['(+pic,-obj,#dir)', '(+pic,+obj,#dir)'],
        'eastof': ['(+pic,+obj,-obj)', '(+pic,-obj,+obj)'],
        'inside': ['(+pic,+obj,-obj)', '(+pic,-obj,+obj)'],
        'northof': ['(+pic,+obj,-obj)', '(+pic,-obj,+obj)'],
        'square': ['(+pic,-obj)', '(-pic,+obj)'],
        'triangle': ['(+pic,-obj)', '(-pic,+obj)']
    },
    'uwcse': {
        'courselevel': ['(+course,#level)'],
        'phase': ['(+human,#phase)'],
        'position': ['(+human,#faculty)'],
        'professor': ['(+human)'],
        'projectmember': ['(+project,-human)', '(-project,+human)'],
        'publication': ['(+ref,-human)', '(-ref,+human)'],
        'student': ['(+human)'],
        'ta': ['(+course,-human)', '(-course,+human)'],
        'taughtby': ['(+course,-human)', '(-course,+human)'],
        'tempadvisedby': ['(+human,-human)', '(-human,+human)'],
        'yearsinprogram': ['(+human,#years)'],
        'kmap': ['(+key,-human,-human)'],
        'role': ['(+human,#role)']
    },
    'canc': {
        'atm_as': ['(+drug,+atomid,#atomtype,#charge)', '(+drug,-atomid,#atomtype,#charge)', '(-drug,+atomid,#atomtype,#charge)', '(-drug,+atomid,-atomtype,#charge)', '(-drug,+atomid,#atomtype,-charge)'],
        'atm_ba': ['(+drug,+atomid,#atomtype,#charge)', '(+drug,-atomid,#atomtype,#charge)', '(-drug,+atomid,#atomtype,#charge)', '(-drug,+atomid,-atomtype,#charge)', '(-drug,+atomid,#atomtype,-charge)'],
        'atm_br': ['(+drug,+atomid,#atomtype,#charge)', '(+drug,-atomid,#atomtype,#charge)', '(-drug,+atomid,#atomtype,#charge)', '(-drug,+atomid,-atomtype,#charge)', '(-drug,+atomid,#atomtype,-charge)'],
        'atm_c': ['(+drug,+atomid,#atomtype,#charge)', '(+drug,-atomid,#atomtype,#charge)', '(-drug,+atomid,#atomtype,#charge)', '(-drug,+atomid,-atomtype,#charge)', '(-drug,+atomid,#atomtype,-charge)'],
        'atm_ca': ['(+drug,+atomid,#atomtype,#charge)', '(+drug,-atomid,#atomtype,#charge)', '(-drug,+atomid,#atomtype,#charge)', '(-drug,+atomid,-atomtype,#charge)', '(-drug,+atomid,#atomtype,-charge)'],
        'atm_cl': ['(+drug,+atomid,#atomtype,#charge)', '(+drug,-atomid,#atomtype,#charge)', '(-drug,+atomid,#atomtype,#charge)', '(-drug,+atomid,-atomtype,#charge)', '(-drug,+atomid,#atomtype,-charge)'],
        'atm_cu': ['(+drug,+atomid,#atomtype,#charge)', '(+drug,-atomid,#atomtype,#charge)', '(-drug,+atomid,#atomtype,#charge)', '(-drug,+atomid,-atomtype,#charge)', '(-drug,+atomid,#atomtype,-charge)'],
        'atm_f': ['(+drug,+atomid,#atomtype,#charge)', '(+drug,-atomid,#atomtype,#charge)', '(-drug,+atomid,#atomtype,#charge)', '(-drug,+atomid,-atomtype,#charge)', '(-drug,+atomid,#atomtype,-charge)'],
        'atm_h': ['(+drug,+atomid,#atomtype,#charge)', '(+drug,-atomid,#atomtype,#charge)', '(-drug,+atomid,#atomtype,#charge)', '(-drug,+atomid,-atomtype,#charge)', '(-drug,+atomid,#atomtype,-charge)'],
        'atm_hg': ['(+drug,+atomid,#atomtype,#charge)', '(+drug,-atomid,#atomtype,#charge)', '(-drug,+atomid,#atomtype,#charge)', '(-drug,+atomid,-atomtype,#charge)', '(-drug,+atomid,#atomtype,-charge)'],
        'atm_i': ['(+drug,+atomid,#atomtype,#charge)', '(+drug,-atomid,#atomtype,#charge)', '(-drug,+atomid,#atomtype,#charge)', '(-drug,+atomid,-atomtype,#charge)', '(-drug,+atomid,#atomtype,-charge)'],
        'atm_k': ['(+drug,+atomid,#atomtype,#charge)', '(+drug,-atomid,#atomtype,#charge)', '(-drug,+atomid,#atomtype,#charge)', '(-drug,+atomid,-atomtype,#charge)', '(-drug,+atomid,#atomtype,-charge)'],
        'atm_mn': ['(+drug,+atomid,#atomtype,#charge)', '(+drug,-atomid,#atomtype,#charge)', '(-drug,+atomid,#atomtype,#charge)', '(-drug,+atomid,-atomtype,#charge)', '(-drug,+atomid,#atomtype,-charge)'],
        'atm_n': ['(+drug,+atomid,#atomtype,#charge)', '(+drug,-atomid,#atomtype,#charge)', '(-drug,+atomid,#atomtype,#charge)', '(-drug,+atomid,-atomtype,#charge)', '(-drug,+atomid,#atomtype,-charge)'],
        'atm_na': ['(+drug,+atomid,#atomtype,#charge)', '(+drug,-atomid,#atomtype,#charge)', '(-drug,+atomid,#atomtype,#charge)', '(-drug,+atomid,-atomtype,#charge)', '(-drug,+atomid,#atomtype,-charge)'],
        'atm_o': ['(+drug,+atomid,#atomtype,#charge)', '(+drug,-atomid,#atomtype,#charge)', '(-drug,+atomid,#atomtype,#charge)', '(-drug,+atomid,-atomtype,#charge)', '(-drug,+atomid,#atomtype,-charge)'],
        'atm_p': ['(+drug,+atomid,#atomtype,#charge)', '(+drug,-atomid,#atomtype,#charge)', '(-drug,+atomid,#atomtype,#charge)', '(-drug,+atomid,-atomtype,#charge)', '(-drug,+atomid,#atomtype,-charge)'],
        'atm_pb': ['(+drug,+atomid,#atomtype,#charge)', '(+drug,-atomid,#atomtype,#charge)', '(-drug,+atomid,#atomtype,#charge)', '(-drug,+atomid,-atomtype,#charge)', '(-drug,+atomid,#atomtype,-charge)'],
        'atm_s': ['(+drug,+atomid,#atomtype,#charge)', '(+drug,-atomid,#atomtype,#charge)', '(-drug,+atomid,#atomtype,#charge)', '(-drug,+atomid,-atomtype,#charge)', '(-drug,+atomid,#atomtype,-charge)'],
        'atm_se': ['(+drug,+atomid,#atomtype,#charge)', '(+drug,-atomid,#atomtype,#charge)', '(-drug,+atomid,#atomtype,#charge)', '(-drug,+atomid,-atomtype,#charge)', '(-drug,+atomid,#atomtype,-charge)'],
        'atm_sn': ['(+drug,+atomid,#atomtype,#charge)', '(+drug,-atomid,#atomtype,#charge)', '(-drug,+atomid,#atomtype,#charge)', '(-drug,+atomid,-atomtype,#charge)', '(-drug,+atomid,#atomtype,-charge)'],
        'atm_te': ['(+drug,+atomid,#atomtype,#charge)', '(+drug,-atomid,#atomtype,#charge)', '(-drug,+atomid,#atomtype,#charge)', '(-drug,+atomid,-atomtype,#charge)', '(-drug,+atomid,#atomtype,-charge)'],
        'atm_ti': ['(+drug,+atomid,#atomtype,#charge)', '(+drug,-atomid,#atomtype,#charge)', '(-drug,+atomid,#atomtype,#charge)', '(-drug,+atomid,-atomtype,#charge)', '(-drug,+atomid,#atomtype,-charge)'],
        'atm_zn': ['(+drug,+atomid,#atomtype,#charge)', '(+drug,-atomid,#atomtype,#charge)', '(-drug,+atomid,#atomtype,#charge)', '(-drug,+atomid,-atomtype,#charge)', '(-drug,+atomid,#atomtype,-charge)'],
        'sbond_1': ['(+drug,-atomid,-atomid)', '(+drug,+atomid,-atomid)', '(+drug,-atomid,+atomid)'],
        'sbond_2': ['(+drug,-atomid,-atomid)', '(+drug,+atomid,-atomid)', '(+drug,-atomid,+atomid)'],
        'sbond_3': ['(+drug,-atomid,-atomid)', '(+drug,+atomid,-atomid)', '(+drug,-atomid,+atomid)'],
        'sbond_7': ['(+drug,-atomid,-atomid)', '(+drug,+atomid,-atomid)', '(+drug,-atomid,+atomid)'],
    },
    'canc3': {
        'mol2atm' : ['(+drug,-atomid)'],
        'atm_elem': ['(+atomid,#element)', '(+atomid,-element)'],
        'atm_atomtype': ['(+atomid,#atomtype)', '(+atomid,-atomtype)'],
        'atm_charge': ['(+atomid,#charge)', '(+atomid,-charge)'],
        'sbond_1': ['(+drug,-atomid,-atomid)', '(+drug,+atomid,-atomid)', '(+drug,-atomid,+atomid)'],
        'sbond_2': ['(+drug,-atomid,-atomid)', '(+drug,+atomid,-atomid)', '(+drug,-atomid,+atomid)'],
        'sbond_3': ['(+drug,-atomid,-atomid)', '(+drug,+atomid,-atomid)', '(+drug,-atomid,+atomid)'],
        'sbond_7': ['(+drug,-atomid,-atomid)', '(+drug,+atomid,-atomid)', '(+drug,-atomid,+atomid)']
    },
    'canc2': {
        'atm_as' : ['(+drug,-atomid)'],
        'atm_ba' : ['(+drug,-atomid)'],
        'atm_br' : ['(+drug,-atomid)'],
        'atm_c' : ['(+drug,-atomid)'],
        'atm_ca' : ['(+drug,-atomid)'],
        'atm_cl' : ['(+drug,-atomid)'],
        'atm_cu' : ['(+drug,-atomid)'],
        'atm_f' : ['(+drug,-atomid)'],
        'atm_h': ['(+drug,-atomid)'],
        'atm_hg': ['(+drug,-atomid)'],
        'atm_i' : ['(+drug,-atomid)'],
        'atm_k' : ['(+drug,-atomid)'],
        'atm_mn': ['(+drug,-atomid)'],
        'atm_n': ['(+drug,-atomid)'],
        'atm_na': ['(+drug,-atomid)'],
        'atm_o': ['(+drug,-atomid)'],
        'atm_p': ['(+drug,-atomid)'],
        'atm_pb' : ['(+drug,-atomid)'],
        'atm_s': ['(+drug,-atomid)'],
        'atm_se' : ['(+drug,-atomid)'],
        'atm_sn': ['(+drug,-atomid)'],
        'atm_te': ['(+drug,-atomid)'],
        'atm_ti' : ['(+drug,-atomid)'],
        'atm_zn': ['(+drug,-atomid)'],
        'atm_atype': ['(+atomid,#atomtype)', '(+atomid,-atomtype)'],
        'atm_charge': ['(+atomid,#charge)', '(+atomid,-charge)'],
        'sbond_7_drug0' : ['(-sbondid,+drug)'],
        'sbond_7_atomid1': ['(+sbondid,-atomid)', '(-sbondid,+atomid)'],
        'sbond_7_atomid2': ['(+sbondid,-atomid)', '(-sbondid,+atomid)'],
        'sbond_1_drug0': ['(-sbondid,+drug)'],
        'sbond_1_atomid1': ['(+sbondid,-atomid)', '(-sbondid,+atomid)'],
        'sbond_1_atomid2': ['(+sbondid,-atomid)', '(-sbondid,+atomid)'],
        'sbond_2_drug0': ['(-sbondid,+drug)'],
        'sbond_2_atomid1': ['(+sbondid,-atomid)', '(-sbondid,+atomid)'],
        'sbond_2_atomid2': ['(+sbondid,-atomid)', '(-sbondid,+atomid)'],
        'sbond_3_drug0': ['(-sbondid,+drug)'],
        'sbond_3_atomid1': ['(+sbondid,-atomid)', '(-sbondid,+atomid)'],
        'sbond_3_atomid2': ['(+sbondid,-atomid)', '(-sbondid,+atomid)']
    },
    'cora_reified': {
        'author': ['(+bib,-author)', '(-bib,+author)'],
        'title': ['(+bib,-title)', '(-bib,+title)'],
        'venue': ['(+bib,-venue)', '(-bib,+venue)'],
        'haswordauthor': ['(+author,-word)', '(+author,#word)'],
        'haswordtitle': ['(+title,-word)', '(+title,#word)'],
        'haswordvenue': ['(+venue,-word)', '(+venue,#word)'],
        'sameauthor': ['(+author,-author)', '(-author,+author)'],
        'samebib': ['(+bib,-bib)', '(-bib,+bib)'],
        'samevenue': ['(+venue,-venue)', '(-venue,+venue)'],
        'sametitle': ['(+title,-title)', '(-title,+title)'],
        'kmap': ['(+key,-bib,-bib)']
    },
    'hepatitis': {
        'dbil': ['(+indom,#dbiltype)'],
        'che': ['(+indom,#chetype)'],
        'tp': ['(+indom,#tptype)'],
        'got': ['(+indom,#gottype)'],
        'gpt': ['(+indom,#gpttype)'],
        'ztt': ['(+indom,#ztttype)'],
        'dur': ['(+atype,#durtype)'],
        'ttt': ['(+indom,#ttttype)'],
        'b_rel12': ['(+indom,-mdom)', '(-indom,+mdom)'],
        'b_rel13': ['(+atype,-mdom)', '(-atype,+mdom)'],
        'b_rel11': ['(+bdom,-mdom)', '(-bdom,+mdom)'],
        'tbil': ['(+indom,#tbiltype)'],
        'age': ['(+mdom,#agetype)'],
        'activity': ['(+bdom,#acttype)'],
        'fibros': ['(+bdom,#fibtype)'],
        'alb': ['(+indom,#albtype)'],
        'sex': ['(+mdom,#sextype)'],
        'tcho': ['(+indom,#tchotype)'],
    },
    'terrorists': {
        'colocatedevent': ['(+eventdom,-eventdom)', '(-eventdom,+eventdom)'],
        'haseventfeature': ['(+eventdom,#featuredom)', '(+eventdom,-featuredom)'],
        'performedbysameorg': ['(+eventdom,-eventdom)', '(-eventdom,+eventdom)']
    },
    'yeast': {
        'complex': ['(+gene,#complex)'],
        'enzyme': ['(+gene,#enzyme)'],
        'interaction': ['(+gene,-gene,#intertype)', '(-gene,+gene,#intertype)'],
        'location': ['(+gene,#location)'],
        'path': ['(+gene,-gene)', '(-gene,+gene)'],
        'phenotype': ['(+gene,#phenotype)'],
        'protein_class': ['(+gene,#class)'],
        'rcomplex': ['(+gene,#complex)'],
        'renzyme': ['(+gene,#enzyme)'],
        'rphenotype': ['(+gene,#phenotype)'],
        'rprotein_class': ['(+gene,#class)']
    }
}

WORKING_DIR = os.path.dirname(__file__) + "/../results"

if not os.path.exists(WORKING_DIR + "/scratch"):
    os.mkdir(WORKING_DIR + "/scratch")

WORKING_DIR = WORKING_DIR + "/scratch"


COMPARISON_CASES = [
    {'name': 'decision_tree', 'relational': 'tilde', 'propositional': 'DecisionTree'},
    {'name': 'svm', 'relational': 'kfoil', 'propositional': 'SVM'},
    {'name': 'knn', 'relational': 'RelKNN', 'propositional': 'kNN'}
]


if __name__ == '__main__':
    def algo_prop_factory(algo, params):
        if algo == 'kNN':
            return kNN(params)
        elif algo == 'DecisionTree':
            return DecisionTree(params)
        else:
            return SVM(params)


    def algo_rel_factory(algo, params, work_dir, dataset_name=""):
        if algo.lower() == 'tilde':
            return TILDE(params, work_dir)
        elif algo.lower() == 'aleph':
            return Aleph(params, work_dir)
        elif algo.lower() == 'amie':
            return AMIE(params, work_dir)
        elif algo.lower() == 'metagol':
            return Metagol(params, work_dir)
        elif algo.lower() == 'kfoil':
            return KFoil(params, work_dir, dataset_name)
        elif algo.lower() == 'relknn':
            return RelKNN(params, work_dir, SIMILARITIES_DATA_ROOT + "/" + dataset_name, TARGET_DOMAIN_NAME[dataset_name], weights_step=WEIGHT_STEP)
        else:
            return Aleph(params, work_dir)

    for comp_case in COMPARISON_CASES:
        print("--------------- Processing case: {}".format(comp_case['name']))
        results = []
        parameter = LearnerSettings()
        parameter.add_parameter('aleph_evalfn', 'accuracy')
        parameter.add_parameter('aleph_search', 'bf')
        parameter.add_parameter('aleph_searchtime', 180)
        parameter.add_parameter('aleph_samplesize', 3)
        parameter.add_parameter('timeout', 30)  # in MINUTES
        parameter.add_parameter('amie_maxad', [3, 4])
        parameter.add_parameter('amie_minc', [1.0, 0.9, 0.8, 0.7])
        parameter.add_parameter('amie_minpca', [0.1, 0.5])
        parameter.add_parameter("n_neighbors", [3, 5, 7, 9, 11, 13, 15])
        parameter.add_parameter('kfoil_beam_size', [3])
        parameter.add_parameter('kfoil_g', [0.5, 1.0, 0.1])
        parameter.add_parameter('kfoil_s', [0.01, 0.1, 1.0])
        parameter.add_parameter('kfoil_b', [1])
        parameter.add_parameter('kfoil_N', [False])
        parameter.add_parameter('kfoil_M', [False])
        parameter.add_parameter('kfoil_r', [0.1, 1.0, 10.0])
        parameter.add_parameter('kfoil_d', [2, 3, 5])
        #parameter.add_parameter('kfoil_max_literals', [7])
        #parameter.add_parameter('kfoil_max_clauses', [25])
        #parameter.add_parameter('kfoil_c', [0.25])

        for data in CLASSIFICATION_DATA:
            print("Processing {} dataset...".format(data))
            intermediate_results = {}
            folds = os.listdir(RELATIONAL_DATA_ROOT + "/" + data)

            for fold in folds:
                print("  fold {}".format(fold))
                rel_algo = comp_case['relational']
                prop_algo = comp_case['propositional']

                rel_data = data

                if data == 'webkb':
                    rel_data = 'webkb_bck'

                #if data == 'cora_reified':
                #    rel_data = 'cora'

                if data == 'canc2':
                    rel_data = 'canc3'

                print("      relational algorithm {}".format(rel_algo))

                rel_training_file = RELATIONAL_DATA_ROOT + "/" + rel_data + "/" + fold + "/training.db"
                rel_training_labels = RELATIONAL_DATA_ROOT + "/" + rel_data + "/" + fold + "/training.labels"

                rel_test_file = RELATIONAL_DATA_ROOT + "/" + rel_data + "/" + fold + "/test.db"
                rel_test_labels = RELATIONAL_DATA_ROOT + "/" + rel_data + "/" + fold + "/test.labels"

                definition_file = RELATIONAL_DEFINITIONS_ROOT + "/" + rel_data + "/predicate.defs"

                # prepare scratch folder
                current_working_dir = WORKING_DIR + "/" + rel_algo
                if not os.path.exists(current_working_dir):
                    os.mkdir(current_working_dir)

                current_working_dir = current_working_dir + "/" + data
                if not os.path.exists(current_working_dir):
                    os.mkdir(current_working_dir)

                current_working_dir = current_working_dir + "/" + fold
                if not os.path.exists(current_working_dir):
                    os.mkdir(current_working_dir)

                if rel_algo.lower() == 'relknn':
                    parameter.add_parameter('n_folds', 2)

                if rel_algo.lower() == 'kfoil':
                    # FOR KFOIL CURRENT WORKING DIR IS THE APPENDIX TO KFOIL_HOME/DATASET
                    current_working_dir = fold

                rel_pred = algo_rel_factory(rel_algo, parameter, current_working_dir, dataset_name=data)

                if rel_algo.lower() == 'relknn':
                    parameter.add_parameter('n_folds', 5)

                if rel_algo.lower() == 'kfoil':
                    data_to_use = rel_data
                    if 'terrorists' in data_to_use:
                        data_to_use = 'terrorists'
                    if 'webkb' in data_to_use:
                        data_to_use = 'webkb_bck'
                    rel_pred.read_bias(PREDICATE_BIAS[data_to_use])

                if rel_algo.lower() == 'tilde' and rel_data in LOOKAHEADS:
                    rel_pred.read_lookaheads(LOOKAHEADS[rel_data])

                rel_accuracy = rel_pred.learn_and_evaluate([rel_training_file], [rel_training_labels], test_data_file=rel_test_file,
                                                           test_labels_file=rel_test_labels, definitions=definition_file)

                if rel_accuracy == 0.0:
                    print("                          ZERO ACCURACY OF RELATIONAL LEARNER")
                    continue

                for emb_method in EMBEDDING_METHODS:
                    print("      {} embeddings".format(emb_method))

                    for emb_dim in EMBEDDING_DIMENSION:
                        print("        dimension {}".format(emb_dim))

                        best_epoch_accuracy = 0.0

                        for emb_epoch in EMBEDDING_EPOCHS:
                            print("          epochs {}".format(emb_epoch))

                            emb_data_folder = data

                            if 'terrorists' in data:
                                emb_data_folder = 'terrorists'

                            if 'webkb' in data:
                                emb_data_folder = 'webkb'

                            prop_training_file = EMBEDDINGS_DATA_ROOT + "/" + emb_data_folder + "/" + fold + "/{}_dim{}_epoch{}.entity.emb".format(
                                emb_method, emb_dim, emb_epoch)
                            prop_training_labels = EMBEDDINGS_DATA_ROOT + "/" + data + "/" + fold + "/train.labels"

                            prop_test_labels = EMBEDDINGS_DATA_ROOT + "/" + data + "/" + fold + "/test.labels"

                            prop_pred = algo_prop_factory(prop_algo, parameter)

                            if not os.path.exists(prop_training_file):
                                continue

                            try:
                                prop_accuracy = prop_pred.learn_and_evaluate(prop_training_file, prop_training_labels,
                                                                             test_labels_file=prop_test_labels)
                            except Exception as e:
                                print(emb_method, "epochs", emb_epoch, "dim", emb_dim)
                                print(e)
                                prop_accuracy = 0.0

                            if prop_accuracy > best_epoch_accuracy:
                                best_epoch_accuracy = prop_accuracy

                        performance_diff = best_epoch_accuracy - rel_accuracy

                        results.append({'name': comp_case['name'],
                                        'emb_method': emb_method,
                                        'emb_dim': emb_dim,
                                        'relational_algo': rel_algo,
                                        'propositional_algo': prop_algo,
                                        'acc_diff': performance_diff,
                                        'dataset': data,
                                        'combined_info': "{}-{}-{}-{}".format(prop_algo, rel_algo, emb_method, emb_dim)})

        data = alt.Data(values=results)

        chart = alt.Chart(data).mark_bar().encode(
            x='emb_method:N',
            y='mean(acc_diff):Q',
            column='dataset:N'
        )

        chart.savechart(
            os.path.dirname(__file__) + "/../results/classification-{}-{}-{}.json".format("_".join(CLASSIFICATION_DATA),
                                                                                          "_".join(EMBEDDING_METHODS),
                                                                                          "_".join([x['name'] for x in COMPARISON_CASES]))
        )






