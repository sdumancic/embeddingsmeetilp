import os
import networkx as nx

DATA_ROOT = os.path.dirname(__file__) + "/../data/kbc/2_process/fb15k-237"

TRAIN = DATA_ROOT + "/train.txt"
VALID = DATA_ROOT + "/valid.txt"


graph = nx.Graph()

covered_nodes = set()
relation_counter = {}


for line in open(TRAIN).readlines():
    if len(line) < 3:
        continue

    rel, args = line.strip().replace(")", "").split("(")
    args = args.split(",")

    for n in args:
        if n not in relation_counter:
            relation_counter[n] = set()
        relation_counter[n].add(rel)
        if n not in covered_nodes:
            covered_nodes.add(n)
            graph.add_node(n)

    graph.add_edge(*args, label=rel)

degree_distribution = graph.degree()

degree_values = []
relation_number_values = []

for item in relation_counter:
    relation_number_values.append({'entity': item, 'count': len(relation_counter[item])})

for item in degree_distribution:
    degree_values.append({"node": item[0], "degree": item[1]})

fi = open("degree_distribution.json", 'w')
fi.write(str(degree_values))
fi.close()

fl = open("number_of_relations.json", 'w')
fl.write(str(relation_number_values))
fl.close()



