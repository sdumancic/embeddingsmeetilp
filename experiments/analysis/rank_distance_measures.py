import pickle
import os
import altair as alt
import numpy as np

EMBEDDING_METHODS = ['transE', 'DistMult']

EMBEDDING_DIMENSION = [10, 20, 30, 50, 80, 100]

EMBEDDING_EPOCHS = [20, 40, 60, 80, 100]

CLUSTERING_DATA = ['imdb', 'uwcse', 'terrorists', 'mutagenesis', 'webkb']

CLUSTERING_ALGO = ['Spectral', 'Hierarchical']


def rank_per_dimension_embedding_epochs(data, dim, embedding_method, epochs, datasets, clustering_algo):
    filtered_data = []
    ranking_per_distance = {}

    for item in data:
        if item['dimension'] == dim \
                and item['embedding'] == embedding_method \
                and item['epochs'] == epochs \
                and item['algo'] == clustering_algo:
            filtered_data.append(item)

    for data_id in datasets:
        inner_data = []
        for item in filtered_data:
            if item['dataset'] == data_id:
                inner_data.append(item)
        inner_data = sorted(inner_data, key=lambda x: x['ari'], reverse=True)

        for ind, item in enumerate(inner_data):
            if item['distance'] not in ranking_per_distance:
                ranking_per_distance[item['distance']] = []
            ranking_per_distance[item['distance']].append(ind + 1)

    ranking_per_distance = dict([(x, np.array(ranking_per_distance[x]).mean()) for x in ranking_per_distance])
    ranking_per_distance = [{"distance": x,
                             "rank": ranking_per_distance[x],
                             "dimension": dim,
                             "embedding": embedding_method,
                             "epochs": epochs,
                             "algo": clustering_algo} for x in ranking_per_distance]
    return ranking_per_distance


def rank_similarity_measures(result_file):
    f = open(result_file, 'rb')
    data_points = pickle.load(f)
    f.close()

    ranking_data = []

    for method in EMBEDDING_METHODS:
        for dimension in EMBEDDING_DIMENSION:
            for epochs in EMBEDDING_EPOCHS:
                for cl_algo in CLUSTERING_ALGO:
                    ranking_data = ranking_data + rank_per_dimension_embedding_epochs(data_points, dimension, method,
                                                                                          epochs, CLUSTERING_DATA, cl_algo)

    data = alt.Data(values=ranking_data)

    chart = alt.Chart(data).mark_line().encode(
        x='dimension:Q',
        y='rank:O',
        column='epochs:O',
        row='embedding:N',
        color='distance:N'
    )

    chart.savechart(os.path.dirname(__file__) + "/../../results/clustering_rankings.json")

    epochs = 100
    for cl_algo in CLUSTERING_ALGO:
        print("CLUSTERING ALGORITHM: {}".format(cl_algo))
        for emb_method in EMBEDDING_METHODS:
            print("EMBEDDING METHOD: {}".format(emb_method))
            DISTANCES = []
            if cl_algo == 'Spectral':
                DISTANCES = DISTANCE_METRICS_SPECTRAL
            else:
                DISTANCES = DISTANCE_METRICS_HIERARCHICAL
            print("{0: <20}\t".format("distance"), end='')

            for d in EMBEDDING_DIMENSION:
                print("{0: < 6}\t".format(d), end='')
            print("{0: <6}".format('Avg'))

            print("-"*5)

            distance_info = {}

            for distance in DISTANCES:
                # print("{0: <20}\t".format(distance), end='')
                individuals = []
                for dim in EMBEDDING_DIMENSION:
                    for item in ranking_data:
                        if item['embedding'] == emb_method\
                                and item['algo'] == cl_algo \
                                and item['dimension'] == dim\
                                and item['distance'] == distance\
                                and item['epochs'] == epochs:
                            # print("{0: < 6}\t".format(item['rank']), end='')
                            individuals.append(item['rank'])

                            if distance not in distance_info:
                                distance_info[distance] = {}
                            distance_info[distance][dim] = item['rank']
                distance_info[distance]['avg'] = round(np.array(individuals).mean(), 2)
                # print("{0: <6}".format(round(np.array(individuals).mean(), 2)))

            distances_order = sorted(list(distance_info.keys()), key=lambda x: distance_info[x]['avg'])

            for dist in distances_order:
                print("{0: <20}\t".format(dist), end='')
                for dim in EMBEDDING_DIMENSION:
                    print("{0: < 6}\t".format(distance_info[dist][dim]), end='')
                print("{0: <6}".format(distance_info[dist]['avg']))

            print("*"*20)
        print("\n"*10)


DISTANCE_METRICS_HIERARCHICAL = ['euclidean', 'l1', 'l2', 'manhattan', 'cosine', 'cityblock', 'braycurtis', 'canberra',
                                 'chebyshev', 'correlation', 'dice', 'hamming', 'jaccard', 'kulsinski', 'mahalanobis',
                                 'matching', 'minkowski', 'rogerstanimoto', 'russellrao', 'seuclidean',
                                 'sokalmichener', 'sokalsneath', 'sqeuclidean']
DISTANCE_METRICS_SPECTRAL = ['rbf', 'nearest_neighbors', 'poly', 'sigmoid', 'laplacian']


RESULT_ROOT = os.path.abspath(os.path.dirname(__file__)) + "/../../results"


if __name__ == '__main__':
    classification_data = [x for x in os.listdir(RESULT_ROOT) if x.startswith('clustering') and x.endswith('.pkl')][0]
    rank_similarity_measures(RESULT_ROOT + "/" + classification_data)
