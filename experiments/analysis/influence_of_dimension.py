import pickle
import os
import altair as alt

RESULT_ROOT = os.path.abspath(os.path.dirname(__file__)) + "/../../results"


def analyse_clustering_results(data_dumps, output_folder):
    print("analysing clustering")
    data_points = []

    print("data", data_dumps)

    for file in data_dumps:
        tmp_file = open(file, 'rb')
        tmp = pickle.load(tmp_file)
        data_points = data_points + tmp
        tmp_file.close()

    data = alt.Data(values=data_points)
    chart_mean = alt.Chart(data).mark_circle(color='red', size=60).encode(
        x='epochs:N',
        y='median(ari):Q',
        column='dataset:N',
        row='embedding:N',
        color='algo:N'
    ).transform_filter(
        alt.FieldOneOfPredicate(field='distance', oneOf=['poly', 'sigmoid', 'cosine', 'correlation'])
    )

    chart_min = alt.Chart(data).mark_circle(color='red', size=60).encode(
        x='epochs:N',
        y='min(ari):Q',
        column='dataset:N',
        row='embedding:N',
        color='algo:N'
    ).transform_filter(
        alt.FieldOneOfPredicate(field='distance', oneOf=['poly', 'sigmoid', 'cosine', 'correlation'])
    )

    chart_max = alt.Chart(data).mark_circle(color='red', size=60).encode(
        x='epochs:N',
        y='max(ari):Q',
        column='dataset:N',
        row='embedding:N',
        color='algo:N'
    ).transform_filter(
        alt.FieldOneOfPredicate(field='distance', oneOf=['poly', 'sigmoid', 'cosine', 'correlation'])
    )

    chart_data = alt.Chart(data).mark_point(color='blue').encode(
        x='algo:N',
        y='ari:Q',
        column='dataset:N',
        row='embedding:N',
        color='algo:N'
    ).transform_filter(
        alt.FieldOneOfPredicate(field='distance', oneOf=['poly', 'sigmoid', 'cosine', 'correlation'])
    ).transform_filter(
        alt.FieldEqualPredicate(field='epochs', equal=100)
    )

    # concatenated = alt.layer(chart_data, chart_mean, chart_max, chart_min)

    # concatenated.savechart(output_folder + "/dimension_clustering.json")

    chart_data.savechart(output_folder + "/dimension_data_clustering.json")


def analyse_classification_results(data_dumps, output_folder):
    data_points = []

    for file in data_dumps:
        tmp_file = open(file, 'rb')
        tmp = pickle.load(tmp_file)
        #print(tmp)
        data_points = data_points + tmp
        tmp_file.close()

    data = alt.Data(values=data_points)

    chart_mean = alt.Chart(data).mark_circle(color='red', size=60).encode(
        x='algo:N',
        y='median(mean):Q',
        column='dataset:N',
        row='embedding_method:N',
        color='algo:N'
    ).transform_filter(
        alt.FieldEqualPredicate(field='embedding_epoch', equal=100)
    ).transform_filter(
        alt.FieldOneOfPredicate(field='embedding_method', oneOf=['transE', 'DistMult'])
    )

    chart_min = alt.Chart(data).mark_circle(color='red', size=60).encode(
        x='epochs:N',
        y='min(mean):Q',
        column='dataset:N',
        row='embedding_method:N',
        color='algo:N'
    )

    chart_max = alt.Chart(data).mark_circle(color='red', size=60).encode(
        x='epochs:N',
        y='max(mean):Q',
        column='dataset:N',
        row='embedding_method:N',
        color='algo:N'
    )

    chart_data = alt.Chart(data).mark_point(color='blue').encode(
        x='algo:N',
        y='mean:Q',
        column='dataset:N',
        row='embedding_method:N',
        color='algo:N'
    ).transform_filter(
        alt.FieldEqualPredicate(field='embedding_epoch', equal=100)
    ).transform_filter(
        alt.FieldOneOfPredicate(field='embedding_method', oneOf=['transE', 'DistMult'])
    )

    # joint = alt.layer(chart_data, chart_mean, chart_min, chart_max)

    # joint.savechart(output_folder + "/dimension_classification.json")

    chart_data.savechart(output_folder + "/dimension_data_classification.json")


if __name__ == '__main__':
    clustering_results = [x for x in os.listdir(RESULT_ROOT) if x.startswith('clustering') and x.endswith(".pkl")]
    classification_results = [x for x in os.listdir(RESULT_ROOT) if x.startswith('classification') and x.endswith(".pkl")]

    clustering_results = [RESULT_ROOT + "/" + x for x in clustering_results]
    classification_results = [RESULT_ROOT + "/" + x for x in classification_results]

    #print("clustering", clustering_results)
    #print("classification", classification_results)

    analyse_clustering_results(clustering_results, RESULT_ROOT)
    print("saved clustering")
    analyse_classification_results(classification_results, RESULT_ROOT)
    print("saved classification")

