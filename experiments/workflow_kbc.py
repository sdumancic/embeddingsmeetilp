import os
import pickle
from learners.RelationalLearner import TILDE, AMIE, Aleph, Metagol
from learners.LearnerSettings import LearnerSettings
#import altair as alt

DATA_ROOT = os.path.abspath(os.path.dirname(__file__)) + "/../data/kbc/2_process"
OUT_ROOT = os.path.abspath(os.path.dirname(__file__)) + "/../data/kbc/3_results"

RELATIONAL_DATA = ['fb15k-237'] #['fb15k-237', 'wn18-rr']  # 'yago10'
RELATIONAL_ALGO = ['TILDE'] # , 'AMIE', 'Aleph']

SCRATCH_SPACE = os.path.abspath(os.path.dirname(__file__)) + "/../results/scratch/KBC"
SUB_SAMPLE_RATE = 100

PARALLEL_LEARNING = True
EXCLUDE_TEST_NEGATIVES = False
TILDE_FINAL_RUN_INDUCE = False
MERGE_DATA_FOR_FINAL_LEARNING = True
EXCLUDE_NEGATIVES_IN_TRAINING = False
SPECIAL_PREDICT = None


def prepare_data(train, test, valid, target, tmp_dir):
    train_db = open("{}/train.db".format(tmp_dir), 'w')
    train_lb = open("{}/train.labels".format(tmp_dir), 'w')

    test_db = open("{}/test.db".format(tmp_dir), 'w')
    test_lb = open("{}/test.labels".format(tmp_dir), 'w')

    for pred in train:
        for inst in train[pred]:
            if pred.lower() == target.lower():
                train_lb.write("{}({},{})\n".format(pred.capitalize(), inst[0].capitalize(), inst[1].capitalize()))
            else:
                train_db.write("{}({},{})\n".format(pred.capitalize(), inst[0].capitalize(), inst[1].capitalize()))

    for pred in valid:
        for inst in valid[pred]:
            if pred.lower() == target.lower():
                train_lb.write("{}({},{})\n".format(pred.capitalize(), inst[0].capitalize(), inst[1].capitalize()))
            else:
                train_db.write("{}({},{})\n".format(pred.capitalize(), inst[0].capitalize(), inst[1].capitalize()))

    for pred in test:
        for inst in test[pred]:
            if pred.lower() == target.lower():
                test_lb.write("{}({},{})\n".format(pred.capitalize(), inst[0].capitalize(), inst[1].capitalize()))
            else:
                test_db.write("{}({},{})\n".format(pred.capitalize(), inst[0].capitalize(), inst[1].capitalize()))

    d = open("{}/predicate.defs".format(tmp_dir), 'w')

    for rel in train:
        d.write("{}(entity,entity)\n".format(rel.capitalize()))

    train_db.close()
    train_lb.close()
    test_db.close()
    test_lb.close()
    d.close()


def algo_rel_factory(algo, params, work_dir):
    if algo.lower() == 'tilde':
        return TILDE(params, work_dir, final_run_induce=TILDE_FINAL_RUN_INDUCE, exclude_negatives=EXCLUDE_NEGATIVES_IN_TRAINING, special_predict=SPECIAL_PREDICT)
    elif algo.lower() == 'aleph':
        return Aleph(params, work_dir)
    elif algo.lower() == 'amie':
        return AMIE(params, work_dir)
    elif algo.lower() == 'metagol':
        return Metagol(params, work_dir)
    else:
        return Aleph(params, work_dir)


if __name__ == '__main__':
    if not os.path.exists(SCRATCH_SPACE):
        os.mkdir(SCRATCH_SPACE)

    if not os.path.exists(OUT_ROOT):
        os.mkdir(OUT_ROOT)

    parameter = LearnerSettings()
    parameter.add_parameter('aleph_evalfn', 'posonly')
    #parameter.add_parameter('aleph_search', 'bf')
    parameter.add_parameter('aleph_searchtime', 270)
    parameter.add_parameter('aleph_samplesize', 0)
    parameter.add_parameter('aleph_clauselength', 3)
    parameter.add_parameter('timeout', 60)
    parameter.add_parameter('amie_maxad', [3, 4])
    parameter.add_parameter('amie_minc', [1.0, 0.9, 0.8, 0.7])
    parameter.add_parameter('amie_minpca', [0.1, 0.5])
    parameter.add_parameter('max_depth', [4, 8, 12])
    parameter.add_parameter('minimal_cases', [4, 8, 12, 24, 48])

    results = [] # {data, relation, algorithm, accuracy}

    for data in RELATIONAL_DATA:
        print("processing data " + data)
        data_dir = DATA_ROOT + "/" + data

        train_f = open("{}/train.pkl".format(data_dir), 'rb')
        test_f = open("{}/test.pkl".format(data_dir), 'rb')
        valid_f = open("{}/valid.pkl".format(data_dir), 'rb')

        train = pickle.load(train_f)
        test = pickle.load(test_f)
        valid = pickle.load(valid_f)

        tmp_dir = OUT_ROOT + "/" + data

        if not os.path.exists(tmp_dir):
            # print("creating ", tmp_dir)
            os.mkdir(tmp_dir)

        tmp_dir = tmp_dir + "/tmp"

        if not os.path.exists(tmp_dir):
            # print("creating ", tmp_dir)
            os.mkdir(tmp_dir)

        for target_rel in train:
            print("    relation {} ".format(target_rel))

            tmp_inn = tmp_dir

            if PARALLEL_LEARNING:
                tmp_inn = tmp_dir + "/" + target_rel

                if not os.path.exists(tmp_inn):
                    os.mkdir(tmp_inn)

            prepare_data(train, test, valid, target_rel, tmp_inn)

            scratch_dir = SCRATCH_SPACE + "/" + data

            if not os.path.exists(scratch_dir):
                # print("creating ", scratch_dir)
                os.mkdir(scratch_dir)

            for rel_algo in RELATIONAL_ALGO:
                print("      with {} ".format(rel_algo))
                scratch_dir_inner = scratch_dir + "/" + rel_algo

                if not os.path.exists(scratch_dir_inner):
                    print("creating ", scratch_dir_inner)
                    os.mkdir(scratch_dir_inner)

                scratch_dir_inner += "/" + target_rel

                if not os.path.exists(scratch_dir_inner):
                    print("creating ", scratch_dir_inner)
                    os.mkdir(scratch_dir_inner)
                elif PARALLEL_LEARNING:
                    continue
                else:
                    pass

                training_data = tmp_inn + "/train.db"
                training_labels = tmp_inn + "/train.labels"

                test_data = tmp_inn + "/test.db"
                test_labels = tmp_inn + "/test.labels"

                defs = tmp_inn + "/predicate.defs"

                predictor = algo_rel_factory(rel_algo, parameter, scratch_dir_inner)
                predictor.do_negative_sampling(SUB_SAMPLE_RATE)

                acc = predictor.learn_and_evaluate([training_data], [training_labels], test_data_file=test_data,
                                                   test_labels_file=test_labels, definitions=defs,
                                                   exclude_test_negatives=EXCLUDE_TEST_NEGATIVES,
                                                   append_training_labels=MERGE_DATA_FOR_FINAL_LEARNING)

                results.append({"data": data, "relation": target_rel, "algo": rel_algo, "acc": acc})

            os.remove(tmp_inn + "/train.db")
            os.remove(tmp_inn + "/train.labels")
            os.remove(tmp_inn + "/test.db")
            os.remove(tmp_inn + "/test.labels")
            os.remove(tmp_inn + "/predicate.defs")

    print("DONE!!")
"""
    res_file_name = os.path.abspath(os.path.dirname(__file__)) + "/../results/kbc"

    if not os.path.exists(res_file_name):
        os.mkdir(res_file_name)

    res_file_name += "/kbc-{}-{}.pkl".format('_'.join(RELATIONAL_DATA), '_'.join(RELATIONAL_ALGO))
    res_file = open(res_file_name, 'wb')
    pickle.dump(results, res_file)
    res_file.close()

    data = alt.Data(values=results)
    chart = alt.Chart(data).mark_bar().encode(
        x="relation:N",
        y="acc:Q",
        row="data:N"
    )

    chart.savechart(os.path.abspath(os.path.dirname(__file__)) + "/../results/kbc/kbc-{}-{}.json".format(
        '_'.join(RELATIONAL_DATA), '_'.join(RELATIONAL_ALGO)
    ))

    print("DONE!!")
"""






