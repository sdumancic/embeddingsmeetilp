import os
from learners.PropositionalPrediction import DecisionTree, SVM, kNN
from learners.RelationalLearner import TILDE, Aleph, AMIE, Metagol
from learners.LearnerSettings import LearnerSettings
import numpy as np
import altair as alt
import pickle

CLASSIFICATION_ROOT = os.path.dirname(__file__) + "/../data/classification"
RELATIONAL_DATA_ROOT = CLASSIFICATION_ROOT + "/2_folds"
RELATIONAL_DEFINITIONS_ROOT = CLASSIFICATION_ROOT + "/1_raw"
EMBEDDINGS_DATA_ROOT = CLASSIFICATION_ROOT + "/4_embeddings"

CLASSIFICATION_DATA = ['mutagenesis', 'terrorists', 'webkb', 'hepatitis']
RELATIONAL_ALGORITHMS = ['TILDE']  # 'Aleph' 'AMIE'
EMBEDDING_ALGORITHMS = ['kNN', 'DecisionTree', 'SVM']

EMBEDDING_METHODS = ['transE', 'DistMult'] # 'relational'
EMBEDDING_DIMENSION = [10, 20, 30, 50, 80, 100]
EMBEDDING_EPOCHS = [20, 40, 60, 80, 100]

WORKING_DIR = os.path.dirname(__file__) + "/../results"

if not os.path.exists(WORKING_DIR + "/scratch"):
    os.mkdir(WORKING_DIR + "/scratch")

WORKING_DIR = WORKING_DIR + "/scratch"


if __name__ == '__main__':
    def algo_prop_factory(algo, params):
        if algo == 'kNN':
            return kNN(params)
        elif algo == 'DecisionTree':
            return DecisionTree(params)
        else:
            return SVM(params)


    def algo_rel_factory(algo, params, work_dir):
        if algo.lower() == 'tilde':
            return TILDE(params, work_dir)
        elif algo.lower() == 'aleph':
            return Aleph(params, work_dir)
        elif algo.lower() == 'amie':
            return AMIE(params, work_dir)
        elif algo.lower() == 'metagol':
            return Metagol(params, work_dir)
        else:
            return Aleph(params, work_dir)

    results = []
    parameter = LearnerSettings()
    parameter.add_parameter('aleph_evalfn', 'accuracy')
    parameter.add_parameter('aleph_search', 'bf')
    parameter.add_parameter('aleph_searchtime', 180)
    parameter.add_parameter('aleph_samplesize', 3)
    parameter.add_parameter('timeout', 190)
    parameter.add_parameter('amie_maxad', [3, 4])
    parameter.add_parameter('amie_minc', [1.0, 0.9, 0.8, 0.7])
    parameter.add_parameter('amie_minpca', [0.1, 0.5])

    for data in CLASSIFICATION_DATA:
        print("Processing {} dataset...".format(data))
        intermediate_results = {}
        folds = os.listdir(EMBEDDINGS_DATA_ROOT + "/" + data)

        for fold in folds:
            print("  fold {}".format(fold))

            print("    RELATIONAL")
            for rel_algo in RELATIONAL_ALGORITHMS:
                print("      algorithm {}".format(rel_algo))
                training_file = RELATIONAL_DATA_ROOT + "/" + data + "/" + fold + "/training.db"
                training_labels = RELATIONAL_DATA_ROOT + "/" + data + "/" + fold + "/training.labels"

                test_file = RELATIONAL_DATA_ROOT + "/" + data + "/" + fold + "/test.db"
                test_labels = RELATIONAL_DATA_ROOT + "/" + data + "/" + fold + "/test.labels"

                definition_file = RELATIONAL_DEFINITIONS_ROOT + "/" + data + "/predicate.defs"

                # prepare scratch folder
                current_working_dir = WORKING_DIR + "/" + rel_algo
                if not os.path.exists(current_working_dir):
                    os.mkdir(current_working_dir)

                current_working_dir = current_working_dir + "/" + data
                if not os.path.exists(current_working_dir):
                    os.mkdir(current_working_dir)

                current_working_dir = current_working_dir + "/" + fold
                if not os.path.exists(current_working_dir):
                    os.mkdir(current_working_dir)

                rel_pred = algo_rel_factory(rel_algo, parameter, current_working_dir)

                accuracy = rel_pred.learn_and_evaluate([training_file], [training_labels], test_data_file=test_file,
                                                       test_labels_file=test_labels, definitions=definition_file)

                algo_tuple = tuple([rel_algo, "", 0, 0])
                if algo_tuple not in intermediate_results:
                    intermediate_results[algo_tuple] = []

                intermediate_results[algo_tuple].append(accuracy)

            print("    EMBEDDING")
            for prop_algo in EMBEDDING_ALGORITHMS:
                print("    algorithm {}".format(prop_algo))
                for emb_method in EMBEDDING_METHODS:
                    print("      {} embeddings".format(emb_method))

                    if emb_method == 'relational':
                        training_file = EMBEDDINGS_DATA_ROOT + "/" + data + "/" + fold + "/relational.entity.emb"
                        training_labels = EMBEDDINGS_DATA_ROOT + "/" + data + "/" + fold + "/train.labels"

                        test_labels = EMBEDDINGS_DATA_ROOT + "/" + data + "/" + fold + "/test.labels"

                        if not os.path.exists(training_file):
                            rf = open(training_file, 'w')

                            emb_file = open(EMBEDDINGS_DATA_ROOT + "/" + data + "/" + fold + "/relational_features.training.pkl", 'rb')
                            feats = pickle.load(emb_file)

                            for ent in feats:
                                rf.write("{} {}\n".format(ent, ','.join([str(int(x)) for x in feats[ent]])))
                            emb_file.close()
                            emb_file = open(
                                EMBEDDINGS_DATA_ROOT + "/" + data + "/" + fold + "/relational_features.test.pkl", 'rb')

                            feats = pickle.load(emb_file)
                            for ent in feats:
                                rf.write("{} {}\n".format(ent, ','.join([str(int(x)) for x in feats[ent]])))
                            emb_file.close()

                            rf.close()

                        prop_pred = algo_prop_factory(prop_algo, parameter)
                        accuracy = prop_pred.learn_and_evaluate(training_file, training_labels,
                                                                test_labels_file=test_labels)

                        algo_tuple = tuple([prop_algo, emb_method, 0, 0])
                        if algo_tuple not in intermediate_results:
                            intermediate_results[algo_tuple] = []
                        intermediate_results[algo_tuple].append(accuracy)
                    else:
                        for emb_dim in EMBEDDING_DIMENSION:
                            print("        dimension {}".format(emb_dim))
                            for emb_epoch in EMBEDDING_EPOCHS:
                                print("          epochs {}".format(emb_epoch))
                                training_file = EMBEDDINGS_DATA_ROOT + "/" + data + "/" + fold + "/{}_dim{}_epoch{}.entity.emb".format(emb_method, emb_dim, emb_epoch)
                                training_labels = EMBEDDINGS_DATA_ROOT + "/" + data + "/" + fold + "/train.labels"

                                test_labels = EMBEDDINGS_DATA_ROOT + "/" + data + "/" + fold + "/test.labels"

                                prop_pred = algo_prop_factory(prop_algo, parameter)

                                accuracy = prop_pred.learn_and_evaluate(training_file, training_labels, test_labels_file=test_labels)

                                algo_tuple = tuple([prop_algo, emb_method, emb_dim, emb_epoch])
                                if algo_tuple not in intermediate_results:
                                    intermediate_results[algo_tuple] = []
                                intermediate_results[algo_tuple].append(accuracy)

        for algo_tuple in intermediate_results:
            alg = algo_tuple[0]
            emb_m = algo_tuple[1]
            emb_d = algo_tuple[2]
            emb_e = algo_tuple[3]
            results.append({'algo': alg,
                            'level': 'relational' if alg in RELATIONAL_ALGORITHMS else 'propositional',
                            'mean': np.array(intermediate_results[algo_tuple]).mean(),
                            'std': np.array(intermediate_results[algo_tuple]).std(),
                            'dataset': data,
                            'embedding_method': emb_m,
                            'embedding_dim': emb_d,
                            'embedding_epoch': emb_e
                            })

    results.append({
        "algo": "TILDE-latent",
        "dataset": "webkb",
        "embedding_dim": 0,
        "embedding_epoch": 0,
        "embedding_method": "",
        "level": "relational",
        "mean": 0.91,
        "std": 0.0
      })

    results.append({
        "algo": "TILDE-latent",
        "dataset": "hepatitis",
        "embedding_dim": 0,
        "embedding_epoch": 0,
        "embedding_method": "",
        "level": "relational",
        "mean": 0.93,
        "std": 0.0
      })

    results.append({
        "algo": "TILDE-latent",
        "dataset": "terrorists",
        "embedding_dim": 0,
        "embedding_epoch": 0,
        "embedding_method": "",
        "level": "relational",
        "mean": 0.72,
        "std": 0.0
      })

    results.append({
        "algo": "TILDE-latent",
        "dataset": "mutagenesis",
        "embedding_dim": 0,
        "embedding_epoch": 0,
        "embedding_method": "",
        "level": "relational",
        "mean": 0.83,
        "std": 0.0
      })
    '''
    results.append({
        "algo": "TILDE",
        "dataset": "webkb",
        "embedding_dim": 0,
        "embedding_epoch": 0,
        "embedding_method": "",
        "level": "relational",
        "mean": 0.81,
        "std": 0.0
      })

    results.append({
        "algo": "TILDE",
        "dataset": "hepatitis",
        "embedding_dim": 0,
        "embedding_epoch": 0,
        "embedding_method": "",
        "level": "relational",
        "mean": 0.81,
        "std": 0.0
      })

    results.append({
        "algo": "TILDE",
        "dataset": "terrorists",
        "embedding_dim": 0,
        "embedding_epoch": 0,
        "embedding_method": "",
        "level": "relational",
        "mean": 0.72,
        "std": 0.0
      })

    results.append({
        "algo": "TILDE",
        "dataset": "mutagenesis",
        "embedding_dim": 0,
        "embedding_epoch": 0,
        "embedding_method": "",
        "level": "relational",
        "mean": 0.76,
        "std": 0.0
      })
    '''
    data = alt.Data(values=results)

    chart = alt.Chart(data).mark_bar().encode(
        x='algo:N',
        y='mean:Q',
        color='level:N',
        column='dataset:N'
    )

    chart.savechart(os.path.dirname(__file__) + "/../results/classification-{}-{}-{}.json".format("_".join(CLASSIFICATION_DATA), "_".join(RELATIONAL_ALGORITHMS), '_'.join(EMBEDDING_METHODS)))

    pf = open(os.path.dirname(__file__) + "/../results/classification-{}-{}-{}.pkl".format("_".join(CLASSIFICATION_DATA), "_".join(RELATIONAL_ALGORITHMS), '_'.join(EMBEDDING_METHODS)), 'wb')
    pickle.dump(results, pf)
    pf.close()
    print("DONE!!")
