import os

RELATIONAL_DATA = ['fb15k-237', 'wn18-rr']  # 'yago10'
RELATIONAL_ALGO = ['TILDE'] # , 'AMIE', 'Aleph']

SCRATCH_SPACE = os.path.abspath(os.path.dirname(__file__)) + "/../results/scratch/KBC"



for data in RELATIONAL_DATA:
    current_data_dir = SCRATCH_SPACE + "/" + data
    results = {}
    print("data: ", data)

    for algo in RELATIONAL_ALGO:
        current_algo_dir = current_data_dir + "/" + algo
        relations = os.listdir(current_algo_dir)

        for rel in relations:
            tmp_dir = current_algo_dir + "/" + rel

            log_file = tmp_dir + "/tilde/train.summary.uL"

            complexity = -1
            num_examples = -1
            accuracy = -1

            if os.path.exists(log_file):
                ff = open(log_file)
                testing_flag = False

                for line in ff.readlines():
                    if 'Complexity' in line:
                        tmp = line.split()[1]
                        complexity = float(tmp)
                    elif 'Testing:' in line:
                        testing_flag = True
                    elif testing_flag and 'Number of examples' in line:
                        num_examples = int(line.strip().split()[-1])
                    elif testing_flag and 'Accuracy' in line:
                        accuracy = float(line.split()[1])

                results[rel] = {'accuracy': accuracy, 'num_examples': num_examples, 'complexity': complexity}

    print("    Average accuracy: ", float(sum([results[x]['accuracy'] for x in results]))/float(len(results)))
    weighted_sum = sum([results[x]['accuracy'] * results[x]['num_examples'] for x in results])
    total_examples = sum([results[x]['num_examples'] for x in results])
    print("    Weighted accuracy: ", float(weighted_sum)/float(total_examples))
    print("    Average complexity: ", float(sum([results[x]['complexity'] for x in results]))/float(len(results)))
    print("    Weighted complexity: ", float(sum([results[x]['complexity'] * results[x]['num_examples'] for x in results]))/float(total_examples))