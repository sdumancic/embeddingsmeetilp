import os
from learners.FeatureGenerators import AlephFeatureGenerator, AlephFeatureGeneratorPerEntity, AlephFeatureGeneratorDisc
from learners.LearnerSettings import LearnerSettings
import datetime

CLASSIFICATION_ROOT = os.path.abspath(os.path.dirname(__file__)) + "/../data/classification"
RELATIONAL_DATA_ROOT = CLASSIFICATION_ROOT + "/2_folds"
RELATIONAL_DEFINITIONS_ROOT = CLASSIFICATION_ROOT + "/1_raw"
EMBEDDINGS_DATA_ROOT = CLASSIFICATION_ROOT + "/4_embeddings"

CLASSIFICATION_DATA = ['webkb'] # 'hepatitis',
RELATIONAL_ALGORITHMS = ['AlephFeatureGeneratorDisc'] #, 'AlephFeatureGeneratorPerEntity'] #'Aleph'
ACCURACY_THRESHOLDS = [0.1, 0.3, 0.5, 0.7]
MIN_POS_THRESHOLDS = [0.01, 0.05]
MAX_FEATURES = [1000]
SETTING = 'transductive'

WORKING_DIR = os.path.abspath(os.path.dirname(__file__)) + "/../results"

if not os.path.exists(WORKING_DIR + "/scratch"):
    os.mkdir(WORKING_DIR + "/scratch")

WORKING_DIR = WORKING_DIR + "/scratch"

if not os.path.exists(WORKING_DIR + "/features"):
    os.mkdir(WORKING_DIR + "/features")

WORKING_DIR = WORKING_DIR + "/features"

PREDICATE_BIAS = {
    'mutagenesis': {
        'bond_bondtype1': ['(+aatom,-aatom)', '(-aatom,+aatom)'],
        'bond_bondtype2': ['(+aatom,-aatom)', '(-aatom,+aatom)'],
        'bond_bondtype3': ['(+aatom,-aatom)', '(-aatom,+aatom)'],
        'bond_bondtype4': ['(+aatom,-aatom)', '(-aatom,+aatom)'],
        'bond_bondtype5': ['(+aatom,-aatom)', '(-aatom,+aatom)'],
        'bond_bondtype7': ['(+aatom,-aatom)', '(-aatom,+aatom)'],
        'element': ['(+aatom,#typee)'],
        'atype': ['(+aatom,#typea)'],
        'logp': ['(+mol,#typlelog)'],
        'inda': ['(+mol,#typeia)'],
        'mol2atm': ['(+mol,-aatom)', '(-mol,+aatom)'],
        'ind1': ['(+mol,#typeio)'],
        'lumo': ['(+mol,#typel)'],
        'charge': ['(+aatom,#typec)']
    },
    'webkb_bck': {
        'hasanchor': ['(+link,#word)'],
        'hasneighborhood': ['(+link,#word)'],
        'hasword': ['(+page,#word)', '(+page,-word)'],
        'instructorof': ['(+page,-page)', '(-page,+page)', '(+page,+page)'],
        'linkprop': ['(+link,#propertylink)'],
        'linkto': ['(+link,+page,-page)', '(+link,-page,-page)', '(-link,+page,+page)', '(-link,-page,+page)'],
        'memberofproject': ['(+page,-page)', '(-page,+page)', '(+page,+page)']
    },
    'bongard4': {
        'circle': ['(+pic,-obj)', '(-pic,+obj)'],
        'config': ['(+pic,-obj,#dir)', '(+pic,+obj,#dir)'],
        'eastof': ['(+pic,+obj,-obj)', '(+pic,-obj,+obj)'],
        'inside': ['(+pic,+obj,-obj)', '(+pic,-obj,+obj)'],
        'northof': ['(+pic,+obj,-obj)', '(+pic,-obj,+obj)'],
        'square': ['(+pic,-obj)', '(-pic,+obj)'],
        'triangle': ['(+pic,-obj)', '(-pic,+obj)']
    },
    'uwcse': {
        'courselevel': ['(+course,#level)'],
        'phase': ['(+human,#phase)'],
        'position': ['(+human,#faculty)'],
        'professor': ['(+human)'],
        'projectmember': ['(+project,-human)', '(-project,+human)'],
        'publication': ['(+ref,-human)', '(-ref,+human)'],
        'student': ['(+human)'],
        'ta': ['(+course,-human)', '(-course,+human)'],
        'taughtby': ['(+course,-human)', '(-course,+human)'],
        'tempadvisedby': ['(+human,-human)', '(-human,+human)'],
        'yearsinprogram': ['(+human,#years)'],
        'kmap': ['(+key,-human,-human)']
    },
    'canc': {
        'atm_as': ['(+drug,+atomid,#atomtype,#charge)', '(+drug,-atomid,#atomtype,#charge)', '(-drug,+atomid,#atomtype,#charge)', '(-drug,+atomid,-atomtype,#charge)', '(-drug,+atomid,#atomtype,-charge)'],
        'atm_ba': ['(+drug,+atomid,#atomtype,#charge)', '(+drug,-atomid,#atomtype,#charge)', '(-drug,+atomid,#atomtype,#charge)', '(-drug,+atomid,-atomtype,#charge)', '(-drug,+atomid,#atomtype,-charge)'],
        'atm_br': ['(+drug,+atomid,#atomtype,#charge)', '(+drug,-atomid,#atomtype,#charge)', '(-drug,+atomid,#atomtype,#charge)', '(-drug,+atomid,-atomtype,#charge)', '(-drug,+atomid,#atomtype,-charge)'],
        'atm_c': ['(+drug,+atomid,#atomtype,#charge)', '(+drug,-atomid,#atomtype,#charge)', '(-drug,+atomid,#atomtype,#charge)', '(-drug,+atomid,-atomtype,#charge)', '(-drug,+atomid,#atomtype,-charge)'],
        'atm_ca': ['(+drug,+atomid,#atomtype,#charge)', '(+drug,-atomid,#atomtype,#charge)', '(-drug,+atomid,#atomtype,#charge)', '(-drug,+atomid,-atomtype,#charge)', '(-drug,+atomid,#atomtype,-charge)'],
        'atm_cl': ['(+drug,+atomid,#atomtype,#charge)', '(+drug,-atomid,#atomtype,#charge)', '(-drug,+atomid,#atomtype,#charge)', '(-drug,+atomid,-atomtype,#charge)', '(-drug,+atomid,#atomtype,-charge)'],
        'atm_cu': ['(+drug,+atomid,#atomtype,#charge)', '(+drug,-atomid,#atomtype,#charge)', '(-drug,+atomid,#atomtype,#charge)', '(-drug,+atomid,-atomtype,#charge)', '(-drug,+atomid,#atomtype,-charge)'],
        'atm_f': ['(+drug,+atomid,#atomtype,#charge)', '(+drug,-atomid,#atomtype,#charge)', '(-drug,+atomid,#atomtype,#charge)', '(-drug,+atomid,-atomtype,#charge)', '(-drug,+atomid,#atomtype,-charge)'],
        'atm_h': ['(+drug,+atomid,#atomtype,#charge)', '(+drug,-atomid,#atomtype,#charge)', '(-drug,+atomid,#atomtype,#charge)', '(-drug,+atomid,-atomtype,#charge)', '(-drug,+atomid,#atomtype,-charge)'],
        'atm_hg': ['(+drug,+atomid,#atomtype,#charge)', '(+drug,-atomid,#atomtype,#charge)', '(-drug,+atomid,#atomtype,#charge)', '(-drug,+atomid,-atomtype,#charge)', '(-drug,+atomid,#atomtype,-charge)'],
        'atm_i': ['(+drug,+atomid,#atomtype,#charge)', '(+drug,-atomid,#atomtype,#charge)', '(-drug,+atomid,#atomtype,#charge)', '(-drug,+atomid,-atomtype,#charge)', '(-drug,+atomid,#atomtype,-charge)'],
        'atm_k': ['(+drug,+atomid,#atomtype,#charge)', '(+drug,-atomid,#atomtype,#charge)', '(-drug,+atomid,#atomtype,#charge)', '(-drug,+atomid,-atomtype,#charge)', '(-drug,+atomid,#atomtype,-charge)'],
        'atm_mn': ['(+drug,+atomid,#atomtype,#charge)', '(+drug,-atomid,#atomtype,#charge)', '(-drug,+atomid,#atomtype,#charge)', '(-drug,+atomid,-atomtype,#charge)', '(-drug,+atomid,#atomtype,-charge)'],
        'atm_n': ['(+drug,+atomid,#atomtype,#charge)', '(+drug,-atomid,#atomtype,#charge)', '(-drug,+atomid,#atomtype,#charge)', '(-drug,+atomid,-atomtype,#charge)', '(-drug,+atomid,#atomtype,-charge)'],
        'atm_na': ['(+drug,+atomid,#atomtype,#charge)', '(+drug,-atomid,#atomtype,#charge)', '(-drug,+atomid,#atomtype,#charge)', '(-drug,+atomid,-atomtype,#charge)', '(-drug,+atomid,#atomtype,-charge)'],
        'atm_o': ['(+drug,+atomid,#atomtype,#charge)', '(+drug,-atomid,#atomtype,#charge)', '(-drug,+atomid,#atomtype,#charge)', '(-drug,+atomid,-atomtype,#charge)', '(-drug,+atomid,#atomtype,-charge)'],
        'atm_p': ['(+drug,+atomid,#atomtype,#charge)', '(+drug,-atomid,#atomtype,#charge)', '(-drug,+atomid,#atomtype,#charge)', '(-drug,+atomid,-atomtype,#charge)', '(-drug,+atomid,#atomtype,-charge)'],
        'atm_pb': ['(+drug,+atomid,#atomtype,#charge)', '(+drug,-atomid,#atomtype,#charge)', '(-drug,+atomid,#atomtype,#charge)', '(-drug,+atomid,-atomtype,#charge)', '(-drug,+atomid,#atomtype,-charge)'],
        'atm_s': ['(+drug,+atomid,#atomtype,#charge)', '(+drug,-atomid,#atomtype,#charge)', '(-drug,+atomid,#atomtype,#charge)', '(-drug,+atomid,-atomtype,#charge)', '(-drug,+atomid,#atomtype,-charge)'],
        'atm_se': ['(+drug,+atomid,#atomtype,#charge)', '(+drug,-atomid,#atomtype,#charge)', '(-drug,+atomid,#atomtype,#charge)', '(-drug,+atomid,-atomtype,#charge)', '(-drug,+atomid,#atomtype,-charge)'],
        'atm_sn': ['(+drug,+atomid,#atomtype,#charge)', '(+drug,-atomid,#atomtype,#charge)', '(-drug,+atomid,#atomtype,#charge)', '(-drug,+atomid,-atomtype,#charge)', '(-drug,+atomid,#atomtype,-charge)'],
        'atm_te': ['(+drug,+atomid,#atomtype,#charge)', '(+drug,-atomid,#atomtype,#charge)', '(-drug,+atomid,#atomtype,#charge)', '(-drug,+atomid,-atomtype,#charge)', '(-drug,+atomid,#atomtype,-charge)'],
        'atm_ti': ['(+drug,+atomid,#atomtype,#charge)', '(+drug,-atomid,#atomtype,#charge)', '(-drug,+atomid,#atomtype,#charge)', '(-drug,+atomid,-atomtype,#charge)', '(-drug,+atomid,#atomtype,-charge)'],
        'atm_zn': ['(+drug,+atomid,#atomtype,#charge)', '(+drug,-atomid,#atomtype,#charge)', '(-drug,+atomid,#atomtype,#charge)', '(-drug,+atomid,-atomtype,#charge)', '(-drug,+atomid,#atomtype,-charge)'],
        'sbond_1': ['(+drug,-atomid,-atomid)', '(+drug,+atomid,-atomid)', '(+drug,-atomid,+atomid)'],
        'sbond_2': ['(+drug,-atomid,-atomid)', '(+drug,+atomid,-atomid)', '(+drug,-atomid,+atomid)'],
        'sbond_3': ['(+drug,-atomid,-atomid)', '(+drug,+atomid,-atomid)', '(+drug,-atomid,+atomid)'],
        'sbond_7': ['(+drug,-atomid,-atomid)', '(+drug,+atomid,-atomid)', '(+drug,-atomid,+atomid)'],
    },
    'cora': {
        'author': ['(+bib,-author)', '(-bib,+author)'],
        'title': ['(+bib,-title)', '(-bib,+title)'],
        'venue': ['(+bib,-venue)', '(-bib,+venue)'],
        'haswordauthor': ['(+author,-word)', '(-author,+word)'],
        'haswordtitle': ['(+title,-word)', '(-title,+word)'],
        'haswordvenue': ['(+venue,-word)', '(-venue,+word)'],
        'sameauthor': ['(+author,-author)', '(-author,+author)'],
        'samebib': ['(+bib,-bib)', '(-bib,+bib)'],
        'samevenue': ['(+venue,-venue)', '(-venue,+venue)'],
        'sametitle': ['(+title,-title)', '(-title,+title)']
    },
    'hepatitis': {
        'dbil': ['(+indom,#dbiltype)'],
        'che': ['(+indom,#chetype)'],
        'tp': ['(+indom,#tptype)'],
        'got': ['(+indom,#gottype)'],
        'gpt': ['(+indom,#gpttype)'],
        'ztt': ['(+indom,#ztttype)'],
        'dur': ['(+atype,#durtype)'],
        'ttt': ['(+indom,#ttttype)'],
        'b_rel12': ['(+indom,-mdom)', '(-indom,+mdom)'],
        'b_rel13': ['(+atype,-mdom)', '(-atype,+mdom)'],
        'b_rel11': ['(+bdom,-mdom)', '(-bdom,+mdom)'],
        'tbil': ['(+indom,#tbiltype)'],
        'age': ['(+mdom,#agetype)'],
        'activity': ['(+bdom,#acttype)'],
        'fibros': ['(+bdom,#fibtype)'],
        'alb': ['(+indom,#albtype)'],
        'sex': ['(+mdom,#sextype)'],
        'tcho': ['(+indom,#tchotype)'],
    },
    'terrorists': {
        'colocatedevent': ['(+eventdom,-eventdom)', '(-eventdom,+eventdom)'],
        'haseventfeature': ['(+eventdom,#featuredom)', '(+eventdom,-featuredom)'],
        'performedbysameorg': ['(+eventdom,-eventdom)', '(-eventdom,+eventdom)']
    },
    'yeast': {
        'complex': ['(+gene,#complex)'],
        'enzyme': ['(+gene,#enzyme)'],
        'interaction': ['(+gene,-gene,#intertype)', '(-gene,+gene,#intertype)'],
        'location': ['(+gene,#location)'],
        'path': ['(+gene,-gene)', '(-gene,+gene)'],
        'phenotype': ['(+gene,#phenotype)'],
        'protein_class': ['(+gene,#class)'],
        'rcomplex': ['(+gene,#complex)'],
        'renzyme': ['(+gene,#enzyme)'],
        'rphenotype': ['(+gene,#phenotype)'],
        'rprotein_class': ['(+gene,#class)']
    }
}

if __name__ == '__main__':
    def algo_rel_factory(algo, params, work_dir):
        if algo == 'AlephFeatureGenerator':
            return AlephFeatureGenerator(params, work_dir)
        elif algo == 'AlephFeatureGeneratorPerEntity':
            return AlephFeatureGeneratorPerEntity(params, work_dir)
        elif algo == 'AlephFeatureGeneratorDisc':
            return AlephFeatureGeneratorDisc(params, work_dir)
        else:
            return AlephFeatureGenerator(params, work_dir)


    parameter = LearnerSettings()
    parameter.add_parameter('aleph_evalfn', 'coverage')
    parameter.add_parameter('aleph_nodes', 15000)
    parameter.add_parameter('aleph_clause_length', [10])
    parameter.add_parameter('aleph_min_acc', [0.5])
    parameter.add_parameter('aleph_min_pos', [5])
    parameter.add_parameter('aleph_searchtime', 60)
    # parameter.add_parameter('aleph_max_features', 1000)
    parameter.add_parameter('timeout', 3600)
    parameter.add_parameter('max_negative', 0)

    for data in CLASSIFICATION_DATA:
        print("Processing {} dataset...".format(data))
        folds = os.listdir(EMBEDDINGS_DATA_ROOT + "/" + data)

        for fold in folds:
            print("  fold {}".format(fold))

            for rel_algo in RELATIONAL_ALGORITHMS:
                print("      algorithm {} {}".format(rel_algo, datetime.datetime.now().strftime("%a, %d %B %Y %I:%M:%S")))

                for acc_limit in ACCURACY_THRESHOLDS:
                    print("              accuracy {}".format(acc_limit))

                    for fn in MAX_FEATURES:
                        print("                    max_features {}".format(fn))

                        parameter.add_parameter('aleph_min_acc', [acc_limit])
                        parameter.add_parameter('aleph_max_features', fn)

                        training_file = RELATIONAL_DATA_ROOT + "/" + data + "/" + fold + "/training.db"
                        training_labels = RELATIONAL_DATA_ROOT + "/" + data + "/" + fold + "/training.labels"

                        test_file = RELATIONAL_DATA_ROOT + "/" + data + "/" + fold + "/test.db"
                        test_labels = RELATIONAL_DATA_ROOT + "/" + data + "/" + fold + "/test.labels"

                        definition_file = RELATIONAL_DEFINITIONS_ROOT + "/" + data + "/predicate.defs"

                        # prepare scratch folder
                        current_working_dir = WORKING_DIR + "/" + rel_algo + "_acc" + str(acc_limit) + "_mf" + str(fn)
                        if not os.path.exists(current_working_dir):
                            os.mkdir(current_working_dir)

                        current_working_dir = current_working_dir + "/" + data
                        if not os.path.exists(current_working_dir):
                            os.mkdir(current_working_dir)

                        current_working_dir = current_working_dir + "/" + fold
                        if not os.path.exists(current_working_dir):
                            os.mkdir(current_working_dir)

                        rel_pred = algo_rel_factory(rel_algo, parameter, current_working_dir)

                        if data in PREDICATE_BIAS:
                            rel_pred.read_bias(PREDICATE_BIAS[data])

                        features = {}

                        if SETTING == 'transductive':
                            features = rel_pred.find_features([training_file, test_file], definition_file)
                        else:
                            features = rel_pred.find_features([training_file], definition_file)

                        print("                    found {} features".format(len(features)))
                        ff_file = open("{}/{}/{}/{}_acc{}_mf{}.features".format(RELATIONAL_DATA_ROOT, data, fold, rel_algo, acc_limit, fn), 'w')

                        for feat_ind in features:
                            ff_file.write("{}: {}.\n".format(feat_ind, features[feat_ind]))

                        ff_file.close()

            if SETTING == 'transductive':
                continue