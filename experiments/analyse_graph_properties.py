import networkx as nx
from networkx.algorithms import approximation as approx
import os
import numpy as np
import pickle
import math

CLASSIFICATION_ROOT = os.path.abspath(os.path.dirname(__file__)) + "/../data/classification"
RELATIONAL_DATA_ROOT = CLASSIFICATION_ROOT + "/1_raw"

RESULTS_FOLDER = os.path.abspath(os.path.dirname(__file__)) + "/../results/graph_analysis"

if not os.path.exists(RESULTS_FOLDER):
    os.mkdir(RESULTS_FOLDER)

CLASSIFICATION_DATA = ['hepatitis', 'canc2', 'uwcse2', 'mutagenesis', 'terrorists', 'yeast2', 'webkb']

UNINFORMED_GRAPH = False

"""
    1 - average over all components in the entire dataset
    2 - average within a fold, and then average over folds
    3 - average within a component, then average over the dataset
    4 - average within a component, then over folds, then over the dataset
"""
AVERAGING_TYPE = 1

FILTER_NANS = False

PREDICATE_BIAS = {
    'mutagenesis': {
        'bond_bondtype1': ['(+aatom,-aatom)', '(-aatom,+aatom)'],
        'bond_bondtype2': ['(+aatom,-aatom)', '(-aatom,+aatom)'],
        'bond_bondtype3': ['(+aatom,-aatom)', '(-aatom,+aatom)'],
        'bond_bondtype4': ['(+aatom,-aatom)', '(-aatom,+aatom)'],
        'bond_bondtype5': ['(+aatom,-aatom)', '(-aatom,+aatom)'],
        'bond_bondtype7': ['(+aatom,-aatom)', '(-aatom,+aatom)'],
        'element': ['(+aatom,#typee)'],
        'atype': ['(+aatom,#typea)'],
        'logp': ['(+mol,#typlelog)'],
        'inda': ['(+mol,#typeia)'],
        'mol2atm': ['(+mol,-aatom)', '(-mol,+aatom)'],
        'ind1': ['(+mol,#typeio)'],
        'lumo': ['(+mol,#typel)'],
        'charge': ['(+aatom,#typec)']
    },
    'webkb': {
        'hasanchor': ['(+link,#word)'],
        'hasneighborhood': ['(+link,#word)'],
        'hasword': ['(+page,#word)', '(+page,-word)'],
        'instructorsof': ['(+page,-page)', '(-page,+page)', '(+page,+page)'],
        'linkprop': ['(+link,#propertylink)'],
        'linkto_link0': ['(+linkid,-link)'],
        'linkto_page1': ['(+linkid,-page)'],
        'linkto_page2': ['(+linkid,-page)'],
        'membersofproject': ['(+page,-page)', '(-page,+page)', '(+page,+page)']
    },
    'webkb_bck': {
        'hasanchor': ['(+link,#word)'],
        'hasneighborhood': ['(+link,#word)'],
        'hasword': ['(+page,#word)', '(+page,-word)'],
        'instructorsof': ['(+page,-page)', '(-page,+page)', '(+page,+page)'],
        'linkprop': ['(+link,#propertylink)'],
        'linkto': ['(+link,+page,-page)', '(+link,-page,-page)', '(-link,+page,+page)', '(-link,-page,+page)'],
        'membersofproject': ['(+page,-page)', '(-page,+page)', '(+page,+page)']
    },
    'bongard4': {
        'circle': ['(+pic,-obj)', '(-pic,+obj)'],
        'config': ['(+pic,-obj,#dir)', '(+pic,+obj,#dir)'],
        'eastof': ['(+pic,+obj,-obj)', '(+pic,-obj,+obj)'],
        'inside': ['(+pic,+obj,-obj)', '(+pic,-obj,+obj)'],
        'northof': ['(+pic,+obj,-obj)', '(+pic,-obj,+obj)'],
        'square': ['(+pic,-obj)', '(-pic,+obj)'],
        'triangle': ['(+pic,-obj)', '(-pic,+obj)']
    },
    'uwcse': {
        'courselevel': ['(+course,#level)'],
        'phase': ['(+human,#phase)'],
        'position': ['(+human,#faculty)'],
        'professor': ['(+human)'],
        'projectmember': ['(+project,-human)', '(-project,+human)'],
        'publication': ['(+ref,-human)', '(-ref,+human)'],
        'student': ['(+human)'],
        'ta': ['(+course,-human)', '(-course,+human)'],
        'taughtby': ['(+course,-human)', '(-course,+human)'],
        'tempadvisedby': ['(+human,-human)', '(-human,+human)'],
        'yearsinprogram': ['(+human,#years)'],
        'kmap': ['(+key,-human,-human)'],
        'role': ['(+human,#role)']
    },
    'uwcse2': {
        'courselevel': ['(+course,#level)'],
        'phase': ['(+human,#phase)'],
        'position': ['(+human,#faculty)'],
        'professor': ['(+human)'],
        'projectmember': ['(+project,-human)', '(-project,+human)'],
        'publication': ['(+ref,-human)', '(-ref,+human)'],
        'student': ['(+human)'],
        'ta': ['(+course,-human)', '(-course,+human)'],
        'taughtby': ['(+course,-human)', '(-course,+human)'],
        'tempadvisedby': ['(+human,-human)', '(-human,+human)'],
        'yearsinprogram': ['(+human,#years)'],
        'kmap1': ['(+key,-human)'],
        'kmap2': ['(+key,-human)'],
        'role': ['(+human,#role)']
    },
    'canc': {
        'atm_as': ['(+drug,+atomid,#atomtype,#charge)', '(+drug,-atomid,#atomtype,#charge)', '(-drug,+atomid,#atomtype,#charge)', '(-drug,+atomid,-atomtype,#charge)', '(-drug,+atomid,#atomtype,-charge)'],
        'atm_ba': ['(+drug,+atomid,#atomtype,#charge)', '(+drug,-atomid,#atomtype,#charge)', '(-drug,+atomid,#atomtype,#charge)', '(-drug,+atomid,-atomtype,#charge)', '(-drug,+atomid,#atomtype,-charge)'],
        'atm_br': ['(+drug,+atomid,#atomtype,#charge)', '(+drug,-atomid,#atomtype,#charge)', '(-drug,+atomid,#atomtype,#charge)', '(-drug,+atomid,-atomtype,#charge)', '(-drug,+atomid,#atomtype,-charge)'],
        'atm_c': ['(+drug,+atomid,#atomtype,#charge)', '(+drug,-atomid,#atomtype,#charge)', '(-drug,+atomid,#atomtype,#charge)', '(-drug,+atomid,-atomtype,#charge)', '(-drug,+atomid,#atomtype,-charge)'],
        'atm_ca': ['(+drug,+atomid,#atomtype,#charge)', '(+drug,-atomid,#atomtype,#charge)', '(-drug,+atomid,#atomtype,#charge)', '(-drug,+atomid,-atomtype,#charge)', '(-drug,+atomid,#atomtype,-charge)'],
        'atm_cl': ['(+drug,+atomid,#atomtype,#charge)', '(+drug,-atomid,#atomtype,#charge)', '(-drug,+atomid,#atomtype,#charge)', '(-drug,+atomid,-atomtype,#charge)', '(-drug,+atomid,#atomtype,-charge)'],
        'atm_cu': ['(+drug,+atomid,#atomtype,#charge)', '(+drug,-atomid,#atomtype,#charge)', '(-drug,+atomid,#atomtype,#charge)', '(-drug,+atomid,-atomtype,#charge)', '(-drug,+atomid,#atomtype,-charge)'],
        'atm_f': ['(+drug,+atomid,#atomtype,#charge)', '(+drug,-atomid,#atomtype,#charge)', '(-drug,+atomid,#atomtype,#charge)', '(-drug,+atomid,-atomtype,#charge)', '(-drug,+atomid,#atomtype,-charge)'],
        'atm_h': ['(+drug,+atomid,#atomtype,#charge)', '(+drug,-atomid,#atomtype,#charge)', '(-drug,+atomid,#atomtype,#charge)', '(-drug,+atomid,-atomtype,#charge)', '(-drug,+atomid,#atomtype,-charge)'],
        'atm_hg': ['(+drug,+atomid,#atomtype,#charge)', '(+drug,-atomid,#atomtype,#charge)', '(-drug,+atomid,#atomtype,#charge)', '(-drug,+atomid,-atomtype,#charge)', '(-drug,+atomid,#atomtype,-charge)'],
        'atm_i': ['(+drug,+atomid,#atomtype,#charge)', '(+drug,-atomid,#atomtype,#charge)', '(-drug,+atomid,#atomtype,#charge)', '(-drug,+atomid,-atomtype,#charge)', '(-drug,+atomid,#atomtype,-charge)'],
        'atm_k': ['(+drug,+atomid,#atomtype,#charge)', '(+drug,-atomid,#atomtype,#charge)', '(-drug,+atomid,#atomtype,#charge)', '(-drug,+atomid,-atomtype,#charge)', '(-drug,+atomid,#atomtype,-charge)'],
        'atm_mn': ['(+drug,+atomid,#atomtype,#charge)', '(+drug,-atomid,#atomtype,#charge)', '(-drug,+atomid,#atomtype,#charge)', '(-drug,+atomid,-atomtype,#charge)', '(-drug,+atomid,#atomtype,-charge)'],
        'atm_n': ['(+drug,+atomid,#atomtype,#charge)', '(+drug,-atomid,#atomtype,#charge)', '(-drug,+atomid,#atomtype,#charge)', '(-drug,+atomid,-atomtype,#charge)', '(-drug,+atomid,#atomtype,-charge)'],
        'atm_na': ['(+drug,+atomid,#atomtype,#charge)', '(+drug,-atomid,#atomtype,#charge)', '(-drug,+atomid,#atomtype,#charge)', '(-drug,+atomid,-atomtype,#charge)', '(-drug,+atomid,#atomtype,-charge)'],
        'atm_o': ['(+drug,+atomid,#atomtype,#charge)', '(+drug,-atomid,#atomtype,#charge)', '(-drug,+atomid,#atomtype,#charge)', '(-drug,+atomid,-atomtype,#charge)', '(-drug,+atomid,#atomtype,-charge)'],
        'atm_p': ['(+drug,+atomid,#atomtype,#charge)', '(+drug,-atomid,#atomtype,#charge)', '(-drug,+atomid,#atomtype,#charge)', '(-drug,+atomid,-atomtype,#charge)', '(-drug,+atomid,#atomtype,-charge)'],
        'atm_pb': ['(+drug,+atomid,#atomtype,#charge)', '(+drug,-atomid,#atomtype,#charge)', '(-drug,+atomid,#atomtype,#charge)', '(-drug,+atomid,-atomtype,#charge)', '(-drug,+atomid,#atomtype,-charge)'],
        'atm_s': ['(+drug,+atomid,#atomtype,#charge)', '(+drug,-atomid,#atomtype,#charge)', '(-drug,+atomid,#atomtype,#charge)', '(-drug,+atomid,-atomtype,#charge)', '(-drug,+atomid,#atomtype,-charge)'],
        'atm_se': ['(+drug,+atomid,#atomtype,#charge)', '(+drug,-atomid,#atomtype,#charge)', '(-drug,+atomid,#atomtype,#charge)', '(-drug,+atomid,-atomtype,#charge)', '(-drug,+atomid,#atomtype,-charge)'],
        'atm_sn': ['(+drug,+atomid,#atomtype,#charge)', '(+drug,-atomid,#atomtype,#charge)', '(-drug,+atomid,#atomtype,#charge)', '(-drug,+atomid,-atomtype,#charge)', '(-drug,+atomid,#atomtype,-charge)'],
        'atm_te': ['(+drug,+atomid,#atomtype,#charge)', '(+drug,-atomid,#atomtype,#charge)', '(-drug,+atomid,#atomtype,#charge)', '(-drug,+atomid,-atomtype,#charge)', '(-drug,+atomid,#atomtype,-charge)'],
        'atm_ti': ['(+drug,+atomid,#atomtype,#charge)', '(+drug,-atomid,#atomtype,#charge)', '(-drug,+atomid,#atomtype,#charge)', '(-drug,+atomid,-atomtype,#charge)', '(-drug,+atomid,#atomtype,-charge)'],
        'atm_zn': ['(+drug,+atomid,#atomtype,#charge)', '(+drug,-atomid,#atomtype,#charge)', '(-drug,+atomid,#atomtype,#charge)', '(-drug,+atomid,-atomtype,#charge)', '(-drug,+atomid,#atomtype,-charge)'],
        'sbond_1': ['(+drug,-atomid,-atomid)', '(+drug,+atomid,-atomid)', '(+drug,-atomid,+atomid)'],
        'sbond_2': ['(+drug,-atomid,-atomid)', '(+drug,+atomid,-atomid)', '(+drug,-atomid,+atomid)'],
        'sbond_3': ['(+drug,-atomid,-atomid)', '(+drug,+atomid,-atomid)', '(+drug,-atomid,+atomid)'],
        'sbond_7': ['(+drug,-atomid,-atomid)', '(+drug,+atomid,-atomid)', '(+drug,-atomid,+atomid)'],
    },
    'canc3': {
        'mol2atm' : ['(+drug,-atomid)'],
        'atm_elem': ['(+atomid,#element)', '(+atomid,-element)'],
        'atm_atomtype': ['(+atomid,#atomtype)', '(+atomid,-atomtype)'],
        'atm_charge': ['(+atomid,#charge)', '(+atomid,-charge)'],
        'sbond_1': ['(+drug,-atomid,-atomid)', '(+drug,+atomid,-atomid)', '(+drug,-atomid,+atomid)'],
        'sbond_2': ['(+drug,-atomid,-atomid)', '(+drug,+atomid,-atomid)', '(+drug,-atomid,+atomid)'],
        'sbond_3': ['(+drug,-atomid,-atomid)', '(+drug,+atomid,-atomid)', '(+drug,-atomid,+atomid)'],
        'sbond_7': ['(+drug,-atomid,-atomid)', '(+drug,+atomid,-atomid)', '(+drug,-atomid,+atomid)']
    },
    'canc2': {
        'atm_as' : ['(+drug,-atomid)'],
        'atm_ba' : ['(+drug,-atomid)'],
        'atm_br' : ['(+drug,-atomid)'],
        'atm_c' : ['(+drug,-atomid)'],
        'atm_ca' : ['(+drug,-atomid)'],
        'atm_cl' : ['(+drug,-atomid)'],
        'atm_cu' : ['(+drug,-atomid)'],
        'atm_f' : ['(+drug,-atomid)'],
        'atm_h': ['(+drug,-atomid)'],
        'atm_hg': ['(+drug,-atomid)'],
        'atm_i' : ['(+drug,-atomid)'],
        'atm_k' : ['(+drug,-atomid)'],
        'atm_mn': ['(+drug,-atomid)'],
        'atm_n': ['(+drug,-atomid)'],
        'atm_na': ['(+drug,-atomid)'],
        'atm_o': ['(+drug,-atomid)'],
        'atm_p': ['(+drug,-atomid)'],
        'atm_pb' : ['(+drug,-atomid)'],
        'atm_s': ['(+drug,-atomid)'],
        'atm_se' : ['(+drug,-atomid)'],
        'atm_sn': ['(+drug,-atomid)'],
        'atm_te': ['(+drug,-atomid)'],
        'atm_ti' : ['(+drug,-atomid)'],
        'atm_zn': ['(+drug,-atomid)'],
        'atm_atype': ['(+atomid,#atomtype)', '(+atomid,-atomtype)'],
        'atm_charge': ['(+atomid,#charge)', '(+atomid,-charge)'],
        'sbond_7_drug0' : ['(-sbondid,+drug)'],
        'sbond_7_atomid1': ['(+sbondid,-atomid)', '(-sbondid,+atomid)'],
        'sbond_7_atomid2': ['(+sbondid,-atomid)', '(-sbondid,+atomid)'],
        'sbond_1_drug0': ['(-sbondid,+drug)'],
        'sbond_1_atomid1': ['(+sbondid,-atomid)', '(-sbondid,+atomid)'],
        'sbond_1_atomid2': ['(+sbondid,-atomid)', '(-sbondid,+atomid)'],
        'sbond_2_drug0': ['(-sbondid,+drug)'],
        'sbond_2_atomid1': ['(+sbondid,-atomid)', '(-sbondid,+atomid)'],
        'sbond_2_atomid2': ['(+sbondid,-atomid)', '(-sbondid,+atomid)'],
        'sbond_3_drug0': ['(-sbondid,+drug)'],
        'sbond_3_atomid1': ['(+sbondid,-atomid)', '(-sbondid,+atomid)'],
        'sbond_3_atomid2': ['(+sbondid,-atomid)', '(-sbondid,+atomid)']
    },
    'cora_reified': {
        'author': ['(+bib,-author)', '(-bib,+author)'],
        'title': ['(+bib,-title)', '(-bib,+title)'],
        'venue': ['(+bib,-venue)', '(-bib,+venue)'],
        'haswordauthor': ['(+author,-word)', '(+author,#word)'],
        'haswordtitle': ['(+title,-word)', '(+title,#word)'],
        'haswordvenue': ['(+venue,-word)', '(+venue,#word)'],
        'sameauthor': ['(+author,-author)', '(-author,+author)'],
        'samebib': ['(+bib,-bib)', '(-bib,+bib)'],
        'samevenue': ['(+venue,-venue)', '(-venue,+venue)'],
        'sametitle': ['(+title,-title)', '(-title,+title)'],
        'kmap': ['(+key,-bib,-bib)']
    },
    'hepatitis': {
        'dbil': ['(+indom,#dbiltype)'],
        'che': ['(+indom,#chetype)'],
        'tp': ['(+indom,#tptype)'],
        'got': ['(+indom,#gottype)'],
        'gpt': ['(+indom,#gpttype)'],
        'ztt': ['(+indom,#ztttype)'],
        'dur': ['(+atype,#durtype)'],
        'ttt': ['(+indom,#ttttype)'],
        'b_rel12': ['(+indom,-mdom)', '(-indom,+mdom)'],
        'b_rel13': ['(+atype,-mdom)', '(-atype,+mdom)'],
        'b_rel11': ['(+bdom,-mdom)', '(-bdom,+mdom)'],
        'tbil': ['(+indom,#tbiltype)'],
        'age': ['(+mdom,#agetype)'],
        'activity': ['(+bdom,#acttype)'],
        'fibros': ['(+bdom,#fibtype)'],
        'alb': ['(+indom,#albtype)'],
        'sex': ['(+mdom,#sextype)'],
        'tcho': ['(+indom,#tchotype)'],
    },
    'terrorists': {
        'colocatedevent': ['(+eventdom,-eventdom)', '(-eventdom,+eventdom)'],
        'haseventfeature': ['(+eventdom,#featuredom)', '(+eventdom,-featuredom)'],
        'performedbysameorg': ['(+eventdom,-eventdom)', '(-eventdom,+eventdom)']
    },
    'yeast': {
        'complex': ['(+gene,#complex)'],
        'enzyme': ['(+gene,#enzyme)'],
        'interaction': ['(+gene,-gene,#intertype)', '(-gene,+gene,#intertype)'],
        'location': ['(+gene,#location)'],
        'path': ['(+gene,-gene)', '(-gene,+gene)'],
        'phenotype': ['(+gene,#phenotype)'],
        'protein_class': ['(+gene,#class)'],
        'rcomplex': ['(+gene,#complex)'],
        'renzyme': ['(+gene,#enzyme)'],
        'rphenotype': ['(+gene,#phenotype)'],
        'rprotein_class': ['(+gene,#class)']
    },
    'yeast2': {
        'complex': ['(+gene,#complex)'],
        'enzyme': ['(+gene,#enzyme)'],
        'interaction': ['(+gene,-gene)', '(-gene,+gene)'],
        'location': ['(+gene,#location)'],
        'path': ['(+gene,-gene)', '(-gene,+gene)'],
        'phenotype': ['(+gene,#phenotype)'],
        'protein_class': ['(+gene,#class)'],
        'rcomplex': ['(+gene,#complex)'],
        'renzyme': ['(+gene,#enzyme)'],
        'rphenotype': ['(+gene,#phenotype)'],
        'rprotein_class': ['(+gene,#class)']
    }
}


def _read_definitions(file):
    defs = {}

    for line in open(file).readlines():
        if len(line) < 3:
            continue

        pred, arg = line.strip().replace(")", "").split("(")
        if ',' not in arg:
            defs[pred] = arg
        else:
            defs[pred] = arg.split(",")

    return defs


def _define_roles(predicate_definitions, predicate_bias):
    predicate_roles = {}
    for pred in predicate_definitions:
        if pred not in predicate_roles:
            predicate_roles[pred] = []
        bias = predicate_bias[pred.lower()]
        for ind, arg in enumerate(predicate_definitions[pred]):
            rroles = [x.split(",")[ind] for x in bias]
            if '#' in ''.join(rroles):
                predicate_roles[pred].append("attribute")
            else:
                predicate_roles[pred].append("name")

    return predicate_roles


STATISTICS = {}


if __name__ == '__main__':
    for data in CLASSIFICATION_DATA:
        print("Processing {} dataset...".format(data))
        if os.path.exists("{}/{}.pkl".format(RESULTS_FOLDER, data + "_uninformed" + str(UNINFORMED_GRAPH))):
            print("      found results")
            continue

        intermediate_results = {}
        folds = [x for x in os.listdir(RELATIONAL_DATA_ROOT + "/" + data) if x.endswith(".db")]
        STATISTICS[data] = {}

        for fold in folds:

            STATISTICS[data][fold] = {}
            STATISTICS[data][fold]['components'] = {}
            STATISTICS[data][fold]['component_size'] = {}
            STATISTICS[data][fold]['num_components'] = []

            STATISTICS[data][fold]['components']['comp_1'] = {
                'node_connectivity': [],
                'average_clustering': [],
                'degree_assortativity_coefficient': [],
                'degree_pearson_correlation_coefficient': [],
                'average_neighbor_degree': [],
                'degree_centrality': [],
                'closeness_centrality': [],
                'information_centrality': [],
                'diameter': [],
                'eccentricity': [],
                'radius': [],
                'graph_clique_number': [],
                'graph_number_of_cliques': [],
                'square_clustering': [],
                'edge_connectivity': []
            }

            print("     fold " + fold)
            uninformed_graph = nx.Graph()
            predicate_definitions = _read_definitions("{}/{}/predicate.defs".format(RELATIONAL_DATA_ROOT, data))
            predicate_roles = _define_roles(predicate_definitions, PREDICATE_BIAS[data])

            nodes = set()

            for line in open("{}/{}/{}".format(RELATIONAL_DATA_ROOT, data, fold)).readlines():
                if len(line) < 3:
                    continue
                else:
                    pred, args = line.strip().replace(")", "").split("(")
                    domains = predicate_definitions[pred]
                    if ',' not in args:
                        if args not in nodes:
                            uninformed_graph.add_node(args, type=domains[0])
                            nodes.add(args)
                        uninformed_graph[args][pred] = True
                        continue

                    args = args.split(",")
                    roles = predicate_roles[pred]

                    if UNINFORMED_GRAPH:
                        for ind, e in enumerate(args):
                            if e not in nodes:
                                uninformed_graph.add_node(e, type=domains[ind])
                                nodes.add(e)
                        uninformed_graph.add_edge(*args, label=pred)
                    else:
                        attrs = [(args[i], domains[i]) for i in range(len(args)) if roles[i] == 'attribute']
                        entity = [(args[i], domains[i]) for i in range(len(args)) if roles[i] != 'attribute']

                        if len(attrs) == 0:
                            for ind, e in enumerate(args):
                                if e not in nodes:
                                    uninformed_graph.add_node(e, type=domains[ind])
                                    nodes.add(e)
                            uninformed_graph.add_edge(*args)
                        elif len(entity) == 1:
                            entity = entity[0]
                            if entity[0] not in nodes:
                                uninformed_graph.add_node(entity[0], type=entity[1])
                                nodes.add(entity[0])

                            for attr in attrs:
                                uninformed_graph.nodes[entity[0]][attr[1]] = attr[0]
                        else:
                            raise Exception("no attributes on edges")

            if nx.algorithms.components.is_connected(uninformed_graph):
                STATISTICS[data][fold]['components']['comp_1'] = {}
                STATISTICS[data][fold]['num_components'] = [1]
                # STATISTICS[data][fold]['component_size'] = [len(uninformed_graph.nodes)]
                STATISTICS[data][fold]['components']['comp_1'] = {
                    'node_connectivity': [],
                    'average_clustering': [],
                    'degree_assortativity_coefficient': [],
                    'degree_pearson_correlation_coefficient': [],
                    'average_neighbor_degree': [],
                    'degree_centrality': [],
                    'closeness_centrality': [],
                    'information_centrality': [],
                    'diameter': [],
                    'eccentricity': [],
                    'radius': [],
                    'graph_clique_number': [],
                    'graph_number_of_cliques': [],
                    'square_clustering': [],
                    'edge_connectivity': [],
                    'degree': [],
                    'selfloops': [],
                    'size': [],
                    #'ramsey': [],
                    'average_degree_connectivity': [],
                    # 'number_of_bridges': [],
                    # 'eigenvector_centrality': [],
                    # 'katz_centrality': [],
                    # 'communicability_betweenness_centrality': [],
                    # 'load_centrality': [],
                    # 'percolation_centrality': [],
                    # 'local_efficiency': []
                }

                STATISTICS[data][fold]['components']['comp_1']['size'].append(len(list(uninformed_graph.nodes)))
                STATISTICS[data][fold]['components']['comp_1']['node_connectivity'].append(approx.node_connectivity(uninformed_graph))
                STATISTICS[data][fold]['components']['comp_1']['average_clustering'].append(approx.average_clustering(uninformed_graph))
                STATISTICS[data][fold]['components']['comp_1']['degree_assortativity_coefficient'].append(
                    nx.algorithms.assortativity.degree_assortativity_coefficient(uninformed_graph))
                STATISTICS[data][fold]['components']['comp_1']['degree_pearson_correlation_coefficient'].append(
                    nx.algorithms.assortativity.degree_pearson_correlation_coefficient(uninformed_graph))
                STATISTICS[data][fold]['components']['comp_1']['average_neighbor_degree'] += nx.average_neighbor_degree(uninformed_graph).values()
                STATISTICS[data][fold]['components']['comp_1']['degree_centrality'] += nx.algorithms.centrality.degree_centrality(uninformed_graph).values()
                STATISTICS[data][fold]['components']['comp_1']['closeness_centrality'] += nx.algorithms.centrality.closeness_centrality(
                    uninformed_graph).values()
                # STATISTICS[data]['information_centrality'] += nx.algorithms.centrality.information_centrality(
                #    uninformed_graph).values()
                try:
                    STATISTICS[data][fold]['components']['comp_1']['diameter'].append(nx.algorithms.distance_measures.diameter(uninformed_graph))
                except Exception:
                    STATISTICS[data][fold]['components']['comp_1']['diameter'].append(np.NaN)
                try:
                    STATISTICS[data][fold]['components']['comp_1']['eccentricity'] += nx.algorithms.distance_measures.eccentricity(uninformed_graph).values()
                except Exception:
                    STATISTICS[data][fold]['components']['comp_1']['eccentricity'] += [np.NaN]
                try:
                    STATISTICS[data][fold]['components']['comp_1']['radius'].append(nx.algorithms.distance_measures.radius(uninformed_graph))
                except Exception:
                    STATISTICS[data]['radius'].append(np.NaN)
                STATISTICS[data][fold]['components']['comp_1']['graph_clique_number'].append(nx.algorithms.clique.graph_clique_number(uninformed_graph))
                STATISTICS[data][fold]['components']['comp_1']['graph_number_of_cliques'].append(nx.algorithms.clique.graph_number_of_cliques(uninformed_graph))
                STATISTICS[data][fold]['components']['comp_1']['square_clustering'] += nx.algorithms.cluster.square_clustering(uninformed_graph).values()
                STATISTICS[data][fold]['components']['comp_1']['edge_connectivity'].append(nx.algorithms.connectivity.connectivity.edge_connectivity(uninformed_graph))
                STATISTICS[data][fold]['components']['comp_1']['degree'] += [ val for (node, val) in uninformed_graph.degree()]
                STATISTICS[data][fold]['components']['comp_1']['selfloops'].append(uninformed_graph.number_of_selfloops())
                #STATISTICS[data][fold]['components']['comp_1']['selfloops'] += approx.ramsey.ramsey_R2(uninformed_graph)


                try:
                    STATISTICS[data][fold]['components']['comp_1'][
                        'average_degree_connectivity'] += nx.algorithms.assortativity.average_degree_connectivity(
                        uninformed_graph).values()
                except Exception:
                    STATISTICS[data][fold]['components']['comp_1']['average_degree_connectivity'] += [np.NaN]
                """
                STATISTICS[data][fold]['components']['comp_1']['number_of_bridges'].append(len(nx.algorithms.bridges.bridges(uninformed_graph)))

                try:
                    STATISTICS[data][fold]['components']['comp_1'][
                        'eigenvector_centrality'] += nx.algorithms.centrality.eigenvector_centrality(
                        uninformed_graph).values()
                except Exception:
                    STATISTICS[data][fold]['components']['comp_1']['eigenvector_centrality'] += [np.NaN]

                try:
                    STATISTICS[data][fold]['components']['comp_1'][
                        'katz_centrality'] += nx.algorithms.centrality.katz_centrality(
                        uninformed_graph).values()
                except Exception:
                    STATISTICS[data][fold]['components']['comp_1']['katz_centrality'] += [np.NaN]

                try:
                    STATISTICS[data][fold]['components']['comp_1'][
                        'communicability_betweenness_centrality'] += nx.algorithms.centrality.communicability_betweenness_centrality(
                        uninformed_graph).values()
                except Exception:
                    STATISTICS[data][fold]['components']['comp_1']['communicability_betweenness_centrality'] += [np.NaN]

                try:
                    STATISTICS[data][fold]['components']['comp_1'][
                        'load_centrality'] += nx.algorithms.centrality.load_centrality(
                        uninformed_graph).values()
                except Exception:
                    STATISTICS[data][fold]['components']['comp_1']['load_centrality'] += [np.NaN]

                try:
                    STATISTICS[data][fold]['components']['comp_1'][
                        'percolation_centrality'] += nx.algorithms.centrality.percolation_centrality(
                        uninformed_graph).values()
                except Exception:
                    STATISTICS[data][fold]['components']['comp_1']['percolation_centrality'] += [np.NaN]

                try:
                    STATISTICS[data][fold]['components']['comp_1']['local_efficiency'].append(nx.algorithms.efficiency.local_efficiency(uninformed_graph))
                except Exception:
                    STATISTICS[data][fold]['components']['comp_1']['local_efficiency'].append(np.NaN)
                """

            else:
                subgraphs = [uninformed_graph.subgraph(c) for c in nx.connected_components(uninformed_graph)]
                component_subgraph = 1
                STATISTICS[data][fold]['num_components'].append(len(subgraphs))
                #STATISTICS[data][fold]['component_size'] = []

                for sg in subgraphs:
                    component_id = "comp_" + str(component_subgraph)
                    component_subgraph += 1
                    STATISTICS[data][fold]['components'][component_id] = {}
                    # STATISTICS[data][fold]['component_size'].append(len(sg.nodes))

                    if len(list(sg.nodes)) == 1:
                        continue

                    STATISTICS[data][fold]['components'][component_id] = {
                        'node_connectivity': [],
                        'average_clustering': [],
                        'degree_assortativity_coefficient': [],
                        'degree_pearson_correlation_coefficient': [],
                        'average_neighbor_degree': [],
                        'degree_centrality': [],
                        'closeness_centrality': [],
                        'information_centrality': [],
                        'diameter': [],
                        'eccentricity': [],
                        'radius': [],
                        'graph_clique_number': [],
                        'graph_number_of_cliques': [],
                        'square_clustering': [],
                        'edge_connectivity': [],
                        'degree': [],
                        'selfloops': [],
                        'size': [],
                        # 'ramsey': [],
                        'average_degree_connectivity': [],
                        # 'number_of_bridges': [],
                        # 'eigenvector_centrality': [],
                        # 'katz_centrality': [],
                        # 'communicability_betweenness_centrality': [],
                        # 'load_centrality': [],
                        # 'percolation_centrality': [],
                        # 'node_clustering': [],
                        # 'local_efficiency': []
                    }

                    STATISTICS[data][fold]['components'][component_id]['size'] = [len(list(sg.nodes))]
                    STATISTICS[data][fold]['components'][component_id]['node_connectivity'].append(approx.node_connectivity(sg))
                    STATISTICS[data][fold]['components'][component_id]['average_clustering'].append(approx.average_clustering(sg))
                    try:
                        STATISTICS[data][fold]['components'][component_id]['degree_assortativity_coefficient'].append(
                            nx.algorithms.assortativity.degree_assortativity_coefficient(sg))
                    except Exception:
                        STATISTICS[data][fold]['components'][component_id]['degree_assortativity_coefficient'].append(np.NaN)
                    try:
                        STATISTICS[data][fold]['components'][component_id]['degree_pearson_correlation_coefficient'].append(
                            nx.algorithms.assortativity.degree_pearson_correlation_coefficient(sg))
                    except Exception:
                        STATISTICS[data][fold]['components'][component_id]['degree_pearson_correlation_coefficient'].append(np.NaN)
                    STATISTICS[data][fold]['components'][component_id]['average_neighbor_degree'] += nx.average_neighbor_degree(sg).values()
                    try:
                        STATISTICS[data][fold]['components'][component_id]['degree_centrality'] += nx.algorithms.centrality.degree_centrality(
                            sg).values()
                    except Exception:
                        STATISTICS[data][fold]['components'][component_id]['degree_centrality'] += [np.NaN]
                    try:
                        STATISTICS[data][fold]['components'][component_id]['closeness_centrality'] += nx.algorithms.centrality.closeness_centrality(
                            sg).values()
                    except:
                        STATISTICS[data][fold]['components'][component_id]['closeness_centrality'] += [np.NaN]
                    try:
                        STATISTICS[data][fold]['components'][component_id]['information_centrality'] += nx.algorithms.centrality.information_centrality(
                            sg).values()
                    except Exception:
                        STATISTICS[data][fold]['components'][component_id]['information_centrality'] += [np.NaN]
                    try:
                        STATISTICS[data][fold]['components'][component_id]['diameter'].append(nx.algorithms.distance_measures.diameter(sg))
                    except Exception:
                        STATISTICS[data][fold]['components'][component_id]['diameter'].append(np.NaN)
                    try:
                        STATISTICS[data][fold]['components'][component_id]['eccentricity'] += nx.algorithms.distance_measures.eccentricity(
                            sg).values()
                    except Exception:
                        STATISTICS[data][fold]['components'][component_id]['eccentricity'] += [np.NaN]
                    try:
                        STATISTICS[data][fold]['components'][component_id]['radius'].append(nx.algorithms.distance_measures.radius(sg))
                    except Exception:
                        STATISTICS[data][fold]['components'][component_id]['radius'].append(np.NaN)
                    try:
                        STATISTICS[data][fold]['components'][component_id]['graph_clique_number'].append(
                            nx.algorithms.clique.graph_clique_number(sg))
                    except Exception:
                        STATISTICS[data][fold]['components'][component_id]['graph_clique_number'].append(np.NaN)
                    STATISTICS[data][fold]['components'][component_id]['graph_number_of_cliques'].append(
                        nx.algorithms.clique.graph_number_of_cliques(sg))
                    try:
                        STATISTICS[data][fold]['components'][component_id]['square_clustering'] += nx.algorithms.cluster.square_clustering(
                            sg).values()
                    except Exception:
                        STATISTICS[data][fold]['components'][component_id]['square_clustering'] += [np.NaN]
                    try:
                        STATISTICS[data][fold]['components'][component_id]['edge_connectivity'].append(
                            nx.algorithms.connectivity.connectivity.edge_connectivity(sg))
                    except Exception:
                        STATISTICS[data][fold]['components'][component_id]['edge_connectivity'].append(np.NaN)

                    try:
                        STATISTICS[data][fold]['components'][component_id]['average_degree_connectivity'] += nx.algorithms.assortativity.average_degree_connectivity(
                            sg).values()
                    except Exception:
                        STATISTICS[data][fold]['components'][component_id]['average_degree_connectivity'] += [np.NaN]

                    STATISTICS[data][fold]['components'][component_id]['degree'] += [val for (node, val) in sg.degree()]
                    STATISTICS[data][fold]['components'][component_id]['selfloops'].append(sg.number_of_selfloops())
                    # STATISTICS[data][fold]['components'][component_id]['ramsey'] += approx.ramsey.ramsey_R2(sg)
                    #STATISTICS[data][fold]['components'][component_id]['number_of_bridges'].append(len(nx.algorithms.bridges.bridges(sg)))
                    """
                    try:
                        STATISTICS[data][fold]['components'][component_id]['eigenvector_centrality'] += nx.algorithms.centrality.eigenvector_centrality(
                            sg).values()
                    except Exception:
                        STATISTICS[data][fold]['components'][component_id]['eigenvector_centrality'] += [np.NaN]

                    try:
                        STATISTICS[data][fold]['components'][component_id]['katz_centrality'] += nx.algorithms.centrality.katz_centrality(
                            sg).values()
                    except Exception:
                        STATISTICS[data][fold]['components'][component_id]['katz_centrality'] += [np.NaN]


                    try:
                        STATISTICS[data][fold]['components'][component_id]['communicability_betweenness_centrality'] += nx.algorithms.centrality.communicability_betweenness_centrality(
                            sg).values()
                    except Exception:
                        STATISTICS[data][fold]['components'][component_id]['communicability_betweenness_centrality'] += [np.NaN]

                    try:
                        STATISTICS[data][fold]['components'][component_id]['load_centrality'] += nx.algorithms.centrality.load_centrality(
                            sg).values()
                    except Exception:
                        STATISTICS[data][fold]['components'][component_id]['load_centrality'] += [np.NaN]

                    try:
                        STATISTICS[data][fold]['components'][component_id]['percolation_centrality'] += nx.algorithms.centrality.percolation_centrality(
                            sg).values()
                    except Exception:
                        STATISTICS[data][fold]['components'][component_id]['percolation_centrality'] += [np.NaN]

                    try:
                        STATISTICS[data][fold]['components'][component_id]['node_clustering'] += nx.algorithms.cluster.clustering(
                            sg).values()
                    except Exception:
                        STATISTICS[data][fold]['components'][component_id]['pnode_clustering'] += [np.NaN]

                    try:
                        STATISTICS[data][fold]['components'][component_id]['local_efficiency'].append(
                            nx.algorithms.efficiency.local_efficiency(sg))
                    except Exception:
                        STATISTICS[data][fold]['components'][component_id]['local_efficiency'].append(np.NaN)
                    """

        result_save = open("{}/{}.pkl".format(RESULTS_FOLDER, data + "_uninformed" + str(UNINFORMED_GRAPH)), 'wb')
        pickle.dump(STATISTICS, result_save)
        result_save.close()

    STATISTICS = {}

    for data in CLASSIFICATION_DATA:
        resfile = open("{}/{}.pkl".format(RESULTS_FOLDER, data + "_uninformed" + str(UNINFORMED_GRAPH)), 'rb')
        STATISTICS[data] = {}
        res = pickle.load(resfile)
        STATISTICS[data] = res[data]

    print("{:40s} | {:40s}".format('statistics', 'dataset'))
    print("{:40s} | ".format("") + ' '.join(["{:15s}".format(data) for data in CLASSIFICATION_DATA]))
    print("-"*70)

    global_data_statistics = {}

    for data in CLASSIFICATION_DATA:
        global_data_statistics[data] = {}

        for fold in STATISTICS[data]:
            fold_statistics = {}
            for comp in STATISTICS[data][fold]['components']:
                for measure in STATISTICS[data][fold]['components'][comp]:
                    if measure not in fold_statistics:
                        fold_statistics[measure] = []

                    if len(STATISTICS[data][fold]['components'][comp]['average_neighbor_degree']) < 2:
                        continue

                    if AVERAGING_TYPE == 1 or AVERAGING_TYPE == 2:
                        if isinstance(STATISTICS[data][fold]['components'][comp][measure], list):
                            fold_statistics[measure] += [x for x in STATISTICS[data][fold]['components'][comp][measure] if not math.isnan(x)] if FILTER_NANS else STATISTICS[data][fold]['components'][comp][measure]
                        else:
                            if FILTER_NANS and math.isnan(STATISTICS[data][fold]['components'][comp][measure]):
                                pass
                            else:
                                fold_statistics[measure].append(STATISTICS[data][fold]['components'][comp][measure])
                    elif AVERAGING_TYPE == 3 or AVERAGING_TYPE == 4:
                        if FILTER_NANS:
                            fold_statistics[measure].append(
                                np.mean(np.array([x for x in STATISTICS[data][fold]['components'][comp][measure] if not math.isnan(x)]).astype(np.float)))
                        else:
                            fold_statistics[measure].append(np.array(STATISTICS[data][fold]['components'][comp][measure]).astype(np.float).mean())

            for measure in fold_statistics:
                if measure not in global_data_statistics[data]:
                    global_data_statistics[data][measure] = []

                if AVERAGING_TYPE == 1 or AVERAGING_TYPE == 2:
                    global_data_statistics[data][measure] += fold_statistics[measure]
                else:
                    global_data_statistics[data][measure].append(np.array(fold_statistics[measure]).mean())

            for item in ['num_components']:
                if item not in global_data_statistics[data]:
                    global_data_statistics[data][item] = []
                global_data_statistics[data][item].append(np.array(STATISTICS[data][fold][item]).mean())

            # if 'unit-components' not in global_data_statistics[data]:
            #     global_data_statistics[data]['unit-components'] = []
            # global_data_statistics[data]['unit-components'].append(len([x for x in STATISTICS[data][fold]['component_size'] if x < 2]))

    for measure in global_data_statistics[CLASSIFICATION_DATA[0]]:
        if FILTER_NANS:
            print("{:40s}".format(measure) + " | ", end='')
            print(" ".join(
                ["{:7.2f}({:.2f})".format(np.mean(np.array([x for x in global_data_statistics[data][measure] if x not in [np.NaN, np.inf, np.infty]]).astype(np.float)),
                                          np.std(np.array([x for x in global_data_statistics[data][measure] if x not in [np.NaN, np.inf, np.infty]]).astype(np.float))) for data in
                 CLASSIFICATION_DATA]))
        else:
            print("{:40s}".format(measure) + " | ", end='')
            print(" ".join(
                ["{:7.2f}({:.2f})".format(np.mean(np.array(global_data_statistics[data][measure]).astype(np.float)),
                                          np.std(np.array(global_data_statistics[data][measure]).astype(np.float))) for data in CLASSIFICATION_DATA]))














