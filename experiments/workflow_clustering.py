import os
from learners.LearnerSettings import LearnerSettings
from learners.PropositionalClustering import Hierarchical, Spectral
import altair as alt
import pickle


def get_k(dataset):
    if dataset in ['imdb', 'uwcse', 'mutagenesis']:
        return 2
    elif dataset == 'webkb':
        return 7
    elif dataset == 'terrorists':
        return 6
    else:
        print("UNKNOWN DATASET {}!!!!".format(dataset))


def clustering_factory(algo, params):
    if algo == 'Spectral':
        return Spectral(params)
    else:
        return Hierarchical(params)


DATA_ROOT = os.path.dirname(__file__) + "/../data/clustering/3_embeddings"
CLUSTERING_ALGO = ['Spectral', 'Hierarchical']
CLUSTERING_DATA = ['imdb', 'uwcse', 'terrorists', 'mutagenesis', 'webkb']
DISTANCE_METRICS_HIERARCHICAL = ['euclidean', 'l1', 'l2', 'manhattan', 'cosine', 'cityblock', 'braycurtis', 'canberra',
                                 'chebyshev', 'correlation', 'dice', 'hamming', 'jaccard', 'kulsinski', 'mahalanobis',
                                 'matching', 'minkowski', 'rogerstanimoto', 'russellrao', 'seuclidean',
                                 'sokalmichener', 'sokalsneath', 'sqeuclidean', 'precomputed']
DISTANCE_METRICS_SPECTRAL = ['rbf', 'nearest_neighbors', 'poly', 'sigmoid', 'laplacian', 'precomputed'] # 'chi2', 'cosine', 'linear' - no negatives

EMBEDDING_METHODS = ['transE', 'DistMult']

EMBEDDING_DIMENSION = [10, 20, 30, 50, 80, 100]

EMBEDDING_EPOCHS = [20, 40, 60, 80, 100]

if __name__ == '__main__':
    results = []
    for data in CLUSTERING_DATA:
        print("Processing {} dataset...".format(data))
        parameters = LearnerSettings()
        parameters.add_parameter("n_clusters", get_k(data))

        for emb_method in EMBEDDING_METHODS:
            print("  with {} embedding".format(emb_method))

            for emb_dim in EMBEDDING_DIMENSION:
                print("    with dimension " + str(emb_dim))

                for emb_epochs in EMBEDDING_EPOCHS:
                    print("      and {} epochs".format(emb_epochs))

                    for algo in CLUSTERING_ALGO:
                        print("        with {} clustering".format(algo))

                        EMBEDDINGS_FILE = DATA_ROOT + "/" + data + "/{}_dim{}_epoch{}.entity.emb".format(emb_method,
                                                                                                         emb_dim,
                                                                                                         emb_epochs)

                        EMBEDDINGS_RELATION_FILE = DATA_ROOT + "/" + data + "/{}_dim{}_epoch{}.relation.emb".format(emb_method,
                                                                                                                    emb_dim,
                                                                                                                    emb_epochs)

                        LABEL_FILE = DATA_ROOT + "/" + data + "/train.labels"

                        if algo == 'Hierarchical':
                            for distance in DISTANCE_METRICS_HIERARCHICAL:
                                print("          with {} distance".format(distance))
                                parameters.add_parameter("hierarchical_distance", distance)
                                parameters.add_parameter('embedding_method', emb_method)
                                clusterer = clustering_factory(algo, parameters)

                                if distance == 'precomputed':
                                    clusterer.read_relation_embedding(EMBEDDINGS_RELATION_FILE)

                                ari = clusterer.learn_and_evaluate(EMBEDDINGS_FILE, LABEL_FILE)

                                results.append({'algo': algo,
                                                'dataset': data,
                                                'ari': ari,
                                                'embedding': emb_method,
                                                'dimension': emb_dim,
                                                'epochs': emb_epochs,
                                                'distance': distance})
                        elif algo == 'Spectral':
                            for distance in DISTANCE_METRICS_SPECTRAL:
                                print("          with {} distance".format(distance))
                                parameters.add_parameter("spectral_affinity", distance)
                                parameters.add_parameter('embedding_method', emb_method)
                                clusterer = clustering_factory(algo, parameters)

                                if distance == 'precomputed':
                                    clusterer.read_relation_embedding(EMBEDDINGS_RELATION_FILE)

                                ari = clusterer.learn_and_evaluate(EMBEDDINGS_FILE, LABEL_FILE)
                                results.append({'algo': algo,
                                                'dataset': data,
                                                'ari': ari,
                                                'embedding': emb_method,
                                                'dimension': emb_dim,
                                                'epochs': emb_epochs,
                                                'distance': distance})

    results.append({'algo': 'Hierarchical',
                    'dataset': 'imdb',
                    'ari': 0.62,
                    'embedding': 'relational',
                    'dimension': 0,
                    'epochs': 0,
                    'distance': 'ReCeNT'})

    results.append({'algo': 'Hierarchical',
                    'dataset': 'uwcse',
                    'ari': 0.97,
                    'embedding': 'relational',
                    'dimension': 0,
                    'epochs': 0,
                    'distance': 'ReCeNT'})

    results.append({'algo': 'Hierarchical',
                    'dataset': 'mutagenesis',
                    'ari': 0.32,
                    'embedding': 'relational',
                    'dimension': 0,
                    'epochs': 0,
                    'distance': 'ReCeNT'})

    results.append({'algo': 'Hierarchical',
                    'dataset': 'webkb',
                    'ari': 0.04,
                    'embedding': 'relational',
                    'dimension': 0,
                    'epochs': 0,
                    'distance': 'ReCeNT'})

    results.append({'algo': 'Hierarchical',
                    'dataset': 'terrorists',
                    'ari': 0.00,
                    'embedding': 'relational',
                    'dimension': 0,
                    'epochs': 0,
                    'distance': 'ReCeNT'})

    results.append({'algo': 'Spectral',
                    'dataset': 'imdb',
                    'ari': 1.0,
                    'embedding': 'relational',
                    'dimension': 0,
                    'epochs': 0,
                    'distance': 'ReCeNT'})

    results.append({'algo': 'Spectral',
                    'dataset': 'uwcse',
                    'ari': 0.98,
                    'embedding': 'relational',
                    'dimension': 0,
                    'epochs': 0,
                    'distance': 'ReCeNT'})

    results.append({'algo': 'Spectral',
                    'dataset': 'mutagenesis',
                    'ari': 0.35,
                    'embedding': 'relational',
                    'dimension': 0,
                    'epochs': 0,
                    'distance': 'ReCeNT'})

    results.append({'algo': 'Spectral',
                    'dataset': 'webkb',
                    'ari': 0.57,
                    'embedding': 'relational',
                    'dimension': 0,
                    'epochs': 0,
                    'distance': 'ReCeNT'})

    results.append({'algo': 'Spectral',
                    'dataset': 'terrorists',
                    'ari': 0.26,
                    'embedding': 'relational',
                    'dimension': 0,
                    'epochs': 0,
                    'distance': 'ReCeNT'})

    data = alt.Data(values=results)

    chart = alt.Chart(data).mark_bar().encode(
        x='algo:N',
        y='max(ari):Q',
        color='algo:N',
        column='dataset:N',
        row='embedding:N'
    )
    chart.savechart(os.path.dirname(__file__) + "/../results/clustering.json")

    pf = open(os.path.dirname(__file__) + "/../results/clustering.pkl", 'wb')
    pickle.dump(results, pf)
    pf.close()

    print("DONE!")
