

def build_graph(lines_train):

    # stores all the vertices of the graph (with unique 0 based index)
    vertices = set()
    # stores all the relations of the graph (with unique 0 based index)
    relations = set()

    # iterate over all lines in the file and fill the sets
    for edge in lines_train:
    	vertices.add(edge[0])
    	vertices.add(edge[2])
    	relations.add(edge[1])
	
    # turn the sets into lists
    vertices = list(vertices)
    relations = list(relations)
    
    # index from the dataset's vertex identifier to the graph-tool vertex number
    vertex_indexer = dict((vt, i) for i, vt in enumerate(vertices))
    # indexer of the relations
    relation_indexer = dict((vt, i) for i, vt in enumerate(relations))

    return vertices, relations, vertex_indexer, relation_indexer
