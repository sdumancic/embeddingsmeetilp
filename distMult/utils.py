import numpy as np
import random
import keras.backend as K
from keras.engine.topology import Layer
from scipy import sparse


class ZeroMasked(Layer):
    def __init__(self, **kwargs):
        self.supports_masking = True
        super(ZeroMasked, self).__init__(**kwargs)

    def call(self, x, mask=None):
        out = K.switch(mask[:, :, np.newaxis], x, 0)
        return K.cast(out, x.dtype)

    def compute_output_shape(self, input_shape):
        return input_shape

    def compute_mask(self, inputs, mask):
        return None


def load_txt_file(filename):
    lines = []
    with open(filename, 'r') as f:
	for line in f:
	    args = line.split('\t')
	    args[-1] = args[-1][:-1]
    	    lines.append(args)
    return lines

# functions to save and load sparse matrices
def save_sparse_csr(filename,array):
    np.savez(filename, data=array.data, indices=array.indices, indptr=array.indptr, shape=array.shape)

def load_sparse_csr(filename):
    loader = np.load(filename)
    return sparse.csr_matrix((loader['data'], loader['indices'], loader['indptr']), shape = loader['shape'])


# function to create a dictionary of true triples
def dict_trueTriples(lines_train, lines_valid, lines_test, vertex_indexer, relation_indexer):
    is_correct_triple = dict()

    # iterate over all the lines in the FB text file
    for edge_filter in lines_valid:
	entry = tuple(elem for elem in edge_filter)
        is_correct_triple[entry] = True

    # iterate over all the lines in the FB text file
    for edge_filter in lines_test:
        entry = tuple(elem for elem in edge_filter)
        is_correct_triple[entry] = True

    # iterate over all the lines in the FB text file
    for edge_filter in lines_train:
        entry = tuple(elem for elem in edge_filter)
        is_correct_triple[entry] = True

    return is_correct_triple

