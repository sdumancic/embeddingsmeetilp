import sys
import os
import signal

import copy
import time
import itertools
import abc

import pickle
import cPickle

import numpy as np
from scipy import sparse
from scipy import stats

import h5py

import random
import math

from keras.models import Sequential, Model
from keras.layers import Input, Masking, Embedding, LSTM
from keras.layers.core import Dense, Dropout, Activation, Reshape, Lambda
from keras.layers.merge import Concatenate, Multiply, Add, Dot
from keras.optimizers import SGD, Adadelta, RMSprop, Adam, Adagrad
from keras.utils import np_utils
import keras.backend as K

from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import MinMaxScaler
from sklearn.cross_validation import train_test_split
from sklearn import linear_model, grid_search, metrics
from sklearn import cross_validation
from keras.constraints import unit_norm

from utils import *
from graph_related import *
import numpy as np

# # todo handle imports properly
sys.path.append('../')

from keras.layers.merge import _Merge

folder = '/media/dataExt1/embeddingsmeetilp/data/clustering/2_pre-embeddings/'
#folder ='/media/dataExt1/embeddingsmeetilp/data/classification/3_pre-embeddings/'
dataset = 'uwcse' # imdb mutagenesis terrorists uwcse webkb genes/yeast.{1-4}.db hepatitis/hep.{1-5}.db
store_in = 'results/' + dataset

# Load files
train_file_path =  folder + dataset + '/train.txt'
#valid_file_path = '../data/yago15kT/yago15k_valid_noNumEnt_fixed.txt'
#test_file_path = '../data/yago15kT/yago15k_test_noNumEnt_fixed.txt'

lines_train = load_txt_file(train_file_path)
#lines_valid = load_txt_file(valid_file_path)
#lines_test = load_txt_file(test_file_path)


# build the graph with graph_tool
vertices, relations, vertex_indexer, relation_indexer = build_graph(lines_train)

idx2ent = {}
for ent in vertex_indexer.keys():
    idx = vertex_indexer[ent]
    idx2ent[idx] = ent

# dictionary with triples that are also correct
#is_correct_triple = dict_trueTriples(lines_train, lines_valid, lines_test, vertex_indexer, relation_indexer)

# number of entities
numEnt = len(vertex_indexer)
numRel = len(relation_indexer)

drop = 0.0
embedding_dim = 100
n_epochs = 100
num_negative = numEnt / 50
batch_size = 256
triples = []
for line in lines_train:
    head = vertex_indexer[line[0]]
    tail = vertex_indexer[line[2]]
    rel = [relation_indexer[line[1]]]
    triples.append([head, rel, tail])

# REINCORPORATE SAMPLING MECHANISM

# distMult model
e1 = Input(shape=(num_negative + 1,), name="e1")
e2 = Input(shape=(1,), name="e2")

rel = Input(shape=(1,), name="rel")

# Normalize entities embeddings ? 
ent_embedding = Embedding(numEnt, embedding_dim, name='ent_embeddings')
rel_embedding = Embedding(numRel, embedding_dim, name='rel_embeddings')

e1_emb = Dropout(drop)(ent_embedding(e1))
e2_emb = Dropout(drop)(ent_embedding(e2))

rel_emb = Dropout(drop)(rel_embedding(rel))

state_emb = Reshape((embedding_dim,))(rel_emb)

joint_emb = Multiply(name='joint_emb')([state_emb, e2_emb])
score = Dot(2, name='score')([joint_emb, e1_emb])

score = Activation('softmax')(Reshape((num_negative + 1,), name='soft_out')(score))
adam = Adam()

model = Model(input=[e1, e2, rel], output=[score])
model.compile(loss='categorical_crossentropy', optimizer=adam)
print model.summary()

score_fn = K.function([model.get_layer("e2").input, model.get_layer("rel").input, K.learning_phase()], [model.get_layer("joint_emb").output])
soft_out_fn = K.function([model.get_layer("e1").input, model.get_layer("e2").input, model.get_layer("rel").input, K.learning_phase()], [model.get_layer("soft_out").output])

def generator_distM(batchsize, triples_training, vertex_values):
    batches = int(len(triples_training)/batchsize)
    while True:
	np.random.shuffle(triples_training)
	for idx in xrange(0, batches):
	    e1_train = []
	    e2_train = []
	    rel_train = []

            if idx == batches - 1:
		triples_batch = triples_training[idx*batchsize:]
            else:
		triples_batch = triples_training[idx*batchsize:(idx+1)*batchsize]

            for tri in triples_batch:
                head_cand = [tri[0]] + list(np.random.choice(list(vertex_values-set([tri[0]])), num_negative))
                tail_cand = [tri[2]] + list(np.random.choice(list(vertex_values-set([tri[2]])), num_negative))
                e1_train.append(head_cand)
                e1_train.append(tail_cand)
                e2_train.append([tri[2]])
                e2_train.append([tri[0]])

		rel_train.append(tri[1])
		rel_train.append(tri[1])

	    e1_train = np.array(e1_train)
	    e2_train = np.array(e2_train)
	    rel_train = np.array(rel_train)
	    num_triples = e1_train.shape[0]
	    Y = [0]*(num_negative + 1)
	    Y[0] = 1
	    Y = np.array([Y]*num_triples)
            yield ([e1_train, e2_train, rel_train], Y)
	
vertex_values = set(vertex_indexer.values())
generator_token = generator_distM(batch_size, triples, vertex_values)
steps_per_epoch = int(len(triples) / batch_size)

bestMRR = -1
best_ranks = []
for epoch in range(n_epochs):
    print 'Epoch %s'%(epoch)
    model.fit_generator(generator_token, steps_per_epoch, verbose=2, epochs=1)

    if (epoch+1)%20==0:

	# Get embedding matrix
	ent_emb_matrix = model.get_layer('ent_embeddings').get_weights()[0]
	rel_emb_matrix = model.get_layer('rel_embeddings').get_weights()[0]


        filename = store_in + '/distM_dim%s_nneg%s_epoch%s.pkl'%(embedding_dim, num_negative, epoch+1)
        f = open(filename, 'w')
        cPickle.dump(ent_emb_matrix, f)
        cPickle.dump(vertex_indexer, f)
        cPickle.dump(rel_emb_matrix, f)
        cPickle.dump(relation_indexer, f)
        f.close()

#	ranks = []
#	# Link prediction
#	entities = range(numEnt)
#	for nb_ex, edge in enumerate(lines_test):
#
#            if edge[0] in vertex_indexer and edge[2] in vertex_indexer:
#    	    	left_hand_side_correct = edge[0]
#	    	right_hand_side_correct = edge[2]
#	    	rel_id = relation_indexer[edge[1]]
#
#	    	head = vertex_indexer[edge[0]]
#	    	tail = vertex_indexer[edge[2]]
#
#	    	triples = []
#	    	Y_test_labels = []
#	    	# TAIL prediction
#	    	for ent in entities:
#        	    str_label_list = [edge[0], idx2ent[ent]]
#        	    Y_test_labels.append(str_label_list)
#
#	    	x = score_fn([np.array(head).reshape((1, 1)), np.array(rel_id).reshape((1,1)), 0])[0].reshape(1, -1)	
#	    	scores = np.dot(x, emb_matrix.T).squeeze()
#	    	result_labels = [(x,y) for (y,x) in sorted(zip(scores, Y_test_labels), reverse=True)]
#
#	    	rank_cleaned = 1
#		if len(edge)==5:
#
#                    for r in result_labels:
#                        if r[0][1] == right_hand_side_correct:
#                            break
#
#                        if not (left_hand_side_correct, edge[1], r[0][1], edge[3], edge[4]) in is_correct_triple:
#                            rank_cleaned += 1
#		else:
#
#	    	    for r in result_labels:
#	            	if r[0][1] == right_hand_side_correct:
#	            	    break
#
#	    	    	if not (left_hand_side_correct, edge[1], r[0][1]) in is_correct_triple:
#        	    	    rank_cleaned += 1
#
#	    	ranks.append(rank_cleaned)
#	    	#print 'MR (right) : %s'%(rank_cleaned)
#	    	sys.stdout.flush()
#
#	    	triples = []
#	    	Y_test_labels = []
#	    	# HEAD prediction
#	    	for ent in entities:
#        	    str_label_list = [idx2ent[ent], edge[2]]
#        	    Y_test_labels.append(str_label_list)
#
#	    	x = score_fn([np.array(tail).reshape((1, 1)), np.array(rel_id).reshape((1,1)), 0])[0].reshape(1, -1)
#	    	scores = np.dot(x, emb_matrix.T).squeeze()
#
#	    	result_labels = [(x,y) for (y,x) in sorted(zip(scores, Y_test_labels), reverse=True)]
#
#	    	rank_cleaned = 1
#		if len(edge)==5:
#                    for r in result_labels:
#                        if r[0][0] == left_hand_side_correct:
#                            break
#
#                        if not (r[0][0], edge[1], right_hand_side_correct, edge[3], edge[4]) in is_correct_triple:
#                            rank_cleaned += 1
#
#
#		else:
#
#	    	    for r in result_labels:
#        	    	if r[0][0] == left_hand_side_correct:
#            	    	    break
#
#	            	if not (r[0][0], edge[1], right_hand_side_correct) in is_correct_triple:
#            	    	    rank_cleaned += 1
#	
#	    	ranks.append(rank_cleaned)
#	    	sys.stdout.flush()
#
#	ones = np.ones((len(ranks)))	
#	ranks = np.array(ranks)
#	hits10 = np.mean(ranks<11)
#	hits1 = np.mean(ranks==1)
#	MR = np.mean(ranks)
#	MRR = np.mean(ones / ranks)
#        print 'Dropout %s'%(drop)
#	print 'Last performance : %s - %s - %s - %s'%(MRR, MR, hits1, hits10)
#	if MRR > bestMRR:
#	    bestMRR = MRR
#	    best_ranks = ranks
#            print 'Best performance : %s - %s - %s - %s'%(MRR, MR, hits1, hits10)
#            filename = 'emb_disM_%s.pkl'%(drop)
#            f = open(filename, 'w')
#            cPickle.dump(emb_matrix, f)
#            cPickle.dump(rel_matrix, f)
#            f.close()
#	    #print 'Saved model! Epoch %s'%(epoch)
#	    #model.save('distM_%s_bestFB15k.h5'%(embedding_dim))
#
#
#index = 0
#filename = 'ranks_disM_%s.txt'%(drop)
#f = open(filename, 'w')
#for nb_ex, edge in enumerate(lines_test):
#
#    if edge[0] in vertex_indexer and edge[2] in vertex_indexer:
#        if len(edge) == 3:
#            sentence = '(%s,%s,%s) %s %s'%(edge[0], edge[1], edge[2], best_ranks[index], best_ranks[index+1])
#        else:
#            sentence = '(%s, %s, %s, %s, %s) %s %s'%(edge[0], edge[1], edge[2], edge[3], edge[4], best_ranks[index], best_ranks[index+1])
#
#	index += 2
#	f.write(sentence + '\n')
#
#f.close()
#
