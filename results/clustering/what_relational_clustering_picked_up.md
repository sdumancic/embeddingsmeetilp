# IMDB

    * relational structure (actors and directors have different ways of interacting with other entities)

    RESULT:
        * Relational vs TransE: comparable
        * Relational vs. DistMult: relational win by a large margin
--

# UWCSE

    * attributes and structure (student has attributes such as years in program and phase of studies, while professors have positions)

    * structure/relations (professors advise students, while students are advised by professors)

    RESULT:
        * Relational does much better

--

# MUTAGENESIS

    * attributes (values of molecular attributes)

    * attributes of connected entities (attr values of their atoms)

    ? some information about bond types seems partially interesting

    NOTE: disconnected graph, connected by attribute values

    RESULT:
        * Relational does much better


--

# TERRORISTS

    * attributes (word popping up in conversations between terrorist groups?)

    * distinct structural formats (how they interact with other entities)

    * some clusters show community structure (there is the Hamas cluster, Revolutionary_Organization_17 cluster)


    RESULT:
        * comparable performance
--

# WEBKB

    * attributes (words in documents)

    * structural profiles are distinct

    * some clusters are communities (all pages connected to one central page)

    RESULT:
        * Relational vs. TransE: relational does better (0.56), but TransE is close (0.52)
        * Relational vs. DistMult: relational wins by a large margin