# HEPATITIS

    * depth of rules: 1/2/2/1/1 (number of jumps)
    * relations only rules: 1/2/2/2/1
    * entity attribute query: 6/1/7/9/7
    * mixed rules: 0/11/10/19/0

    total attr nr.: 2, used: 2
    total number of neighbourhood attributes: 10, used: 7

    total number of outgoing relations: 3, used: 2

    Note: patient graph are connected through shared attribute values

    RESULT:
        * TILDE on original data: embeddings do better (99% vs. 81%)
            - vs Decision Tree: embeddings do better (90% vs 81%)
        * TILDE on latent data: embeddings do better (99% vs. 93%)

--

# MUTAGENESIS

    * depth of rules: 0/0/0/0/0
    * relations only rules: 0/0/0/0/0
    * entity attribute query: 5/6/4/5/13
    * mixed rules: 0/0/0/0/0

    total number of mol attributes: 4, attributes used: 4
    total number of neighbourhood attr: 3, used: 0

    total number of outgoing relations: 4, used 0

    Note: disconnected graphs

    RESULT:
        * TILDE on original data: comparable (78% vs 77%)
            - vs Decision Tree: comparable
        * TILDE on latent data: relational methods do better (84% vs 78%)

--

# TERRORISTS

    * depth of rules: 1/1/0/1/0
    * relations only rules: 0/0/0/0/0
    * entity attribute query: 35/13/7/26/15
    * mixed rules: 3/1/0/1/0

    total attribute nr: 104, attributes used: ~35

    total number of outgoing relations: 2, used: 2

    RESULT:
         both are comparable (embeddings 86% vs. relational 85%)

            - TILDE on original data vs embeddings Decision Tree: relational methods win (85% vs. 78%)

--

# WEBKB

    * depth of rules: 2/2/2/1
    * relations only rules: 2/2/3/2
    * entity attribute query: 34/30/21/45
    * mixed rules: 10/15/5/1

    total attribute nr: ~1000, attributes used: ~30-50
    total attributes of neighbours: ~ 1000, used: <10

    total number of outgoing relations: 4, used: 2

    RESULT:
        * TILDE on original data: relational methods win (81% vs 56%)
        * TILDE on latent data: relational methods win by a large margin (91% vs 56%)