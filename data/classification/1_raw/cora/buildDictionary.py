import os
import pickle

def_file = [x for x in os.listdir(".") if "def" in x][0]
db_files = [x for x in os.listdir(".") if "db" in x]

predicate_defs = {}

for line in open(def_file).readlines():
    if len(line) < 3:
        continue
    else:
        pred, args = line.strip().replace(")", "").split("(")
        args = args.split(",")
        predicate_defs[pred] = args

domains = {}

for dfile in db_files:
    for line in open(dfile).readlines():
        if len(line) < 3:
            continue
        else:
            pred, args = line.strip().replace(")", "").split("(")
            args = args.split(",")

            for elem, dom in zip(args, predicate_defs[pred]):
                if dom not in domains:
                    domains[dom] = {}
                if elem not in domains[dom]:
                    domains[dom][elem] = dom[0].capitalize() + str(len(domains[dom]) + 1)


for dfile in db_files:
    out_file = open(dfile.replace(".db", ".new.db"), 'w')
    for line in open(dfile).readlines():
        if len(line) < 3:
            continue
        else:
            pred, args = line.strip().replace(")", "").split("(")
            args = args.split(",")

            args_with_domains = zip(args, predicate_defs[pred])

            out_file.write("{}({})\n".format(pred, ",".join(map(lambda x: domains[x[1]][x[0]], args_with_domains))))

    out_file.close()


with open('dictionary.pkl', 'wb') as f:
    # Pickle the 'data' dictionary using the highest protocol available.
    pickle.dump(domains, f, pickle.HIGHEST_PROTOCOL)
