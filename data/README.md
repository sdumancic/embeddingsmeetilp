Folders have the following structure:
  - `classification` folder contains the data for classification experiments. Sub-directories:
    - `1_raw` contains raw data
    - `2_folds` contains training and test files for training
    - `3_pre-embedding` contains data in format for learning embeddings
    - `4_embeddings` contains embeddings themselves

  - `clustering` folder contains the data for clustering experiments. Sub-directories:
    - `1_raw` contains original data in logic format
    - `2_pre-embedding` contains data in format for learning embeddings
    - `3_embeddings` contains embeddings themselves

  - `scripts` contains scripts used to get the data in the right shape:
    - `reifyData.py` transforms the n-arity predicates (n > 2) to binary relations
    - `prepareFolds.py` prepares training and test files

