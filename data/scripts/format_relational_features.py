import os
from learners.FeatureGenerators import FeatureReader
from learners.LearnerSettings import constant_domains
import pickle
import numpy as np

CLASSIFICATION_DATA_ROOT = os.path.abspath(os.path.dirname(__file__)) + "/../classification/2_folds"
CLASSIFICATION_DATASETS = ['terrorists', 'mutagenesis', 'webkb']

EMBEDDING_DIR = os.path.abspath(os.path.dirname(__file__)) + "/../classification/4_embeddings"

PREDICATE_DEF_ROOT = os.path.abspath(os.path.dirname(__file__)) + "/../classification/1_raw"

FEATURE_GENERATOR = ['AlephFeatureGenerator']
ACCURACY_LIMITS = [0.1, 0.3, 0.5, 0.7]
MAX_FEATURE_LIMITS = [1000]

DOMAINS_TO_IGNORE = constant_domains

TIMEOUT = 1 # hours
PRUNE = False

SETTING = 'transductive'


def read_pred_definitions(infile):
    predicates = {}

    for line in open(infile).readlines():
        pred, args = line.strip().lower().replace(")", "").split("(")
        args = [x.lower() for x in args.split(",")]

        predicates[pred] = args

    return predicates


for data in CLASSIFICATION_DATASETS:
    print("Processing {} data".format(data))

    data_dir = CLASSIFICATION_DATA_ROOT + "/" + data
    folds = os.listdir(data_dir)

    save_dir = EMBEDDING_DIR + "/" + data

    PREDICATE_DEFINITION = PREDICATE_DEF_ROOT + "/" + data + "/predicate.defs"

    for fold in folds:
        print("    with fold {}".format(fold))
        data_dir_inner = data_dir + "/" + fold
        save_dir_inner = save_dir + "/" + fold

        TRAINING_DATA = data_dir_inner + "/training.db"
        TEST_DATA = data_dir_inner + "/test.db"

        for feature_gen in FEATURE_GENERATOR:
            print("        with {}".format(feature_gen))

            for acc_limit in ACCURACY_LIMITS:
                print("              accuracy {}".format(acc_limit))

                for fn in MAX_FEATURE_LIMITS:
                    print("                    max features {}".format(fn))

                    if not os.path.exists(data_dir_inner + "/{}_acc{}_mf{}.features".format(feature_gen, acc_limit, fn)):
                        print("could not fine {}/{}_acc{}_mf{}.features".format(data_dir_inner, feature_gen, acc_limit, fn))
                        continue

                    RELATIONAL_FEATURES = data_dir_inner + "/{}_acc{}_mf{}.features".format(feature_gen, acc_limit, fn)

                    fr = FeatureReader(PREDICATE_DEFINITION, RELATIONAL_FEATURES, [TRAINING_DATA], save_dir_inner, timeout=TIMEOUT, prune=PRUNE)

                    constants = fr.get_constants()

                    entity_matrix = {}

                    for dom in [x for x in constants if x not in constant_domains]:
                        print("          domain {} train".format(dom))
                        tmp_mat = fr.get_feature_vector([x.lower() for x in constants[dom]], dom.lower())

                        for ent in tmp_mat:
                            entity_matrix[ent] = tmp_mat[ent]

                    rel_features_file = open("{}/relational_features_acc{}_mf{}_prune{}.training.pkl".format(save_dir_inner, acc_limit, fn, PRUNE), 'wb')

                    pickle.dump(entity_matrix, rel_features_file)

                    rel_features_file.close()

                    #tfr = FeatureReader(PREDICATE_DEFINITION, RELATIONAL_FEATURES, [TEST_DATA], save_dir_inner, timeout=TIMEOUT, prune=PRUNE)

                    fr.read_new_data([TEST_DATA])

                    #constants = tfr.get_constants()
                    constants = fr.get_constants()

                    entity_matrix = {}

                    for dom in [x for x in constants if x not in constant_domains]:
                        print("          domain {} test".format(dom))
                        #tmp_mat = tfr.get_feature_vector([x.lower() for x in constants[dom]], dom.lower())
                        tmp_mat = fr.get_feature_vector([x.lower() for x in constants[dom]], dom.lower(), start_with_features=True)

                        for ent in tmp_mat:
                            entity_matrix[ent] = tmp_mat[ent]

                    trel_features_file = open("{}/relational_features_acc{}_mf{}_prune{}.test.pkl".format(save_dir_inner, acc_limit, fn, PRUNE), 'wb')

                    pickle.dump(entity_matrix, trel_features_file)

                    trel_features_file.close()

            if SETTING == 'transductive':
                continue

