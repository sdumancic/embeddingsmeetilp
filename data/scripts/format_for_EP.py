import os
from learners.LearnerSettings import constant_domains
import pickle
import numpy as np


DATASETS = ["uwcse"]
INPUT_DIR = os.path.abspath(os.path.dirname(__file__)) + "/../classification/1_raw"
OUTPUT_DIR = os.path.abspath(os.path.dirname(__file__)) + "/../classification/5_ep"

ACCURACY_LIMITS = [0.1, 0.3, 0.5, 0.7]
MAX_FEATURE_LIMITS = [1000, 10000]
SETUP = 'transductive'
PRUNE = [True, False]

RELATIONAL_FEATURES_DIR = os.path.abspath(os.path.dirname(__file__)) + "/../classification/4_embeddings"

if not os.path.exists(OUTPUT_DIR):
    os.mkdir(OUTPUT_DIR)

for dataset in DATASETS:
    print(dataset)
    INPUT_DATA = os.path.abspath(os.path.dirname(__file__)) + "/../classification/1_raw/" + dataset
    OUTPUT = OUTPUT_DIR + "/" + dataset

    if not os.path.exists(OUTPUT):
        os.mkdir(OUTPUT)

    DB_FILES = [x for x in os.listdir(INPUT_DATA) if x.endswith(".db")]
    LABEL_FILES = [x for x in os.listdir(INPUT_DATA) if x.endswith(".labels")]
    PRED_DEFINITIONS = INPUT_DATA + "/predicate.defs"

    print("using DB files: {}".format(DB_FILES))
    print("using labels: {}".format(LABEL_FILES))

    graph_file = open("{}/{}.graph".format(OUTPUT, dataset), 'w')
    attributes_file = open("{}/{}.attributes".format(OUTPUT, dataset), 'w')
    graph_types_file = open("{}/{}.graph_with_types".format(OUTPUT, dataset), 'w')
    vertex_types_file = open("{}/{}.node_types".format(OUTPUT, dataset), 'w')

    labels_file = open("{}/{}.labels".format(OUTPUT, dataset), 'w')
    attrid_file = open("{}/{}.attr_id_to_name".format(OUTPUT, dataset), 'w')

    predicate_defs = {}

    for line in open(PRED_DEFINITIONS).readlines():
        if len(line) < 3 or line.startswith("//"):
            continue
        else:
            pred, doms = line.strip().replace(")", "").replace(".", "").split("(")
            pred = pred.lower()
            doms = [x.lower() for x in doms.split(",")]

            predicate_defs[pred] = doms

    attributes = {}
    attribute_ids = {}
    node_types = {}

    for db_file in DB_FILES:
        print("    " + db_file)
        for line in open("{}/{}".format(INPUT_DATA, db_file)).readlines():
            if len(line) < 3:
                continue
            else:
                pred, args = line.strip().replace(")", "").replace(".", "").split("(")
                pred = pred.lower()
                args = [x.replace("%", '').replace('\\', '').replace('=', '').lower() for x in args.split(",")]

                for elem, dom in zip(args, predicate_defs[pred]):
                    if dom not in node_types:
                        node_types[dom] = set()
                    node_types[dom].add(elem)

                if len(args) == 1:
                    if args[0] not in attributes:
                        attributes[args[0]] = set()
                    attributes[args[0]].add(pred)
                    if pred not in attribute_ids:
                        attribute_ids[pred] = len(attribute_ids)
                else:
                    attribute_pred = [True if x in constant_domains else False for x in predicate_defs[pred]]

                    if True not in attribute_pred:
                        for ind1 in range(len(args)-1):
                            for ind2 in range(ind1 + 1, len(args)):
                                graph_file.write("{},{}\n".format(args[ind1], args[ind2]))
                                graph_types_file.write("{},{},{}\n".format(args[ind1], pred, args[ind2]))
                    else:
                        attribute_items = [x[0] for x in zip(args, attribute_pred) if x[1]]
                        nodes = [x[0].lower() for x in zip(args, attribute_pred) if not x[1]]

                        for node in nodes:
                            if node not in attributes:
                                attributes[node] = set()
                            for attr in attribute_items:
                                attributes[node].add(attr)
                                if attr not in attribute_ids:
                                    attribute_ids[attr] = len(attribute_ids)

    for elem in attributes:
        attributes_file.write("{},{}\n".format(elem, ",".join(map(lambda x: str(attribute_ids[x]), attributes[elem]))))

    for item in attribute_ids:
        attrid_file.write("{} {}\n".format(attribute_ids[item], item))
    attrid_file.close()

    for dom in node_types:
        for elem in node_types[dom]:
            vertex_types_file.write("{},{}\n".format(elem, dom))

    for l_files in LABEL_FILES:
        print("    " + l_files)
        for line in open("{}/{}".format(INPUT_DATA, l_files)).readlines():
            if len(line) < 3:
                continue
            else:
                lab, elem = line.strip().replace(")", "").split("(")
                labels_file.write("{},{}\n".format(elem.lower(), lab.lower()))

    relational_features_dir = RELATIONAL_FEATURES_DIR + "/" + dataset
    relational_features_dir = relational_features_dir + "/" + [x for x in os.listdir(relational_features_dir)][0]

    for min_acc in ACCURACY_LIMITS:
        for fn in MAX_FEATURE_LIMITS:
            for pr in PRUNE:

                relfeat_file = open("{}/{}_acc{}_mf{}_prune{}.relational_features".format(OUTPUT, dataset, min_acc, fn, pr), 'w')

                training_rf = open("{}/relational_features_acc{}_mf{}_prune{}.training.pkl".format(relational_features_dir, min_acc, fn, pr), 'rb')


                ent_mat = pickle.load(training_rf)

                #for elem in ent_mat:
                #    non_zero_elems = np.nonzero(ent_mat[elem])[0]
                #    relfeat_file.write("{},{}\n".format(elem.lower(), ",".join([str(x) for x in non_zero_elems])))

                #if SETUP == 'inductive':
                test_rf = open("{}/relational_features_acc{}_mf{}_prune{}.test.pkl".format(relational_features_dir, min_acc, fn, pr),
                                   'rb')
                ent_mat_2 = pickle.load(test_rf)

                for item in ent_mat_2:
                    if item not in ent_mat:
                        ent_mat[item] = ent_mat_2[item]
                    else:
                        ent_mat[item] = ent_mat[item] + ent_mat_2[item]

                for elem in ent_mat:
                    non_zero_elems = np.nonzero(ent_mat[elem])[0]
                    if len(non_zero_elems):
                        relfeat_file.write("{},{}\n".format(elem.lower(), ",".join([str(x) for x in non_zero_elems])))

                relfeat_file.close()

    graph_file.close()
    attributes_file.close()
    graph_types_file.close()
    vertex_types_file.close()
    labels_file.close()
