import os
import pandas as pd
import numpy as np

RELEP_ROOT = os.path.abspath(os.path.dirname(__file__)) + "/../relep"
RELEP_TO_STORE = os.path.abspath(os.path.dirname(__file__)) + "/../classification/5_ep"

DIMENSION = [20]
SETUP = ['transductive']  # 'inductive',

for dataset in ['webkb']: #os.listdir(RELEP_ROOT):
    source_root = RELEP_ROOT + "/" + dataset
    output_root = RELEP_TO_STORE + "/" + dataset

    if not os.path.exists(output_root):
        os.mkdir(output_root)

    for setup in SETUP:
        source_root_f = source_root + "/" + setup
        output_root_f = output_root + "/" + setup

        if not os.path.exists(output_root_f):
            os.mkdir(output_root_f)

        for dim in DIMENSION:
            embs = [x for x in os.listdir(source_root_f) if str(dim) in x and x.endswith(".h5")]
            print("all embs: {}".format(embs))

            for emb in embs:
                print("processing {}".format(emb))
                name = "_".join(emb.replace(".h5", "").split("__")[1].split("_"))
                relfeat_dim = int(emb.replace(".h5", "").replace("__", "_").split("_")[-3])
                print("Saving to: " + output_root_f)
                save_file_relations = open("{}/relep_{}_wr.entity.emb".format(output_root_f, name), 'w')
                save_file_norelations = open("{}/relep_{}_nr.entity.emb".format(output_root_f, name), 'w')
                store = pd.HDFStore("{}/{}".format(source_root_f, emb))
                embedded_nodes = store['df']
                num_of_entities = embedded_nodes.shape[0]
                emb_columns = embedded_nodes.columns[1:]
                emb_columns_norelations = embedded_nodes.columns[1:-10]

                for ind in range(num_of_entities):
                    entity_name = embedded_nodes.loc[ind, 'identity']
                    features = list(embedded_nodes.loc[ind, emb_columns])

                    save_file_relations.write("{} {}\n".format(entity_name, ",".join([str(x) for x in features])))
                    save_file_norelations.write("{} {}\n".format(entity_name, ",".join(str(x) for x in features[:-relfeat_dim])))

                save_file_relations.close()








