import os

DATA_DIRECTORY = os.path.dirname(os.path.abspath(__file__)) + "/../classification/1_raw/bongard4"
OUTPUT_ROOT = os.path.dirname(os.path.abspath(__file__)) + "/../classification/2_folds/bongard4"

if not os.path.exists(OUTPUT_ROOT):
    os.mkdir(OUTPUT_ROOT)

data_files = [x for x in os.listdir(DATA_DIRECTORY) if x.endswith(".db") and not x.startswith(".")]
label_files = [x for x in os.listdir(DATA_DIRECTORY) if x.endswith(".labels")]

print(data_files)
print(label_files)

for test_file in data_files:
    training_files = [x for x in data_files if x != test_file]
    training_labels = [x for x in label_files if test_file.replace(".db", "") not in x]
    test_labels = [x for x in label_files if test_file.replace(".db", "") in x]

    names_in_training_files = set()
    names_in_training_files.add("labels")
    for x in training_files:
        tmp = x.split(".")
        for y in tmp:
            names_in_training_files.add(y)

    print(names_in_training_files)

    working_folder = OUTPUT_ROOT + "/" + test_file

    if not os.path.exists(working_folder):
        os.mkdir(working_folder)

    training_data_file = open("{}/training.db".format(working_folder), 'w')

    training_labels_files = open("{}/training.labels".format(working_folder), 'w')

    test_data_file = open("{}/test.db".format(working_folder), 'w')

    test_labels_files = open("{}/test.labels".format(working_folder), 'w')

    for f in training_files:
        for line in open(DATA_DIRECTORY + "/" + f).readlines():
            training_data_file.write(line)

    for f in training_labels:
        for line in open(DATA_DIRECTORY + "/" + f).readlines():
            training_labels_files.write(line)

    for line in open(DATA_DIRECTORY + "/" + test_file):
        test_data_file.write(line)

    for f in test_labels:
        for line in open(DATA_DIRECTORY + "/" + f):
            test_labels_files.write(line)

    training_data_file.close()
    training_labels_files.close()
    test_labels_files.close()
    test_data_file.close()
