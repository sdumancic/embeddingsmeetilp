###################################
#
#    HAS TO BE RUN WITH PYTHON 2.7
#
###################################

import cPickle
import os
import numpy as np
import numpy.linalg as la


def _get_transe_score(entity1, entity2, relation):
    difference = la.norm(entity1 + relation - entity2)
    return round(np.exp(-difference), 2)


def _get_distmult_score(entity1, entity2, relation):
    score = np.dot(np.multiply(entity1, entity2), relation)
    return round(np.exp(-score), 2)


def _get_relation_distance(relation1, relation2):
    distance = la.norm(relation1 - relation2)
    return round(distance, 2)


def _get_entity_distance(entity1, entity2):
    distance = la.norm(entity1 - entity2)
    return round(distance, 2)


def _angle_between_relations(relation1, relation2):
    # cosang = np.dot(relation1, relation2)
    # sinang = la.norm(np.cross(relation1, relation2))
    # radian = np.arctan2(sinang, cosang)

    dot = np.dot(relation1, relation2)
    x_modulus = np.sqrt((relation1 * relation1).sum())
    y_modulus = np.sqrt((relation2 * relation2).sum())
    cos_angle = dot / x_modulus / y_modulus
    radian = np.arccos(cos_angle)

    return int(np.rad2deg(radian))


def _emb_avg(emb1, emb2):
    return np.divide(np.add(emb1, emb2), 2.0)


def _emb_sum(emb1, emb2):
    return np.add(emb1, emb2)


def _emb_concat(emb1, emb2):
    return np.concatenate((emb1, emb2))


EMBEDDING_METHOD = 'DistMult'
EMBEDDING_ROOT = os.path.dirname(__file__) + "/../../" + EMBEDDING_METHOD + "/results"

CLUSTERING_FLAG = False
CLASSIFICATION_FLAG = True

DATA_ROOT = os.path.dirname(__file__) + "/../clustering/3_embeddings"
CLASSIFICATION_DATA_ROOT = os.path.dirname(__file__) + "/../classification/4_embeddings"

CLUSTERING_DATASETS = ['imdb', 'uwcse.db', 'mutagenesis', 'terrorists', 'webkb']
CLASSIFICATION_DATASETS = ['terrorists', 'mutagenesis', 'terrorists', 'webkb', 'hepatitis']

CLUSTERING_PRE_EMBEDDINGS_ROOT = os.path.dirname(__file__) + "/../clustering/2_pre-embeddings"
CLASSIFICATION_PRE_EMBEDDINGS_ROOT = os.path.dirname(__file__) + "/../classification/3_pre-embeddings"

PRECOMPUTE = False

if CLUSTERING_FLAG:
    for data in CLUSTERING_DATASETS:
        embedding_data = os.listdir(EMBEDDING_ROOT + "/" + data)

        save_dir = DATA_ROOT + "/" + data

        if not os.path.exists(save_dir):
            os.mkdir(save_dir)

        for emb_file in embedding_data:
            f = open(EMBEDDING_ROOT + "/" + data + "/" + emb_file, 'rb')
            ent_emb_matrix = cPickle.load(f)
            ent_index = cPickle.load(f)
            rel_emb_matrix = cPickle.load(f)
            rel_index = cPickle.load(f)

            dim = emb_file.replace(".pkl", "").split("_")[1]
            epochs = emb_file.replace(".pkl", "").split("_")[3]
            ent_file_name = save_dir + "/{}_{}_{}.entity.emb".format(EMBEDDING_METHOD, dim, epochs)
            rel_file_name = save_dir + "/{}_{}_{}.relation.emb".format(EMBEDDING_METHOD, dim, epochs)

            emb_file = open(ent_file_name, 'w')
            for entity in ent_index:
                emb_file.write("{} {}\n".format(entity, ",".join([str(x) for x in ent_emb_matrix[ent_index[entity]]])))

            emb_file.close()

            rel_file = open(rel_file_name, 'w')
            for relation in rel_index:
                rel_file.write(
                    "{} {}\n".format(relation, ",".join([str(x) for x in rel_emb_matrix[rel_index[relation]]])))

            rel_file.close()

            labels_to_use = CLUSTERING_PRE_EMBEDDINGS_ROOT + "/" + data + "/labels.txt"

            labels_file = open(DATA_ROOT + "/" + data + "/train.labels", 'w')

            for line in open(labels_to_use).readlines():
                labels_file.write(line)

            labels_file.close()

if CLASSIFICATION_FLAG:
    for data in CLASSIFICATION_DATASETS:
        print("Processing {} data".format(data))
        embedding_data = [x for x in os.listdir(EMBEDDING_ROOT + "/" + data) if x.endswith(".pkl")]

        save_dir = CLASSIFICATION_DATA_ROOT + "/" + data

        if not os.path.exists(save_dir):
            os.mkdir(save_dir)

        for emb_file in embedding_data:
            if EMBEDDING_METHOD != "complEX":
                print("  with {} embeddings".format(emb_file))
                f = open(EMBEDDING_ROOT + "/" + data + "/" + emb_file, 'rb')
                try:
                    ent_emb_matrix = cPickle.load(f)
                    ent_index = cPickle.load(f)
                    rel_emb_matrix = cPickle.load(f)
                    rel_index = cPickle.load(f)

                    dim = emb_file.replace(".pkl", "").split("_")[1]
                    epochs = emb_file.replace(".pkl", "").split("_")[3]

                    folds = os.listdir(CLASSIFICATION_PRE_EMBEDDINGS_ROOT + "/" + data)
                    for fold in folds:
                        save_dir_inner = save_dir + "/" + fold

                        if not os.path.exists(save_dir_inner):
                            os.mkdir(save_dir_inner)

                        ent_file_name = save_dir_inner + "/{}_{}_{}.entity.emb".format(EMBEDDING_METHOD, dim, epochs)
                        rel_file_name = save_dir_inner + "/{}_{}_{}.relation.emb".format(EMBEDDING_METHOD, dim, epochs)

                        emb_file = open(ent_file_name, 'w')
                        for entity in ent_index:
                            emb_file.write(
                                "{} {}\n".format(entity, ",".join([str(x) for x in ent_emb_matrix[ent_index[entity]]])))
                        emb_file.close()

                        rel_file = open(rel_file_name, 'w')
                        for relation in rel_index:
                            rel_file.write(
                                "{} {}\n".format(relation, ",".join([str(x) for x in rel_emb_matrix[rel_index[relation]]])))

                        rel_file.close()

                        for labels_filename in ['train.labels', 'test.labels']:
                            labels_to_use = CLASSIFICATION_PRE_EMBEDDINGS_ROOT + "/" + data + "/" + fold + "/" + labels_filename

                            labels_file = open(save_dir_inner + "/" + labels_filename, 'w')

                            for line in open(labels_to_use):
                                labels_file.write(line)

                            labels_file.close()
                except Exception:
                    print("        SKIPPED")
                f.close()
            else:
                f = open(EMBEDDING_ROOT + "/" + data + "/" + emb_file, 'rb')
                try:
                    ent_emb_matrix_real = cPickle.load(f)
                    ent_emb_matrix_img = cPickle.load(f)
                    ent_index = cPickle.load(f)
                    rel_emb_matrix_real = cPickle.load(f)
                    rel_emb_matrix_img = cPickle.load(f)
                    rel_index = cPickle.load(f)

                    dim = emb_file.replace(".pkl", "").split("_")[1]
                    epochs = emb_file.replace(".pkl", "").split("_")[3]

                    folds = os.listdir(CLASSIFICATION_PRE_EMBEDDINGS_ROOT + "/" + data)

                    for fold in folds:
                        save_dir_inner = save_dir + "/" + fold

                        if not os.path.exists(save_dir_inner):
                            os.mkdir(save_dir_inner)

                        for op in ['avg', 'sum', 'concat']:
                            ent_file_name = save_dir_inner + "/{}{}_{}_{}.entity.emb".format(EMBEDDING_METHOD, op, dim, epochs)
                            rel_file_name = save_dir_inner + "/{}{}_{}_{}.relation.emb".format(EMBEDDING_METHOD, op, dim, epochs)

                            emb_file = open(ent_file_name, 'w')
                            for entity in ent_index:
                                if op == 'avg':
                                    result = _emb_avg(ent_emb_matrix_real[ent_index[entity]], ent_emb_matrix_img[ent_index[entity]])
                                    emb_file.write(
                                        "{} {}\n".format(entity, ",".join([str(x) for x in result])))
                                elif op == 'sum':
                                    result = _emb_sum(ent_emb_matrix_real[ent_index[entity]],
                                                      ent_emb_matrix_img[ent_index[entity]])
                                    emb_file.write(
                                        "{} {}\n".format(entity, ",".join([str(x) for x in result])))
                                else:
                                    result = _emb_concat(ent_emb_matrix_real[ent_index[entity]],
                                                      ent_emb_matrix_img[ent_index[entity]])
                                    emb_file.write(
                                        "{} {}\n".format(entity, ",".join([str(x) for x in result])))
                            emb_file.close()

                            rel_file = open(rel_file_name, 'w')
                            for relation in rel_index:
                                if op == 'avg':
                                    result = _emb_avg(rel_emb_matrix_real[rel_index[relation]], rel_emb_matrix_img[rel_index[relation]])
                                    rel_file.write(
                                        "{} {}\n".format(relation,
                                                     ",".join([str(x) for x in result])))
                                elif op == 'sum':
                                    result = _emb_sum(rel_emb_matrix_real[rel_index[relation]],
                                                      rel_emb_matrix_img[rel_index[relation]])
                                    rel_file.write(
                                        "{} {}\n".format(relation,
                                                         ",".join([str(x) for x in result])))
                                else:
                                    result = _emb_concat(rel_emb_matrix_real[rel_index[relation]],
                                                         rel_emb_matrix_img[rel_index[relation]])
                                    rel_file.write(
                                        "{} {}\n".format(relation,
                                                         ",".join([str(x) for x in result])))

                            rel_file.close()

                        for labels_filename in ['train.labels', 'test.labels']:
                            labels_to_use = CLASSIFICATION_PRE_EMBEDDINGS_ROOT + "/" + data + "/" + fold + "/" + labels_filename

                            labels_file = open(save_dir_inner + "/" + labels_filename, 'w')

                            for line in open(labels_to_use):
                                labels_file.write(line)

                            labels_file.close()
                except Exception:
                    print("        SKIPPED")

                f.close()






