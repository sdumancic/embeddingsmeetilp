import os

ROOT = os.path.dirname(os.path.abspath(__file__)) + "/../classification/1_raw/financial_reified/"

DATA_FILES = [ROOT + "financial.1.db", ROOT + "financial.2.db", ROOT + "financial.3.db", ROOT + "financial.4.db", ROOT + "financial.5.db"]

uwcse_predicate_definition = {
    "TA": ["course", "person", "quarter"],
    "Taughtby": ["course", "person", "quarter"]
}

webkb_predicate_definition = {
    "LinkTo": ["link", "page", "page"]
}

financial_predicate_definition = {
    "Order_amount": ["key", "bank_to", "amount"],
    "Order_symbol": ["key", "bank_to", "symbol"],
    "Trans_trans_type": ["key", "date", "trans_type"],
    "Trans_operation": ["key", "date", "operation"],
    "Trans_amount": ["key", "date", "amount"],
    "Trans_balance": ["key", "date", "balance"],
    "Trans_trans_char": ["key", "date", "trans_char"]
}

sysa_predicate_definitions = {
    "Tfkomp_tkbeg": ["tkid", "vvid", "tkbeg"],
    "Tfkomp_tkend": ["tkid", "vvid", "tkend"],
    "Tfkomp_tkexkzwei": ["tkid", "vvid", "tkexkzwei"],
    "Tfkomp_tkstacd": ["tkid", "vvid", "tkstacd"],
    "Tfkomp_tkleist": ["tkid", "vvid", "tkleist"],
    "Tfkomp_tkinkprl": ["tkid", "vvid", "tkinkprl"],
    "Tfkomp_tkinkpre": ["tkid", "vvid", "tkinkpre"],
    "Tfkomp_tktarpra": ["tkid", "vvid", "tktarpra"],
    "Tfkomp_tkuebvwsysp": ["tkid", "vvid", "tkuebvwsysp"],
    "Tfkomp_tkuebvwsysl": ["tkid", "vvid", "tkuebvwsysl"],
    "Tfkomp_tkprfin": ["tkid", "vvid", "tkprfin"],
    "Tfkomp_tkdyncd": ["tkid", "vvid", "tkdyncd"],
    "Tfkomp_tkausbcd": ["tkid", "vvid", "tkausbcd"],
    "Tfkomp_tkrauv": ["tkid", "vvid", "tkrauv"],
    "Tfkomp_tksmed": ["tkid", "vvid", "tksmed"],
    "Tfkomp_tkrizucd": ["tkid", "vvid", "tkrizucd"],
    "Tfkomp_tktodleista": ["tkid", "vvid", "tktodleista"],
    "Tfkomp_tkerlleista": ["tkid", "vvid", "tkerlleista"],
    "Tfkomp_tkrenleista": ["tkid", "vvid", "tkrenleista"],
    "Tfkomp_tkeuleista": ["tkid", "vvid", "tkeuleista"],
    "Tfkomp_tkunfleista": ["tkid", "vvid", "tkunfleista"],
    "Tfrol_trteceinal": ["prid", "tkid", "trteceinal"],
    "Tfrol_truntcd": ["prid", "tkid", "truntcd"],
    "Tfrol_trklauscd": ["prid", "tkid", "trklauscd"],
    "Tfrol_trstafcd": ["prid", "tkid", "trstafcd"],
    "Tfrol_trricd": ["prid", "tkid", "trricd"]
}

sysb_predicate_definitions = {
    "Tfkomp_tkbeg": ["tkid", "vvid", "tkbeg"],
    "Tfkomp_tkend": ["tkid", "vvid", "tkend"],
    "Tfkomp_tkexkzwei": ["tkid", "vvid", "tkexkzwei"],
    "Tfkomp_tkstacd": ["tkid", "vvid", "tkstacd"],
    "Tfkomp_tkleist": ["tkid", "vvid", "tkleist"],
    "Tfkomp_tkinkprl": ["tkid", "vvid", "tkinkprl"],
    "Tfkomp_tkinkpre": ["tkid", "vvid", "tkinkpre"],
    "Tfkomp_tktarpra": ["tkid", "vvid", "tktarpra"],
    "Tfkomp_tkuebvwsysp": ["tkid", "vvid", "tkuebvwsysp"],
    "Tfkomp_tkuebvwsysl": ["tkid", "vvid", "tkuebvwsysl"],
    "Tfkomp_tkprfin": ["tkid", "vvid", "tkprfin"],
    "Tfkomp_tkdyncd": ["tkid", "vvid", "tkdyncd"],
    "Tfkomp_tkausbcd": ["tkid", "vvid", "tkausbcd"],
    "Tfkomp_tkrauv": ["tkid", "vvid", "tkrauv"],
    "Tfkomp_tksmed": ["tkid", "vvid", "tksmed"],
    "Tfkomp_tkrizucd": ["tkid", "vvid", "tkrizucd"],
    "Tfkomp_tktodleista": ["tkid", "vvid", "tktodleista"],
    "Tfkomp_tkerlleista": ["tkid", "vvid", "tkerlleista"],
    "Tfkomp_tkrenleista": ["tkid", "vvid", "tkrenleista"],
    "Tfkomp_tkeuleista": ["tkid", "vvid", "tkeuleista"],
    "Tfkomp_tkunfleista": ["tkid", "vvid", "tkunfleista"],
    "Tfrol_trteceinal": ["prid", "tkid", "trteceinal"],
    "Tfrol_truntcd": ["prid", "tkid", "truntcd"],
    "Tfrol_trklauscd": ["prid", "tkid", "trklauscd"],
    "Tfrol_trstafcd": ["prid", "tkid", "trstafcd"],
    "Tfrol_trricd": ["prid", "tkid", "trricd"]
}

id_dict = {}

for DATA_FILE in DATA_FILES:
    input_file = open(DATA_FILE)
    old_file = open(DATA_FILE.replace(".db", "_old.db"), 'w')

    new_facts = []

    for line in input_file:
        old_file.write(line)

        if line.count(',') < 2:
            new_facts.append(line.strip())
        else:
            pred, args = line.strip().replace(")", "").split("(")
            args = args.split(",")
            id_from_args = ",".join(args)

            if pred not in id_dict:
                id_dict[pred] = {}

            if id_from_args not in id_dict[pred]:
                id_dict[pred][id_from_args] = len(id_dict[pred])

            reified_id = pred + str(id_dict[pred][id_from_args])

            domain_names = []
            if pred in uwcse_predicate_definition:
                domain_names = uwcse_predicate_definition[pred]
            elif pred in webkb_predicate_definition:
                domain_names = webkb_predicate_definition[pred]
            elif pred in financial_predicate_definition:
                domain_names = financial_predicate_definition[pred]
            elif pred in sysa_predicate_definitions:
                domain_names = sysa_predicate_definitions[pred]
            elif pred in sysb_predicate_definitions:
                domain_names = sysb_predicate_definitions[pred]
            else:
                raise Exception("cannot fin predicate {}".format(pred))

            if len(set(domain_names)) < len(domain_names):
                domain_names = [x + str(ind) for (ind, x) in enumerate(domain_names)]

            for item in zip(domain_names, args):
                new_facts.append("{}_{}({},{})".format(pred, item[0], reified_id, item[1]))

    old_file.close()
    input_file.close()

    new_data_file = open(DATA_FILE, 'w')

    for f in new_facts:
        new_data_file.write(f + "\n")

    new_data_file.close()
