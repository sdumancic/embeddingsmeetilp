import os
import pickle

DATA_ROOT = os.path.abspath(os.path.dirname(__file__)) + "/../kbc/1_raw"
OUT_ROOT = os.path.abspath(os.path.dirname(__file__)) + "/../kbc/2_process"

if not os.path.exists(OUT_ROOT):
    os.mkdir(OUT_ROOT)

if __name__ == "__main__":

    for data in os.listdir(DATA_ROOT):
        final_out = OUT_ROOT + "/" + data
        final_data = DATA_ROOT + "/" + data

        if not os.path.exists(final_out):
            os.mkdir(final_out)

        entity_dict = {}
        relation_dict = {}

        for t_file in ['train.txt', 'test.txt', 'valid.txt']:
            fact_per_relation = {}

            for line in open("{}/{}".format(final_data, t_file)).readlines():
                head, rel, tail = line.strip().split()

                if head not in entity_dict:
                    entity_dict[head] = "ent" + str(len(entity_dict))

                if tail not in entity_dict:
                    entity_dict[tail] = "ent" + str(len(entity_dict))

                if rel not in relation_dict:
                    relation_dict[rel] = "rel" + str(len(relation_dict))

                if relation_dict[rel] not in fact_per_relation:
                    fact_per_relation[relation_dict[rel]] = set()

                fact_per_relation[relation_dict[rel]].add((entity_dict[head], entity_dict[tail]))

            r_file = open("{}/{}".format(final_out, t_file), 'w')

            for r in fact_per_relation:
                for gr in fact_per_relation[r]:
                    r_file.write("{}({},{})\n".format(r.capitalize(), gr[0].capitalize(), gr[1].capitalize()))

            f = open("{}/{}".format(final_out, t_file.replace(".txt", ".pkl")), 'wb')

            pickle.dump(fact_per_relation, f)

            f.close()

        en = open("{}/dicts.pkl".format(final_out), 'wb')

        pickle.dump(entity_dict, en)
        pickle.dump(relation_dict, en)

        en.close()

