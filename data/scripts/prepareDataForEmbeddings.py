import os

TASK = "classification"  # 'clustering' or 'classification

TO_SKIP = ['sysa', 'sysa_ori', 'sysb', 'sysb_ori', 'financial', 'financial_format2']

DATA_ROOT = ""
OUTPUT_ROOT = ""

if TASK == 'clustering':
    DATA_ROOT = os.path.dirname(os.path.abspath(__file__)) + "/../{}/1_raw".format(TASK)
    OUTPUT_ROOT = os.path.dirname(os.path.abspath(__file__)) + "/../{}/2_pre-embeddings".format(TASK)
else:
    DATA_ROOT = os.path.dirname(os.path.abspath(__file__)) + "/../{}/2_folds".format(TASK)
    OUTPUT_ROOT = os.path.dirname(os.path.abspath(__file__)) + "/../{}/3_pre-embeddings".format(TASK)

for dataset in ['financial_reified']: # os.listdir(DATA_ROOT):

    working_directory = OUTPUT_ROOT + "/" + dataset

    if not os.path.exists(working_directory):
        os.mkdir(working_directory)

    if TASK == 'clustering':
        train_file = open("{}/train.txt".format(working_directory), 'w')
        for line in open("{}/{}/data.db".format(DATA_ROOT, dataset)).readlines():
            if len(line) < 3:
                continue
            rel, args = line.strip().replace(")", "").split("(")
            args = args.split(",")

            train_file.write("{}\t{}\t{}\n".format(args[0], rel, args[1]))

        train_file.close()

        labels_file = open("{}/labels.txt".format(working_directory), 'w')
        for line in open("{}/{}/labels.db".format(DATA_ROOT, dataset)).readlines():
            if len(line) < 3:
                continue
            label, entity = line.strip().replace(")", "").split("(")
            labels_file.write("{}\t{}\n".format(entity, label))
        labels_file.close()

    elif TASK == 'classification':
        if dataset in TO_SKIP:
            continue
        folds = os.listdir("{}/{}".format(DATA_ROOT, dataset))
        for fold in folds:
            inner_working_directory = "{}/{}".format(working_directory, fold)

            if not os.path.exists(inner_working_directory):
                os.mkdir(inner_working_directory)

            train_file = open("{}/train.txt".format(inner_working_directory), 'w')
            for line in open("{}/{}/{}/training.db".format(DATA_ROOT, dataset, fold)).readlines():
                if len(line) < 3:
                    continue
                rel, args = line.strip().replace(")", "").split("(")
                args = args.split(",")

                train_file.write("{}\t{}\t{}\n".format(args[0], rel, args[1]))

            for line in open("{}/{}/{}/test.db".format(DATA_ROOT, dataset, fold)).readlines():
                if len(line) < 3:
                    continue
                rel, args = line.strip().replace(")", "").split("(")
                args = args.split(",")

                train_file.write("{}\t{}\t{}\n".format(args[0], rel, args[1]))

            training_labels = [x for x in os.listdir("{}/{}/{}".format(DATA_ROOT, dataset, fold)) if "training" in x and "labels" in x]
            test_labels = [x for x in os.listdir("{}/{}/{}".format(DATA_ROOT, dataset, fold)) if "test" in x and "labels" in x]

            for tf in training_labels:
                tl = open("{}/{}".format(inner_working_directory, tf.replace("training", "train")), 'w')
                for line in open("{}/{}/{}/{}".format(DATA_ROOT, dataset, fold, tf)).readlines():
                    if len(line) < 3:
                        continue
                    label, entity = line.strip().replace(")", "").split("(")

                    if ',' in entity:
                        tl.write(" ".join(entity.split(",")))
                    else:
                        tl.write("{}\t{}\n".format(entity, label))
                tl.close()

            for tf in test_labels:
                tl = open("{}/{}".format(inner_working_directory, tf), 'w')
                for line in open("{}/{}/{}/{}".format(DATA_ROOT, dataset, fold, tf)).readlines():
                    if len(line) < 3:
                        continue
                    label, entity = line.strip().replace(")", "").split("(")

                    if ',' in entity:
                        tl.write(" ".join(entity.split(",")))
                    else:
                        tl.write("{}\t{}\n".format(entity, label))
                tl.close()

    else:
        print("UNKNOWN TASK {}".format(TASK))
