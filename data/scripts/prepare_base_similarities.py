import os
import subprocess

dataset = 'mutagenesis'
target_domain = 'mol'

DATA_DIRECTORY = os.path.dirname(os.path.abspath(__file__)) + "/../classification/1_raw" + '/' + dataset
OUTPUT_ROOT = os.path.dirname(os.path.abspath(__file__)) + "/../classification/similarities"

if not os.path.exists(OUTPUT_ROOT):
    os.mkdir(OUTPUT_ROOT)

OUTPUT_ROOT = OUTPUT_ROOT + '/' + dataset

if not os.path.exists(OUTPUT_ROOT):
    os.mkdir(OUTPUT_ROOT)

RECENT_ROOT_CODE = os.environ.get('RECENT_HOME')

data_files = [x for x in os.listdir(DATA_DIRECTORY) if x.endswith(".db") and not x.startswith(".")]

predicate_defs = DATA_DIRECTORY + "/predicate.defs"

predicate_declarations = DATA_DIRECTORY + "/predicate.decl"

DEPTH = [0, 1, 2]
WEIGHTS = ["1.0,0.0,0.0,0.0,0.0", "0.0,1.0,0.0,0.0,0.0", "0.0,0.0,1.0,0.0,0.0", "0.0,0.0,0.0,1.0,0.0", "0.0,0.0,0.0,0.0,1.0"]

for depth in DEPTH:

    print("depth: ", depth)

    for w in WEIGHTS:

        print("weights: ", w)

        cmd = "java -jar {}/RelationalClustering.jar --db {} --domain {} " \
              "--declarations {} --depth {} --root {} --weights {} " \
              "--algorithm Hierarchical -k 2 --similarity RCNT --bagSimilarity chiSquared " \
              "--query {} > output_{}_{} 2> error_{}_{}".format(RECENT_ROOT_CODE,
                                  " --db ".join([DATA_DIRECTORY + "/" + x for x in data_files]),
                                  predicate_defs,
                                  predicate_declarations,
                                  depth,
                                  OUTPUT_ROOT,
                                  w,
                                  target_domain, depth, w, depth, w)

        print(cmd)

        proc = subprocess.Popen(cmd, shell=True, cwd=OUTPUT_ROOT, preexec_fn=os.setsid)
        proc.wait()